# DISPAQ:Distributed Profitable Area Query System

DISPAQ is a distributed system to help taxi driver searching their passenger when the taxi is empty. Using this application, taxi driver get a set of area that make more advantage than others based on their current location and current time.

## Getting Started

### Prerequisites
#### Software
- Hadoop 2.6.0
- Spark 1.5.0
- Java 1.7

#### Java Library
- geohash (https://github.com/davidmoten/geo)
- spark-mongo driver
- mongo-java driver
- mongo-hadoop driver

### Configuration Setting
#### Server Machine
username: hduser
password: dslab24

| Hostname | Address | Hadoop|Spark|mongoDB|
| -------- |:---------------:|:---:|:---:|:---:|
| pc11 | 164.125.121.111 |namenode,datanode|master,working node|configuration server, master server|
| pc12 | 164.125.121.112 |datanode|working node|shard|
| pc13 | 164.125.121.113 |datanode|working node|shard|
| pc14 | 164.125.121.114 |datanode|working node|shard|
| pc15 | 164.125.121.115 |datanode|working node|shard|

#### Hadoop
Start Hadoop Environment in namenode server
```
$ start-all.sh
```
Stop Hadoop Environment in namenode server
```
$ stop-all.sh
```

#### Spark
Start history server in master server
```
$ cd $SPARK_HOME;
$ ./start-history-server.sh;
```

#### MongoDB
Start configuration server
```
$ sudo mongod --configsvr --dbpath /mnt/mongodb/3nodes
```

Start shard server
```
$ sudo mongod --shardsvr --dbpath /mnt/mongodb/3nodes/
```

Start mongos server
```
$ mongos --configdb 164.125.121.111:27019 --port 27020
```
### Web UI for Monitoring
- ResourceManager server: http://pc11:8088/cluster
- NameNode server: http://pc11:50070
- Spark History server: http://pc11:18080

## Application

The application contains two main functionality, PQ-index construction and query processing
### PQ-index Construction


#### Syntax

```
$ spark-submit
  --master yarn-cluster
  --executor-memory [memory size]
  --num-executors [number of executor]
  --jars
          /home/hduser/dispaq/lib/geo-0.7.1.jar,
          /home/hduser/dispaq/lib/grumpy-core-0.2.2.jar,
          /home/hduser/dispaq/lib/mongo-spark-connector_2.10-2.0.0-rc0.jar,
          /home/hduser/dispaq/lib/mongo-java-driver-3.3.0.jar,
          /home/hduser/dispaq/lib/mongo-hadoop-core-1.5.0.jar
  --class [main class]
  [application jar]
  [data source]
  [destination]
  [number of partition]
  [interval time period in minutes]
  [length of geohash code]
```
#### Example
```
$ spark-submit 
    --master yarn-cluster 
    --executor-memory 4GB 
    --num-executors 4 
    --jars 
            /home/hduser/dispaq/lib/geo-0.7.1.jar,
            /home/hduser/dispaq/lib/grumpy-core-0.2.2.jar,
            /home/hduser/dispaq/lib/mongo-spark-connector_2.10-2.0.0-rc0.jar,
            /home/hduser/dispaq/lib/mongo-java-driver-3.3.0.jar,
            /home/hduser/dispaq/lib/mongo-hadoop-core-1.5.0.jar
    --class "construction.upgradedconstruction" 
    /home/hduser/dispaq/dispaq-1.0.jar 
    dataset12month 
    month12test3 
    100
    10
    7
```
#### Note
Option for main class
- NYC       : `construction.upgradedconstruction`
- Chicago   : `construction.upgradedconstructionChicago`

### Query Processing

Explain what these tests test and why

#### Syntax

```
$ spark-submit 
    --master yarn-cluster 
    --executor-memory [memory size]
    --num-executors [number of executor]
    --jars 
        /home/hduser/dispaq/lib/geo-0.7.1.jar,
        /home/hduser/dispaq/lib/grumpy-core-0.2.2.jar,
        /home/hduser/dispaq/lib/mongo-spark-connector_2.10-2.0.0-rc0.jar,
        /home/hduser/dispaq/lib/mongo-java-driver-3.3.0.jar,
        /home/hduser/dispaq/lib/mongo-hadoop-core-1.5.0.jar 
    --class "query.distributedZSkylineQueryOptUpgrade" 
    [application jar]
    [current location]
    [pq-index collection in mongoDB]
    [geohash code length]
    [interval time]
    [current time]
    [partition]
```
#### Example
```
$ spark-submit 
    --master yarn-cluster 
    --executor-memory 4GB 
    --num-executors 3 
    --jars 
        /home/hduser/dispaq/lib/geo-0.7.1.jar,
        /home/hduser/dispaq/lib/grumpy-core-0.2.2.jar,
        /home/hduser/dispaq/lib/mongo-spark-connector_2.10-2.0.0-rc0.jar,
        /home/hduser/dispaq/lib/mongo-java-driver-3.3.0.jar,
        /home/hduser/dispaq/lib/mongo-hadoop-core-1.5.0.jar 
    --class "query.distributedBNLQueryUpgrade" 
    /home/hduser/dispaq/dispaq-1.0.jar 
    -73.98869,40.75872 
    month6upgrade 
    6 
    10 
    1484409000000
    4
```
#### Note
There is four method for query processing, each method has a main class
- Block Nested Looping       : `query.distributedBNLQueryUpgrade`
- Divide and Conquer   : `query.distributedDCQueryUpgrade`
- Z-Skylize: `query.distributedZSkylineQueryUpgrade`
- Optimized Z-Skyline: `query.distributedZSkylineQueryOptUpgrade`


## Authors

* **Fadhilah Kurnia Putri** - *Initial work* - contact: fadhilahkp@gmail.com
