package query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.github.davidmoten.geo.GeoHash;

import model.ProfitabilityArea;

public abstract class abstractSkyline {
	protected static List<ProfitabilityArea> pm;
	
	protected static String defineArea(String lat, String lon, Integer len){
		Double areaLong = Double.parseDouble(lon);
        Double areaLat = Double.parseDouble(lat);
        if (areaLat > -90.0 && areaLat < 90) {
            return GeoHash.encodeHash(areaLat, areaLong, len);
        }
        else return null;
	}
	
	protected static long timePeriodIndentification(Date ct, Integer interval){
		Long timecategory;
		Date temp = new Date(0, 0, 0, ct.getHours(), ct.getMinutes());
                //Integer med = interval/2;
		if(temp.getMinutes()%interval > 0 ) {
			timecategory = temp.getTime()+((interval-(temp.getMinutes()%interval))*60000);
			//System.out.println("> 5 "+pickupDate+" "+temp +" "+temp.getMinutes() +" "+new Date(timecategory));
		}
		else{
			timecategory = temp.getTime();
			//System.out.println("<= 5 "+pickupDate+" "+temp +" "+temp.getMinutes() +" "+new Date(timecategory));
		}
		//System.out.println("time period:" + timecategory);
		return timecategory;
		
	}
	
	protected static String[] splitLine(String line){
	    String temp = line.replaceAll("\\[","").replaceAll("\\]", "").replaceAll("\\(", "").replaceAll("\\)", "");
	    String[] res = temp.split(",");
	    return res;
	}
	
	public abstractSkyline(){
		pm = new ArrayList<ProfitabilityArea>();
	}
	
	protected static boolean checkIncomparable(ProfitabilityArea data, ProfitabilityArea comparator){
		if(data.getTripdistance() < comparator.getTripdistance()|| data.getSpendingTime() < comparator.getSpendingTime() || data.getProfit() > comparator.getProfit() || data.getDemandProbability() > comparator.getDemandProbability()){
	        return true; 
	    }
	    else return false;
	}
	
	protected static boolean checkDomination(ProfitabilityArea data, ProfitabilityArea comparator){
		if(data.getTripdistance() == comparator.getTripdistance() && data.getSpendingTime() == comparator.getSpendingTime() && data.getProfit() == comparator.getProfit() && data.getDemandProbability() == comparator.getDemandProbability()){
			return false;
		}
		else if(data.getTripdistance() <= comparator.getTripdistance() && data.getSpendingTime() <= comparator.getSpendingTime() && data.getProfit() >= comparator.getProfit() && data.getDemandProbability() >= comparator.getDemandProbability()){
			return true;
		}
		else return false;
	}

	protected static List<ProfitabilityArea> dominanceTest(List<ProfitabilityArea> skyline, List<ProfitabilityArea> data){
            List<ProfitabilityArea> pal = new ArrayList<>();
            for(ProfitabilityArea pad:data){
                for(ProfitabilityArea pas: skyline){
                    //System.out.println(pad.print());
                    //System.out.println(pas.print());
                    //System.out.println("=====================================");
                    if(checkDomination(pas, pad) ){
                        pal.add(pad);
                        break;
                    }
                }
            }
            data.removeAll(pal);
            return data;
        }
        
	protected static List<ProfitabilityArea> skyline(){
		return pm;
	}
	
	protected static List<ProfitabilityArea> merge(List<ProfitabilityArea> s1,List<ProfitabilityArea> s2){
		return null;
	}
}
