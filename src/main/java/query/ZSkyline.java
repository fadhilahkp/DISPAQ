package query;

import com.google.common.collect.Multimap;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import model.Checkpoint;
import model.ProfitabilityArea;

public class ZSkyline extends abstractSkyline{
	
        private static SortedMap<String,List<ProfitabilityArea>> category;

    static List<ProfitabilityArea> skyline(Multimap<String, ProfitabilityArea> category) {
        List<ProfitabilityArea> skyline,temp;
		//SortedMap<String, List<ProfitabilityArea>> category;

		skyline = new ArrayList<>();
		//category = new TreeMap<>();
//		Collections.sort(pm,new Comparator<ProfitabilityArea>(){
//				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
//					
//					return(o1.getZbin()).compareTo( o2.getZbin());
//				}
//			});
		//category = new ;
//		for (ProfitabilityArea profitabilityArea : pm) {
//			if(!category.containsKey(profitabilityArea.getZbin())){
//				temp = new ArrayList<>();
//				temp.add(profitabilityArea);
//				category.put(profitabilityArea.getZbin(), temp);
//			}else{
//				category.get(profitabilityArea.getZbin()).add(profitabilityArea);
//				//category.put(profitabilityArea.getZbin(), value)
//			}
//		}
		List<ProfitabilityArea> m;
		Set<String> s;
		s = category.keySet();
		Iterator i1 = s.iterator();
		String key;
		//List<Checkpoint> cp = new ArrayList<>();
		int ones = 0,other=0;
		while (i1.hasNext()) {
			key = (String) i1.next();
                        m  = (List<ProfitabilityArea>) category.get(key);
//			System.out.println(m.getKey());
//			System.out.println("============isi============");
//                        for (ProfitabilityArea pa : m.getValue()) {
//                             System.out.println(pa.print());
//                            if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                             
//                                        System.out.println("masuk");}
//                        }
//          for (Checkpoint pa : m.getValue()) {
//         		System.out.println(pa.print());
//         	}
			if(skyline.isEmpty()){
				if(m.size()==1){
					skyline.addAll(m);}
				else{
					skyline.addAll(BNL.skyline(m));
				}
				ProfitabilityArea a = m.get(0);
				//Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
				//cp.add(c);
			}else{
				ProfitabilityArea pa = m.get(0);
				Boolean remove = false;
				Boolean check = false;
				for(ProfitabilityArea c:skyline){
//                                    if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                                        System.out.println(pa.print());
//                                        System.out.println(c.print());
//                                    }
					if(pa.getProfitpos() > c.getProfitpos()&& pa.getDemandprobabilitypos() > c.getDemandprobabilitypos()&& pa.getSpendingtimpos() > c.getSpendingtimpos()&& pa.getTripdistancepos() > c.getTripdistancepos()){
						remove = true;
					break;}
					else if(pa.getProfitpos().equals(c.getProfitpos())  || pa.getDemandprobabilitypos().equals(c.getDemandprobabilitypos())  || pa.getSpendingtimpos().equals(c.getSpendingtimpos()) || pa.getTripdistancepos().equals(c.getTripdistancepos())){
						check=true;
                                                break;
					}
				}
				if(!remove && !check){
					if(m.size()==1){
						skyline.addAll(m);
					}else{
						skyline.addAll(BNL.skyline(m));
					}
//                                        skyline.addAll(m);
//					skyline = BNLSkyline(skyline);
					//ProfitabilityArea a = m.getValue().get(0);
					//Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
					//cp.add(c);
				}
                                else if(check){
//					skyline.addAll(m);
//					skyline = BNLSkyline(skyline);
                                        skyline.addAll(dominanceTest(skyline,BNL.skyline(m)));  

                                        //skyline.addAll(BNLSkyline(m.getValue()));
				}
//					if(m.getValue().size() == 1){
//						ones++;
//					}
//					else{other++;
//					//System.out.println(pa.getZbin()+" "+m.getValue().size());
//					}
//			}
//			System.out.println("============skyline temp============");
//	          for (ProfitabilityArea pa : skyline) {
//	       		System.out.println(pa.print());
//	       	}
		}}
//		System.out.println("one value "+ones);
//		System.out.println("more than one value "+other);
		return skyline;
    }
	public ZSkyline(){
		pm = new ArrayList<>();
	}
        
        public ZSkyline(List<ProfitabilityArea> pal){
		pm = new ArrayList<>(pal);
	}
        
        public ZSkyline(SortedMap<String, List<ProfitabilityArea>> category){
            ZSkyline.category = new TreeMap<>(category);
            //System.out.println("CATEGORY SIZE PARAMETER: "+category.size());
            //System.out.println("CATEGORY SIZE INSIDE: "+ZSkyline.category.size());
            //ZSkyline.category.putAll(category);
        }
//	protected static SortedMap<String, List<ProfitabilityArea>> computeZValue(List<ProfitabilityArea> pm){
//		 Long w,x,y,z;
//        List<ProfitabilityArea> temp = new ArrayList<ProfitabilityArea>();
//        
//        //System.out.println("division : "+distdiv+" "+timediv+" "+costdiv);
//        for(ProfitabilityArea pa:pm){
//                //System.out.println("value : "+rs.getTripdistance()+" "+rs.getTraveltime()+" "+rs.getCost());
//                //System.out.println("asli : "+rs.getTripdistance()/distdiv+" "+rs.getTraveltime()/timediv+" "+rs.getCost()/costdiv);
//            w = 255 -(long) Math.ceil(pa.getDemandProbability()*100L);
//            x = (long) Math.ceil(pa.getTripdistance()*10L);
//            y = (long) Math.ceil(pa.getSpendingTime()/10);
//            z = 255 -(long) Math.ceil(pa.getProfit()*10L);
//
//            
//            pa.setDemandprobabilitypos(w);
//            pa.setTripdistancepos(x);
//            pa.setSpendingtimpos(y);
//            pa.setProfitpos(z);
//            
//            
//            Long zvalue = mortonEncode_magicbits(w,x, y, z);
//            pa.setZbin(Long.toBinaryString(zvalue));
//                //if(!rs.getZskyline()){
//            for (ProfitabilityArea profitabilityArea : pm) {
//                if(!category.containsKey(profitabilityArea.getZbin())){
//                        temp = new ArrayList<>();
//                        temp.add(profitabilityArea);
//                        category.put(profitabilityArea.getZbin(), temp);
//                }else{
//                        category.get(profitabilityArea.getZbin()).add(profitabilityArea);
//                        //category.put(profitabilityArea.getZbin(), value)
//                }
//                    //rsl.add(pa);///} 
//                //else {
//                //    res.add(rs);
//                //}
//        }}
//        return category;
//	}
	
//	protected static Long splitBy4(long a){
//		Long x = a & 0xff; // we only look at the first 21 bits
//		//x = (x | x << 24) & new BigInteger("ff000000ff",16).longValue();  // shift left 32 bits, OR with self, and 00011111000000000000000000000000000000001111111111111111
//		x = (x | x << 12) & new BigInteger("f000f",16).longValue();  // shift left 32 bits, OR with self, and 00011111000000000000000011111111000000000000000011111111
//		x = (x | x << 6) & new BigInteger("3030303",16).longValue(); // shift left 32 bits, OR with self, and 0001000000001111000000001111000000001111000000001111000000000000
//		x = (x | x << 3) & new BigInteger("11111111",16).longValue(); // shift left 32 bits, OR with self, and 0001000011000011000011000011000011000011000011000011000100000000
//		return x;
//	}
//	 
//	protected static Long mortonEncode_magicbits(Long x, Long y, Long z, Long a){
//		Long answer = 0L;
//	    answer |= splitBy4(x) | splitBy4(y) << 1 | splitBy4(z) << 2 | splitBy4(a) << 3;
//	    return answer;
//	}
	
//	public static List<ProfitabilityArea> skyline(){
//		List<ProfitabilityArea> skyline,temp;
//		SortedMap<String, List<ProfitabilityArea>> category;
//
//		skyline = new ArrayList<ProfitabilityArea>();
//		category = new TreeMap<String, List<ProfitabilityArea>>();
////		Collections.sort(pm,new Comparator<ProfitabilityArea>(){
////				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
////					
////					return(o1.getZbin()).compareTo( o2.getZbin());
////				}
////			});
//                pm = computeZValue();
////category = new ;
//		for (ProfitabilityArea profitabilityArea : pm) {
//			if(!category.containsKey(profitabilityArea.getZbin())){
//				temp = new ArrayList<ProfitabilityArea>();
//				temp.add(profitabilityArea);
//				category.put(profitabilityArea.getZbin(), temp);
//			}else{
//				category.get(profitabilityArea.getZbin()).add(profitabilityArea);
//				//category.put(profitabilityArea.getZbin(), value)
//			}
//		}
//		Map.Entry<String,List<ProfitabilityArea>> m;
//		Set s;
//		s = category.entrySet();
//		Iterator i1 = s.iterator();
//		
//		List<Checkpoint> cp = new ArrayList<Checkpoint>();
//		
//		while (i1.hasNext()) {
//			m = (Map.Entry<String,List<ProfitabilityArea>>) i1.next();
////			System.out.println(m.getKey());
////			System.out.println("============isi============");
////	      for (ProfitabilityArea pa : m.getValue()) {
////	   		System.out.println(pa.print());
////	   	}
////	      for (Checkpoint pa : m.getValue()) {
////	     		System.out.println(pa.print());
////	     	}
//			if(skyline.isEmpty()){
//				if(m.getValue().size()==1){
//					skyline.addAll(m.getValue());}
//				else{
//					skyline = dominanceTest(m.getValue(), skyline);
//				}
//				ProfitabilityArea a = m.getValue().get(0);
//				Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
//				cp.add(c);
//			}else{
//				ProfitabilityArea pa = m.getValue().get(0);
//				Boolean remove = false;
//				Boolean check = false;
//				for(Checkpoint c:cp){
//					if(pa.getProfitpos() > c.getProfit() && pa.getDemandprobabilitypos() > c.getDemandProbability() && pa.getSpendingtimpos() > c.getSpendingTime() && pa.getTripdistancepos() > c.getTripdistance()){
//						remove = true;
//					break;}
//					else if(pa.getProfitpos().equals(c.getProfit())  || pa.getDemandprobabilitypos().equals(c.getDemandProbability())  || pa.getSpendingtimpos().equals(c.getSpendingTime()) || pa.getTripdistancepos().equals(c.getTripdistance())){
//						check=true;
//					}
//				}
//				if(!remove && !check){
//					if(m.getValue().size()==1){
//						skyline.addAll(m.getValue());
//					}else{
//						skyline = dominanceTest(m.getValue(), skyline);
//					}
//					ProfitabilityArea a = m.getValue().get(0);
//					Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
//					cp.add(c);
//				}else if(check){
//					skyline = dominanceTest(m.getValue(), skyline);
//				}
//			}
////			System.out.println("============skyline temp============");
////	          for (ProfitabilityArea pa : skyline) {
////	       		System.out.println(pa.print());
////	       	}
//		}
//		
//		return skyline;
//	}
        
        	public static List<ProfitabilityArea> skyline(){
		//HashMap<String, List<ProfitabilityArea>> resHash = new HashMap<>();
		List<ProfitabilityArea> skyline,temp;
		//SortedMap<String, List<ProfitabilityArea>> category;
                //pm = computeZValue();
		skyline = new ArrayList<>();
		//category = new TreeMap<>();
//		Collections.sort(pm,new Comparator<ProfitabilityArea>(){
//				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
//					
//					return(o1.getZbin()).compareTo( o2.getZbin());
//				}
//			});
		//category = new ;
//		for (ProfitabilityArea profitabilityArea : pm) {
//			if(!category.containsKey(profitabilityArea.getZbin())){
//				temp = new ArrayList<>();
//				temp.add(profitabilityArea);
//				category.put(profitabilityArea.getZbin(), temp);
//			}else{
//				category.get(profitabilityArea.getZbin()).add(profitabilityArea);
//				//category.put(profitabilityArea.getZbin(), value)
//			}
//		}
		Map.Entry<String,List<ProfitabilityArea>> m;
		Set s;
		s = category.entrySet();
		Iterator i1 = s.iterator();
		
		List<Checkpoint> cp = new ArrayList<>();
		int ones = 0,other=0;
		while (i1.hasNext()) {
			m = (Map.Entry<String,List<ProfitabilityArea>>) i1.next();
//			System.out.println(m.getKey());
//			System.out.println("============isi============");
//                        for (ProfitabilityArea pa : m.getValue()) {
//                             System.out.println(pa.print());
//                            if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                             
//                                        System.out.println("masuk");}
//                        }
//          for (Checkpoint pa : m.getValue()) {
//         		System.out.println(pa.print());
//         	}
                    BNL bnl = null;
                    if(m.getValue().size() > 1){
                        bnl = new BNL();
                    }
			if(skyline.isEmpty()){
				if(m.getValue().size()==1){
					skyline.addAll(m.getValue());}
				else{
                                        bnl.setPM(m.getValue());
					skyline.addAll(bnl.skyline());
				}
				ProfitabilityArea a = m.getValue().get(0);
				//Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
				//cp.add(c);
			}else{
				ProfitabilityArea pa = m.getValue().get(0);
				Boolean remove = false;
				Boolean check = false;
				for(ProfitabilityArea c:skyline){
//                                    if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                                        System.out.println(pa.print());
//                                        System.out.println(c.print());
//                                    }
					if(pa.getProfitpos() > c.getProfitpos()&& pa.getDemandprobabilitypos() > c.getDemandprobabilitypos()&& pa.getSpendingtimpos() > c.getSpendingtimpos()&& pa.getTripdistancepos() > c.getTripdistancepos()){
						remove = true;
					break;}
					else if(pa.getProfitpos().equals(c.getProfitpos())  || pa.getDemandprobabilitypos().equals(c.getDemandprobabilitypos())  || pa.getSpendingtimpos().equals(c.getSpendingtimpos()) || pa.getTripdistancepos().equals(c.getTripdistancepos())){
						check=true;
					}
				}
				if(!remove && !check){
                                    if(m.getValue().size()==1){
                                            skyline.addAll(m.getValue());
                                    }else{
                                        bnl.setPM(m.getValue());
                                        skyline.addAll(bnl.skyline());
                                    }
                                    ProfitabilityArea a = m.getValue().get(0);
                                    //Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
                                    //cp.add(c);
				}else if(check){
                                    skyline.addAll(m.getValue());
                                    bnl.setPM(skyline);
                                    skyline = bnl.skyline();
				}
                                else if(remove){
//                                    if(m.getValue().size() == 1){
//						ones++;
//					}
//					else{other++;}
                                    //for (ProfitabilityArea asd : m.getValue()) {
                                    //System.out.println(pa.getZbin()+" "+m.getValue().size());
                                    //}
                                }
			}
                        
//			System.out.println("============skyline temp============");
//	          for (ProfitabilityArea pa : skyline) {
//	       		System.out.println(pa.print());
//	       	}
		}
		//System.out.println("one value "+ones);
		//System.out.println("more than one value "+other);
		return skyline;
	}
}
