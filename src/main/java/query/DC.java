package query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import model.ProfitabilityArea;

public class DC extends abstractSkyline{
		public DC(){
		pm = new ArrayList<>();
	}
        
        public DC(List<ProfitabilityArea> pal){
		pm = new ArrayList<>(pal);
	}
	
        protected static boolean checkDominationDC(ProfitabilityArea data, ProfitabilityArea comparator){
		if(data.getTripdistance() < comparator.getTripdistance()|| data.getSpendingTime() < comparator.getSpendingTime() || data.getProfit() > comparator.getProfit() || data.getDemandProbability() > comparator.getDemandProbability()
	    		|| (data.getTripdistance() <= comparator.getTripdistance() && data.getSpendingTime() <= comparator.getSpendingTime() && data.getProfit() >= comparator.getProfit() && data.getDemandProbability() >= comparator.getDemandProbability())){
	        return true; 
	    }
	    else return false;
	}
	protected static List<ProfitabilityArea> sort(List<ProfitabilityArea> pm, Integer dim){
		if(dim == 4){
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o2.getProfit()).compareTo( o1.getProfit());
				}
			});
		} else if(dim==3){
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o2.getDemandProbability()).compareTo( o1.getDemandProbability());
				}
			});
		}else if(dim==2){
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o1.getSpendingTime()).compareTo( o2.getSpendingTime());
				}
			});
		}else{
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o1.getTripdistance()).compareTo( o2.getTripdistance());
				}
			});
		}
		return pm;
	}
	
	protected static boolean partition(ProfitabilityArea pm, ProfitabilityArea med, Integer dim){
		if(dim == 4){
			if(pm.getProfit() > med.getProfit()) return true;
			else return false;
		} else if(dim==3){
			if(pm.getDemandProbability() > med.getDemandProbability()) return true;
			else return false;
		}else if(dim==2){
			if(pm.getSpendingTime() < med.getSpendingTime()) return true;
			else return false;
		}else{
			if(pm.getTripdistance() < med.getTripdistance()) return true;
			else return false;
		}
		//returnn false;
	}
	
//	public List<ProfitabilityArea> skyline(List<ProfitabilityArea> pml,Integer dim){
//		List<ProfitabilityArea> p1,p2,s1,s2,t;
//	    /*System.out.println("pm list");
//		for (RouteSummary routeSummary : pm) {
//			System.out.println(routeSummary.print());
//		}*/
//		if(pml.size() == 1){
//			return pml;
//		}
//		//median = dp
//		List<ProfitabilityArea> pm=sort(pml, dim);
//		ProfitabilityArea median = pm.get(pm.size()/2-1);
//		//partition
//		
//		p1 = new ArrayList<ProfitabilityArea>();
//		p2 = new ArrayList<ProfitabilityArea>();
//		for (ProfitabilityArea profitabilityArea : pml) {
//			if(profitabilityArea.equals(median))p1.add(profitabilityArea);
//			else
//			if(partition(profitabilityArea, median, dim)) p1.add(profitabilityArea);
//			else p2.add(profitabilityArea);
//		}
//		
////		System.out.println("============P1============");
////        for (ProfitabilityArea pa : p1) {
////     		System.out.println(pa.print());
////     	}
////        System.out.println("============P2============");
////        for (ProfitabilityArea pa : p2) {
////     		System.out.println(pa.print());
////     	}
//		s1 = new ArrayList<ProfitabilityArea>();
//		s2 = new ArrayList<ProfitabilityArea>();
//		//System.out.println("------------------------------------------------------------------------------------");
//		if(!p1.isEmpty()){
//			s1 = skyline(p1,dim);}
//		if(!p2.isEmpty()){
//			s2 = skyline(p2,dim);}
////		System.out.println("============S1============");
////        for (ProfitabilityArea pa : s1) {
////     		System.out.println(pa.print());
////     	}
////        System.out.println("============S2============");
////        for (ProfitabilityArea pa : s2) {
////     		System.out.println(pa.print());
////     	}
//		s1.addAll(merge(s1, s2,dim));
//        //t = mergeSkyline(s1, s2,dim);
////		System.out.println("============RES============");
////        for (ProfitabilityArea pa : s1) {
////     		System.out.println(pa.print());
////     	}
//	    //return t;
//	    return s1;
//	}
//	
//	protected List<ProfitabilityArea> merge(List<ProfitabilityArea> s1,List<ProfitabilityArea> s2, Integer dim){
//		List<ProfitabilityArea> rsl,r1,r2,r3,s11,s12,s21,s22;
//        rsl = new ArrayList<ProfitabilityArea>();
//        
//        if (s1.size() == 1) {
//             for (ProfitabilityArea pm : s2) {
//                if(checkDomination(pm, s1.get(0)) || checkIncomparable(pm, s1.get(0))){
//                    rsl.add(pm);
//                }
//            }
//            return rsl;
//        } else if (s2.size() == 1) {
//        	for (ProfitabilityArea pm : s1) {
//              if(!(checkDomination(s2.get(0),pm) || checkIncomparable(s2.get(0), pm))){
//                  return rsl;
//              }
//            }
//            return s2;
//        } else if(dim == 2){
//        	List<ProfitabilityArea> pal = sort(s1, 2);
//        	///2 spending time
//        	// 1 trip distance
//        	ProfitabilityArea temp = pal.get(pal.size()-1);
//        	
//        	for (ProfitabilityArea pm : s2) {
//                if(checkDomination(pm, temp) || checkIncomparable(pm, temp)){
//                    rsl.add(pm);
//                }
//            }
//        	return rsl;
//        }
//        else{
//
//        	List<ProfitabilityArea> temp = sort(s1,dim-1);
//        	Integer med = temp.size()/2-1;
//        	ProfitabilityArea rs = s1.get(med);
//        	
//        	s11 = new ArrayList<ProfitabilityArea>();
//            s12 = new ArrayList<ProfitabilityArea>();
//            for (ProfitabilityArea profitabilityArea : s1) {
//            	if(profitabilityArea.equals(rs))s11.add(profitabilityArea);
//            	else if(partition(profitabilityArea, rs,dim-1)) s11.add(profitabilityArea);
//				else s12.add(profitabilityArea);
//			}
//        
//            s21 = new ArrayList<ProfitabilityArea>();
//            s22 = new ArrayList<ProfitabilityArea>();
//
//            for (ProfitabilityArea profitabilityArea : s2) {
//				if(partition(profitabilityArea, rs,dim-1)) s21.add(profitabilityArea);
//				else s22.add(profitabilityArea);
//			}
////            System.out.println("========="+dim+"========");
////          System.out.println("============s11============");
////            for (ProfitabilityArea pa : s11) {
////         		System.out.println(pa.print());
////         	}
////            System.out.println("============s21============");
////            for (ProfitabilityArea pa : s21) {
////         		System.out.println(pa.print());
////         	}
////            System.out.println("============s12============");
////            for (ProfitabilityArea pa : s12) {
////         		System.out.println(pa.print());
////         	}
////            System.out.println("============s22============");
////            for (ProfitabilityArea pa : s22) {
////         		System.out.println(pa.print());
////         	}
//            r1 = merge(s11, s21, dim);
//            
//            r2 = merge(s12, s22, dim);
////            System.out.println("============s11============");
////            for (ProfitabilityArea pa : s11) {
////         		System.out.println(pa.print());
////         	}
////            System.out.println("============r2============");
////            for (ProfitabilityArea pa : r2) {
////         		System.out.println(pa.print());
////         	}
//            r3 = merge(s11, r2, dim-1);
////            System.out.println("========="+dim+"========");
////            System.out.println("============r1============");
////            for (ProfitabilityArea pa : r1) {
////         		System.out.println(pa.print());
////         	}
////           
////            System.out.println("============r3============");
////            for (ProfitabilityArea pa : r3) {
////         		System.out.println(pa.print());
////         	}
//            rsl.addAll(r1);
//            rsl.addAll(r3);
////            System.out.println("FINAL");
////            for (ProfitabilityArea routeSummary : rsl) {
////         		System.out.println(routeSummary.print());
////         	}
//            return rsl;
//        }
//	}
//}

	protected static List<ProfitabilityArea> skyline(List<ProfitabilityArea> pml, Integer dim){
		List<ProfitabilityArea> p1,p2,s1,s2,t;
	    /*System.out.println("pm list");
		for (RouteSummary routeSummary : pm) {
			System.out.println(routeSummary.print());
		}*/
		if(pml.size() == 1){
			return pml;
		}
		//median = dp
		List<ProfitabilityArea> pm=sort(pml, dim);
		ProfitabilityArea median = pm.get(pm.size()/2-1);
		//partition
		
		p1 = new ArrayList<ProfitabilityArea>();
		p2 = new ArrayList<ProfitabilityArea>();
		for (ProfitabilityArea profitabilityArea : pm) {
			if(profitabilityArea.equals(median))p1.add(profitabilityArea);
			else
			if(partition(profitabilityArea, median, dim)) p1.add(profitabilityArea);
			else p2.add(profitabilityArea);
		}
		
//		System.out.println("============P1============");
//        for (ProfitabilityArea pa : p1) {
//     		System.out.println(pa.print());
//     	}
//        System.out.println("============P2============");
//        for (ProfitabilityArea pa : p2) {
//     		System.out.println(pa.print());
//     	}
		s1 = new ArrayList<ProfitabilityArea>();
		s2 = new ArrayList<ProfitabilityArea>();
		//System.out.println("------------------------------------------------------------------------------------");
		if(!p1.isEmpty()){
			s1 = skyline(p1,dim);}
		if(!p2.isEmpty()){
			s2 = skyline(p2,dim);}
//		System.out.println("============S1============");
//        for (ProfitabilityArea pa : s1) {
//     		System.out.println(pa.print());
//     	}
//        System.out.println("============S2============");
//        for (ProfitabilityArea pa : s2) {
//     		System.out.println(pa.print());
//     	}
		s1.addAll(mergeSkyline(s1, s2,dim));
        //t = mergeSkyline(s1, s2,dim);
//		System.out.println("============RES============");
//        for (ProfitabilityArea pa : s1) {
//     		System.out.println(pa.print());
//     	}
	    //return t;
	    return s1;
		
		
	}

		protected static List<ProfitabilityArea> mergeSkyline(List<ProfitabilityArea> s1,List<ProfitabilityArea> s2, Integer dim){
		
	            List<ProfitabilityArea> rsl,r1,r2,r3,s11,s12,s21,s22;
	            rsl = new ArrayList<ProfitabilityArea>();
	            
	            if (s1.size() == 1) {
	                 for (ProfitabilityArea pm : s2) {
	                    if(checkDominationDC(pm, s1.get(0))){
	                        rsl.add(pm);
	                    }
	                }
	                return rsl;
	            } else if (s2.size() == 1) {
	            	for (ProfitabilityArea pm : s1) {
	                  if(!checkDominationDC(s2.get(0),pm)){
	                      return rsl;
	                  }
	                }
	                return s2;
	            } else if(dim == 2){
	            	List<ProfitabilityArea> pal1 = sort(s1, 2);
	            	List<ProfitabilityArea> pal2 = sort(s1, 1);
	            	///2 spending time
	            	// 1 trip distance
	            	//System.out.println("COMPARE");
	            	ProfitabilityArea temp = pal1.get(0);	            	
	            	ProfitabilityArea temp2 = pal2.get(0);
	            	for (ProfitabilityArea pm : s2) {
	            	//	System.out.println(temp.print());
	            	//	System.out.println(pm.print());
	            		
	                    if((checkDominationDC(pm, temp) && checkDominationDC(pm,temp2))){
	                        rsl.add(pm);
	                     //   System.out.println("not dominated");
	                    }
	                   // System.out.println("=====================");
	                }
	            	
	            	return rsl;
	            }
	            else{

	            	List<ProfitabilityArea> temp = sort(s1,dim-1);
	            	Integer med = temp.size()/2-1;
	            	ProfitabilityArea rs = s1.get(med);
	            	
	            	s11 = new ArrayList<ProfitabilityArea>();
	                s12 = new ArrayList<ProfitabilityArea>();
	                for (ProfitabilityArea profitabilityArea : s1) {
//	                	if(profitabilityArea.getAreacode().equals("dr5rsmb") || profitabilityArea.getAreacode().equals("dr5ru4x"))
//	                		{System.out.println(profitabilityArea.getAreacode());
//	                		System.out.println();
//	                		}
	                	if(profitabilityArea.equals(rs))s11.add(profitabilityArea);
	                	else if(partition(profitabilityArea, rs,dim-1)) s11.add(profitabilityArea);
						else s12.add(profitabilityArea);
					}
                
	                s21 = new ArrayList<ProfitabilityArea>();
	                s22 = new ArrayList<ProfitabilityArea>();

	                for (ProfitabilityArea profitabilityArea : s2) {
//	                	if(profitabilityArea.getAreacode().equals("dr5rsmb") || profitabilityArea.getAreacode().equals("dr5ru4x") )
//	                		{System.out.print(profitabilityArea.getAreacode());
//	                		System.out.println();}
						if(partition(profitabilityArea, rs,dim-1)) s21.add(profitabilityArea);
						else s22.add(profitabilityArea);
					}
//	                System.out.println("========="+dim+"========");
//	              System.out.println("============s11============");
//	                for (ProfitabilityArea pa : s11) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============s21============");
//	                for (ProfitabilityArea pa : s21) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============s12============");
//	                for (ProfitabilityArea pa : s12) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============s22============");
//	                for (ProfitabilityArea pa : s22) {
//	             		System.out.println(pa.print());
//	             	}
	                r1 = mergeSkyline(s11, s21, dim);
	                
	                r2 = mergeSkyline(s12, s22, dim);
//	                System.out.println("============s11============");
//	                for (ProfitabilityArea pa : s11) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============r2============");
//	                for (ProfitabilityArea pa : r2) {
//	             		System.out.println(pa.print());
//	             	}
	                r3 = mergeSkyline(s11, r2, dim-1);
//	                System.out.println("========="+dim+"========");
//	                System.out.println("============r1============");
//	                for (ProfitabilityArea pa : r1) {
//	             		System.out.println(pa.print());
//	             	}
//	               
//	                System.out.println("============r3============");
//	                for (ProfitabilityArea pa : r3) {
//	             		System.out.println(pa.print());
//	             	}
	                rsl.addAll(r1);
	                rsl.addAll(r3);
//	                System.out.println("FINAL");
//	                for (ProfitabilityArea routeSummary : rsl) {
//	             		System.out.println(routeSummary.print());
//	             	}
	                return rsl;
	            }
		}
}