package query;

import com.google.common.base.Supplier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.bson.BSONObject;
import org.bson.types.BasicBSONList;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import model.*;
import static query.abstractSkyline.splitLine;
import scala.Tuple2;
import scala.Tuple3;

/**
 *
 * @author fadhilah
 * 
 * spark-submit 
    --master yarn-cluster 
    --executor-memory 4GB 
    --num-executors 4 
    --jars 
        /home/hduser/dispaq/lib/geo-0.7.1.jar,
        /home/hduser/dispaq/lib/grumpy-core-0.2.2.jar,
        /home/hduser/dispaq/lib/mongo-spark-connector_2.10-2.0.0-rc0.jar,
        /home/hduser/dispaq/lib/mongo-java-driver-3.3.0.jar,
        /home/hduser/dispaq/lib/mongo-hadoop-core-1.5.0.jar 
    --class "query.distributedZSkylineQueryOptUpgrade" 
    [application jar]                   ex: /home/hduser/dispaq/dispaq-1.0.jar 
    [current location]                  ex: -73.78201,40.64474 
    [pq-index collection in mongoDB]    ex: month30upgrade 
    [geohash code length]               ex: 6
    [interval time]                     ex: 10 
    [current time]                      ex: 1484062200000
    [partition]                         ex: 4
 */

public class distributedZSkylineQueryOptUpgrade extends ZSkyline{

	public static void main(String[] args) throws InterruptedException {
            Integer day,arealength,partition;
            Long time,timerange,limit,maxhour;
            String area;
            DBObject objtemp;
            Date currentDate, tempd;
            String currentLocation,dataBase,areaLength,timeInterval;
            BasicDBObject dayObject,timeObject,areaObject;

            currentLocation = args[0];
            dataBase = args[1];
            areaLength = args[2];
            timeInterval = args[3];
            currentDate = new Date(Long.parseLong(args[4]));
            partition = Integer.parseInt(args[5]);

            arealength = Integer.parseInt(areaLength);
            String[] temp = splitLine(currentLocation);
            area = defineArea(temp[1], temp[0], arealength);
            day = currentDate.getDay();
            tempd = new Date(0, 0, 0, currentDate.getHours(), currentDate.getMinutes());
            timerange = Integer.parseInt(timeInterval)*60000L;
            
            limit =tempd.getTime()+timerange;
            maxhour = tempd.getTime()+(timerange*24);
            time = timePeriodIndentification(tempd, Integer.parseInt(timeInterval));
            
            MongoClient mongoClient = new MongoClient("164.125.121.111",27020);
		//DB db = mongoClient.getDB("taxi_trip");
            DB dbs = mongoClient.getDB("taxi_trip");
            DBCollection coll = dbs.getCollection(dataBase);
            
            dayObject = new BasicDBObject("day",day);
            timeObject = new BasicDBObject("time", time);
            areaObject = new BasicDBObject("areacode", area);
            BasicDBList dbl = new BasicDBList();
            dbl.add(dayObject);
            dbl.add(timeObject);
            dbl.add(areaObject);
            DBObject search = new BasicDBObject();
            search.put("$and", dbl);
            DBCursor cursor = coll.find( search);
            List<DBObject> ldb = new ArrayList<>();
            while(cursor.hasNext()){
                ldb.add(cursor.next());
            }
            SparkConf conf = new SparkConf().setAppName("Upgrade Opt Z-Skyline - "+currentLocation+" "+areaLength+" "+timeInterval+" "+dataBase);
            
            JavaSparkContext sc = new JavaSparkContext(conf);
	    
            System.out.println("{'day':"+currentDate.getDay()+", 'time':"+time+", 'areacode': '"+area+"'}");
            
	    final Broadcast<Date> cdatebc = sc.broadcast(tempd);
	    final Broadcast<Integer> arealengthbc = sc.broadcast(arealength);
	    final Broadcast<Integer> timerangebc = sc.broadcast(Integer.parseInt(timeInterval));
            final Broadcast<Integer> partitionbc = sc.broadcast(partition);

	    System.out.println("SET BROADCAST VALUE");
	    JavaRDD<DBObject> data = sc.parallelize(ldb);
            JavaRDD<Iterable<ProfitabilityArea>> pqindex;
            pqindex = data.mapToPair(
                new PairFunction<DBObject, Tuple3<Integer, Long, String>, Iterable<ProfitabilityArea>>() {
                    @Override
                    public Tuple2<Tuple3<Integer, Long, String>, Iterable<ProfitabilityArea>> call(DBObject t) throws Exception {
                        BSONObject obj = t;
                        //System.out.println(obj.toString());
                        Integer day;
                        Long time,traveltime,mintime;
                        Date k;
                        String areacode,zbin,dropoffarea;
                        Boolean zskyline;
                        Double fareaverage,demandprobability, v, tripdistance,cost;
                        //Date k;
                        Tuple3<Integer, Long, String> key;
                        Tuple2<AreaSummary, Iterable<RouteSummary>> value;
                        AreaSummary as;
                        //HashMap<Date,Double> pickuptimeList = new HashMap<>();

                        day = Integer.parseInt(obj.get("day").toString());
                        time = Long.parseLong(obj.get("time").toString());
                        try{
                            areacode = obj.get("areacode").toString();}
                            catch (Exception e) {
                                    areacode = "";
                          }

                        key = new Tuple3<Integer, Long, String>(day,time,areacode);

                        List<ProfitabilityArea> rsl = new ArrayList<ProfitabilityArea>();

                        //as.print();
                        BasicBSONList route = (BasicBSONList) obj.get("route summary");
                        RouteSummary rs;
                        UpgradedSummary us;
                        for (Object object : route) {
                              BSONObject minareasummary,maxareasummary;
                              BSONObject o = (BSONObject) object;
                              //System.out.println(o.toString());
                              us = new UpgradedSummary();
                              rs = new RouteSummary();
                              try{
                                      dropoffarea = o.get("dropoffarea").toString().substring(0, arealengthbc.getValue());}
                                      catch (Exception e) {
                                              dropoffarea = "";
                                                                    }
                              tripdistance = Double.parseDouble(o.get("tripdistance").toString());
                              traveltime = Long.parseLong(o.get("traveltime").toString());
                              cost = Double.parseDouble(o.get("cost").toString());
                              //zskyline = Boolean.parseBoolean(o.get("skyline").toString());
                             // zbin = o.get("zbin").toString();
                              rs.setDay(day);
                              rs.setTime(time);
                              rs.setCount(1.0);
                              rs.setDropoffarea(new Area(dropoffarea));
                              //rs.setSkyline(zskyline);
                              //rs.setZbin(zbin);
                              rs.setCost(cost);
                              rs.setTraveltime(traveltime);
                              rs.setTripdistance(tripdistance);
                              us.setRs(rs);
                              //min area summary
                              minareasummary = (BSONObject) o.get("min");
                              try {
                                mintime = Long.parseLong(minareasummary.get("time").toString());
                                us.setMinTimePeriod(mintime);
                            } catch (Exception e) {
                                System.out.println("ERROR: "+minareasummary.toString());
                            }


                              fareaverage = Double.parseDouble(minareasummary.get("fareaverage").toString());
                              demandprobability = Double.parseDouble(minareasummary.get("demandprob").toString());
                              as = new AreaSummary(rs.getDropoffarea().getAreacode());
                              //as.setDay(day);
                              //as.setTime(time);
                              as.setDemandprob(demandprobability);
                              as.setCount(1.0);
                              as.setFare(fareaverage);

                              BasicBSONList pickuptimelist = (BasicBSONList) minareasummary.get("pickuptime");
                              for (Object ptl : pickuptimelist) {
                                  BSONObject pto = (BSONObject) ptl;
                                  k = new Date(Long.parseLong(pto.get("key").toString()));
                                  v = Double.parseDouble(pto.get("value").toString());
                                  as.addPickupTime(k, v);
                              }
                              us.setMinArea(as);

                              maxareasummary = (BSONObject) o.get("max");

                              mintime = Long.parseLong(maxareasummary.get("time").toString());
                              us.setMaxTimePeriod(mintime);

                              fareaverage = Double.parseDouble(maxareasummary.get("fareaverage").toString());
                              demandprobability = Double.parseDouble(maxareasummary.get("demandprob").toString());
                              as = new AreaSummary(rs.getDropoffarea().getAreacode());
                              //as.setDay(day);
                              //as.setTime(time);
                              as.setDemandprob(demandprobability);
                              as.setCount(1.0);
                              as.setFare(fareaverage);

                              BasicBSONList maxpickuptimelist = (BasicBSONList) maxareasummary.get("pickuptime");
                              for (Object ptl : maxpickuptimelist) {
                                  BSONObject pto = (BSONObject) ptl;
                                  k = new Date(Long.parseLong(pto.get("key").toString()));
                                  v = Double.parseDouble(pto.get("value").toString());
                                  as.addPickupTime(k, v);
                              }
                              us.setMaxArea(as);
                              ProfitabilityArea pa = us.createProfitabilityArea(cdatebc.getValue().getTime());
                              pa.setZbin();
                              //System.out.println(pa.print());
                              rsl.add(pa);
                                  //rs.print();
                      }
                       Collections.sort(rsl,new Comparator<ProfitabilityArea>() {
                            @Override
                            public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
                                return o1.getZvalue().compareTo(o2.getZvalue());
                            }
                        });
                       for(int i = 0; i < partitionbc.getValue();i++){
                           rsl.get(i).setZvalue(Long.parseLong(i+""));
                       }
                      Iterable<ProfitabilityArea> temp = rsl;

                      //System.out.println(as.print());
                      //value = new Tuple2<AreaSummary, IIterable<UpgradedSummary>>(as,temp);
                      return new Tuple2<Tuple3<Integer, Long, String>, Iterable<ProfitabilityArea>>(key,temp);

                    }
                }).values();
	      			
        JavaPairRDD<Integer, Iterable<ProfitabilityArea>> profitabilitymap = pqindex.flatMap(
            new FlatMapFunction<Iterable<ProfitabilityArea>, ProfitabilityArea>() {
                @Override
                public Iterable<ProfitabilityArea> call(Iterable<ProfitabilityArea> t) throws Exception {
                    return t;
                }
        }).groupBy(
            new Function<ProfitabilityArea, Integer>() {
                    public Integer call(ProfitabilityArea v1) throws Exception {
                        if(v1.getZvalue() < partitionbc.getValue()){
                            return Integer.parseInt(v1.getZvalue()+"");
                        }
                        else {return (int) (Math.random()*100%partitionbc.getValue());}
                    }
		},partitionbc.getValue());                  

        JavaRDD<ProfitabilityArea> localprofitableareas = profitabilitymap.map(
            new Function<Tuple2<Integer, Iterable<ProfitabilityArea>>, Iterable<ProfitabilityArea>>() {
                public Iterable<ProfitabilityArea> call(Tuple2<Integer, Iterable<ProfitabilityArea>> v1) throws Exception {

                    Multimap<String, ProfitabilityArea> category = Multimaps.newMultimap(
                      Maps.<String, Collection<ProfitabilityArea>>newTreeMap(/* your comparator here*/),
                      new Supplier<Collection<ProfitabilityArea>>() {
                      @Override
                      public Collection<ProfitabilityArea> get() {
                          return Lists.newArrayList();
                      }
                  });
                    for(ProfitabilityArea rs:v1._2()){
                          category.put(rs.getZbin(), rs); 
                  }
                    List<ProfitabilityArea> skylinel = ZSkyline.skyline(category);
                    
                    return skylinel;
                        }
        }).flatMap(
            new FlatMapFunction<Iterable<ProfitabilityArea>, ProfitabilityArea>() {
                    @Override
                    public Iterable<ProfitabilityArea> call(Iterable<ProfitabilityArea> t) throws Exception {
                        return t;
                    }
            });
                
        JavaRDD<Iterable<ProfitabilityArea>> mergeLocalProfitableArea = localprofitableareas.groupBy(
            new Function<ProfitabilityArea, Integer>() {
                public Integer call(ProfitabilityArea v1) throws Exception {
                    return 1;
                }
            },1).values();

        JavaRDD<Iterable<ProfitabilityArea>> globalprofitableareas = mergeLocalProfitableArea.map(
            new Function<Iterable<ProfitabilityArea>, Iterable<ProfitabilityArea>>() {
                @Override
                public Iterable<ProfitabilityArea> call(Iterable<ProfitabilityArea> v1) throws Exception {
                    List<ProfitabilityArea> rsl = new ArrayList<ProfitabilityArea>();
                    Multimap<String, ProfitabilityArea> category = Multimaps.newMultimap(
                      Maps.<String, Collection<ProfitabilityArea>>newTreeMap(/* your comparator here*/),
                      new Supplier<Collection<ProfitabilityArea>>() {
                      @Override
                      public Collection<ProfitabilityArea> get() {
                          return Lists.newArrayList();
                      }
                    });
                    for(ProfitabilityArea rs:v1){
                          category.put(rs.getZbin(), rs); 
                    }
                    List<ProfitabilityArea> skylinel = ZSkyline.skyline(category);
                    
                    return skylinel;
                }
            });
        
        JavaRDD<ProfitabilityArea> pa = globalprofitableareas.flatMap(
            new FlatMapFunction<Iterable<ProfitabilityArea>, ProfitabilityArea>() {
                public Iterable<ProfitabilityArea> call(Iterable<ProfitabilityArea> t) throws Exception {

                        return t;
                }
        });
                              
        List<ProfitabilityArea> pal = pa.collect();
        System.out.println("===========================OUTPUT===========================");
        for (ProfitabilityArea profitabilityArea : pal) {
            System.out.println(profitabilityArea.print());
        }
        System.out.println("=============================================================");
            sc.stop();
    }

}
