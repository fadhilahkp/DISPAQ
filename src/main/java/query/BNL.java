package query;

import java.util.ArrayList;
import java.util.List;

import model.ProfitabilityArea;

public class BNL extends abstractSkyline{
	
        public BNL(){
		pm = new ArrayList<>();
	}
        
        public BNL(List<ProfitabilityArea> pal){
		pm = new ArrayList<>(pal);
	}
	
        public static void setPM(List<ProfitabilityArea> pal){
            pm = pal;
        }
	public static List<ProfitabilityArea> skyline(List<ProfitabilityArea> pm){
		List<ProfitabilityArea> removedResult,temporaryResult,result,pruned = new ArrayList<ProfitabilityArea>();
		
//		System.out.println("============PM============");
//	    for (ProfitabilityArea pa : pm) {
//	 		System.out.println(pa.print());
//	 	}
		result = new ArrayList<ProfitabilityArea>();
		pm.removeAll(pruned);
		for (ProfitabilityArea profitabilityArea : pm) {
			if(result.isEmpty()) {
				result.add(profitabilityArea);}
			else{
				temporaryResult = new ArrayList<ProfitabilityArea>();
				removedResult = new ArrayList<ProfitabilityArea>();
				Integer incomparable=0;
				
				for (ProfitabilityArea profitabilityAreaResult : result) {
					//System.out.println("============COMPARISON============");
			        //for (ProfitabilityArea pa : t) {
			     		//System.out.println(profitabilityArea.print());
			     		//System.out.println(profitabilityAreaResult.print());
			     	//}
			     	if(checkDomination(profitabilityArea, profitabilityAreaResult)){
						//System.out.println("Dominant");
						if(!temporaryResult.contains(profitabilityArea)){
							temporaryResult.add(profitabilityArea);
						}
						incomparable--;
						removedResult.add(profitabilityAreaResult);
						//break;
						}
					else if(checkIncomparable(profitabilityArea, profitabilityAreaResult)){
						//System.out.println("Incomparable");
						incomparable++;
						//temporaryResult.add(profitabilityArea);
						//break;
					} else{incomparable--;}
				}
				if(incomparable == result.size()) result.add(profitabilityArea);
				result.addAll(temporaryResult);
				result.removeAll(removedResult);
//				System.out.println("============RES============");
//		        for (ProfitabilityArea pa : result) {
//		     		System.out.println(pa.print());
//		     	}
			}
		}
		return result;
	}
}
