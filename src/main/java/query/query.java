package query;

import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.util.Collection;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentSkipListMap;
import model.Checkpoint;
import model.ProfitabilityArea;
import org.apache.commons.collections.MultiMap;


public class query {

	protected static Long splitBy4(long a){
		Long x = a & 0xf; // we only look at the first 21 bits
		//x = (x | x << 24) & new BigInteger("ff000000ff",16).longValue();  // shift left 32 bits, OR with self, and 00011111000000000000000000000000000000001111111111111111
		//x = (x | x << 12) & new BigInteger("f000f000f000f",16).longValue();  // shift left 32 bits, OR with self, and 00011111000000000000000011111111000000000000000011111111
		x = (x | x << 6) & new BigInteger("303",16).longValue(); // shift left 32 bits, OR with self, and 0001000000001111000000001111000000001111000000001111000000000000
		x = (x | x << 3) & new BigInteger("1111",16).longValue(); // shift left 32 bits, OR with self, and 0001000011000011000011000011000011000011000011000011000100000000
		return x;
	}
	 
	protected static Long mortonEncode_magicbits(Long x, Long y, Long z, Long a){
		Long answer = 0L;
	    answer |= splitBy4(x) | splitBy4(y) << 1 | splitBy4(z) << 2 | splitBy4(a) << 3;
	    return answer;
	}
//	
//	protected static String[] splitLine(String line){;
//	    String temp = line.replaceAll("\\[","").replaceAll("\\]", "").replaceAll("\\(", "").replaceAll("\\)", "");
//	    String[] res = temp.split(",");
//	    return res;
//	}
//	
	protected static List<ProfitabilityArea> sort(List<ProfitabilityArea> pm, Integer dim){
		if(dim == 4){
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o2.getProfit()).compareTo( o1.getProfit());
				}
			});
		} else if(dim==3){
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o2.getDemandProbability()).compareTo( o1.getDemandProbability());
				}
			});
		}else if(dim==2){
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o1.getSpendingTime()).compareTo( o2.getSpendingTime());
				}
			});
		}else{
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o1.getTripdistance()).compareTo( o2.getTripdistance());
				}
			});
		}
		return pm;
	}
	
	protected static boolean partition(ProfitabilityArea pm, ProfitabilityArea med, Integer dim){
		if(dim == 4){
			if(pm.getProfit() > med.getProfit()) return true;
			else return false;
		} else if(dim==3){
			if(pm.getDemandProbability() > med.getDemandProbability()) return true;
			else return false;
		}else if(dim==2){
			if(pm.getSpendingTime() < med.getSpendingTime()) return true;
			else return false;
		}else{
			if(pm.getTripdistance() < med.getTripdistance()) return true;
			else return false;
		}
		//returnn false;
	}
//	
//
//	
////	protected static boolean checkDomination(ProfitabilityArea data, ProfitabilityArea comparator){
////		if(data.getTripdistance() < comparator.getTripdistance()|| data.getSpendingTime() < comparator.getSpendingTime() || data.getProfit() > comparator.getProfit() || data.getDemandProbability() > comparator.getDemandProbability()
////	    		|| (data.getTripdistance() <= comparator.getTripdistance() && data.getSpendingTime() <= comparator.getSpendingTime() && data.getProfit() >= comparator.getProfit() && data.getDemandProbability() >= comparator.getDemandProbability())){
////	        return true; 
////	    }
////	    else return false;
////	}
	protected static boolean checkDominationDC(ProfitabilityArea data, ProfitabilityArea comparator){
		if(data.getTripdistance() < comparator.getTripdistance()|| data.getSpendingTime() < comparator.getSpendingTime() || data.getProfit() > comparator.getProfit() || data.getDemandProbability() > comparator.getDemandProbability()
	    		|| (data.getTripdistance() <= comparator.getTripdistance() && data.getSpendingTime() <= comparator.getSpendingTime() && data.getProfit() >= comparator.getProfit() && data.getDemandProbability() >= comparator.getDemandProbability())){
	        return true; 
	    }
	    else return false;
	}
        
        protected static List<ProfitabilityArea> dominanceTest(List<ProfitabilityArea> skyline, List<ProfitabilityArea> data){
            List<ProfitabilityArea> pal = new ArrayList<>();
            for(ProfitabilityArea pad:data){
                for(ProfitabilityArea pas: skyline){
                    //System.out.println(pad.print());
                    //System.out.println(pas.print());
                    //System.out.println("=====================================");
                    if(checkDomination(pas, pad) ){
                        pal.add(pad);
                        break;
                    }
                }
            }
            data.removeAll(pal);
            return data;
        }
//	protected static List<ProfitabilityArea> ZCheck(List<ProfitabilityArea> pm, Integer dim){
//		List<ProfitabilityArea> r,t = new ArrayList<ProfitabilityArea>();
//		boolean regI = false;
//		for(ProfitabilityArea pa:pm){
//			String a = pa.getZbin().substring(dim*4,(dim*4)+4);
//			//System.out.println(pa.getZbin()+" "+dim+" "+a);
//			if(Integer.parseInt(a,2) == 0) regI = true;
//			if(Integer.parseInt(a,2) == 15) t.add(pa);
//		}
////		System.out.println("early remove");
////		for(ProfitabilityArea p:t){
////			System.out.println(p.print());}
//		if(regI) {
//			//System.out.println("early remove 2");
//			//for(ProfitabilityArea p:t){System.out.println(p.print());}
//			pm.removeAll(t);
//			//System.out.println("++++++++++++++++++++++++");
//			}
//		return pm;
//	}
//	protected static List<ProfitabilityArea> ZSkyline(List<ProfitabilityArea> pm, Integer dim,Integer level,Integer totallevel){
//		List<ProfitabilityArea> p1,p2,s1,s2,t;
//		List<List<ProfitabilityArea>> p = new ArrayList<>();
//		List<List<ProfitabilityArea>> s = new ArrayList<>();
//
//		if(pm.size() <= 1){
//			return pm;
//		}else if(level == totallevel){
//			return skyline(pm, dim);
//		}
//
//		int maxvalue = (int) Math.pow(dim, 2);
//		
//		for(int i=0;i<maxvalue;i++){
//			List<ProfitabilityArea> pi = new ArrayList<>();
//			List<ProfitabilityArea> si = new ArrayList<>();
//			p.add(pi);
//			s.add(si);
//		}
//	
//		for(int i=0;i< pm.size();i++){
//                ProfitabilityArea rs = new ProfitabilityArea(pm.get(i));
//                Integer token = dim*level;
//                String partition = rs.getZbin().substring(token,token+dim);
//                Integer partposition = Integer.parseInt(partition, 2); 
//	            p.get(partposition).add(rs);
//		}
////		for(int i=0;i<maxvalue;i++){
////			System.out.println("============p"+i+"============");
////            for (ProfitabilityArea pa : p.get(i)) {
////         		System.out.println(pa.print());
////         	}
////		}
//		//check last region
//		if(!p.get(0).isEmpty()) {
//			p.get(maxvalue-1).clear();}
//		
//        for(int i=0;i<maxvalue;i++){
//        	if(!p.get(i).isEmpty()){
////        		 System.out.println("============"+i+"============");
////               for (ProfitabilityArea pa : p.get(i)) {
////            		System.out.println(pa.print());
////            	}
////               System.out.println(dim+" "+(level+1)+" "+totallevel);
//               List<ProfitabilityArea> temps =ZSkyline(p.get(i), dim, level+1, totallevel); 
//        		s.get(i).addAll(temps);
//        	}
//        }
//        
////        for(int i=0;i<maxvalue;i++){
////			System.out.println("============s"+i+"============");
////            for (ProfitabilityArea pa : s.get(i)) {
////         		System.out.println(pa.print());
////         	}
////		}
//        
//        t = new ArrayList<>();
//        Integer divisor = maxvalue;
//        Set<ProfitabilityArea> paset = new HashSet<>();
//        List<ProfitabilityArea> stemp,stemp2;
//        while(divisor>1){
//        	for(int i=0;i<divisor/2;i++){
//        		//System.out.println(divisor+" "+(i*2)+" "+(i*2)+1);
//        		stemp = new ArrayList<>(mergeZSkyline(s.get((i*2)), s.get((i*2)+1), dim));
//        		stemp2 = new ArrayList<>(mergeZSkyline(s.get((i*2)+1),s.get((i*2)), dim)); 
////        		System.out.println("+++++++++++++ stemp1 ++++++++++");
////        		for (ProfitabilityArea pa : stemp) {
////             		System.out.println(pa.print());
////             	}
////        		System.out.println("+++++++++++++ stemp2 ++++++++++");
////        		for (ProfitabilityArea pa : stemp2) {
////             		System.out.println(pa.print());
////             	}
//        		//paset.addAll(s.get(i-1));
//        		//paset.addAll(mergeZSkyline(s.get((i*2)-2), s.get((i*2)-1), dim));
//        		s.get(i).clear();
//        		s.get(i).addAll(stemp);
//        		s.get(i).addAll(stemp2);
////        		System.out.println("+++++++++++++ RES ++++++++++");
////        		for (ProfitabilityArea pa : s.get(i)) {
////             		System.out.println(pa.print());
////             	}
//        	}
//        	divisor/=2;
//        }
//        t.addAll(s.get(0));
////        System.out.println("============RES1============");
////        for (ProfitabilityArea pa : t) {
////     		System.out.println(pa.print());
////     	}
////        System.out.println("+++++++++++++ RES ++++++++++");
////        for (ProfitabilityArea pa : paset) {
////     		System.out.println(pa.print());
////     	}
//        //return t;
//	    return t;
//		
//		
//	}
//
//	protected static List<ProfitabilityArea> mergeZSkyline(List<ProfitabilityArea> s1,List<ProfitabilityArea> s2, Integer dim){
//		
//        List<ProfitabilityArea> rsl,r1,r2,r3,s11,s12,s21,s22;
//        rsl = new ArrayList<ProfitabilityArea>();
//        
////        System.out.println("============S1============");
////        for (ProfitabilityArea pa : s1) {
////    	//if(pa.getAreacode().equals("dr5ru1q") || pa.getAreacode().equals("dr5rskf"))test = true; 
//// 		System.out.println(pa.print());
////		}
////    System.out.println("============S2============");
////		for (ProfitabilityArea pa : s2) {
////    	//if(pa.getAreacode().equals("dr5ru1q") || pa.getAreacode().equals("dr5rskf"))test = true; 
//// 		System.out.println(pa.print());
//// 		}
//        
//        if(s2.isEmpty()) 
//        	return rsl;
//        else if(s1.isEmpty()) 
//        	return s2;
//        else if (s1.size() == 1) {
//             for (ProfitabilityArea pm : s2) {
//                if(checkDominationDC(pm, s1.get(0))){
//                    rsl.add(pm);
//                }
//            }
//             
//            return rsl;
//        } else if (s2.size() == 1) {
//        	for (ProfitabilityArea pm : s1) {
//              if(!(checkDominationDC(s2.get(0),pm))){
//                  return rsl;
//              }
//            }
//            return s2;
//        } else if(dim == 2){
//        	List<ProfitabilityArea> pal = sort(s1, 2);
//        	///2 spending time
//        	// 1 trip distance
//        	ProfitabilityArea temp = pal.get(pal.size()-1);
//        	
//        	for (ProfitabilityArea pm : s2) {
//                if(checkDominationDC(pm, temp)){
//                    rsl.add(pm);
//                }
//            }
//        	return rsl;
//        }
//        else{
//
//        	List<ProfitabilityArea> temp = sort(s1,dim-1);
//        	Integer med = temp.size()/2-1;
//        	ProfitabilityArea rs = s1.get(med);
//        	
//        	s11 = new ArrayList<ProfitabilityArea>();
//            s12 = new ArrayList<ProfitabilityArea>();
//            for (ProfitabilityArea profitabilityArea : s1) {
//            	if(profitabilityArea.equals(rs))s11.add(profitabilityArea);
//            	else if(partition(profitabilityArea, rs,dim-1)) s11.add(profitabilityArea);
//				else s12.add(profitabilityArea);
//			}
//        
//            s21 = new ArrayList<ProfitabilityArea>();
//            s22 = new ArrayList<ProfitabilityArea>();
//
//            for (ProfitabilityArea profitabilityArea : s2) {
//				if(partition(profitabilityArea, rs,dim-1)) s21.add(profitabilityArea);
//				else s22.add(profitabilityArea);
//			}
////            System.out.println("========="+dim+"========");
////          System.out.println("============s11============");
////            for (ProfitabilityArea pa : s11) {
////         		System.out.println(pa.print());
////         	}
////            System.out.println("============s21============");
////            for (ProfitabilityArea pa : s21) {
////         		System.out.println(pa.print());
////         	}
////            System.out.println("============s12============");
////            for (ProfitabilityArea pa : s12) {
////         		System.out.println(pa.print());
////         	}
////            System.out.println("============s22============");
////            for (ProfitabilityArea pa : s22) {
////         		System.out.println(pa.print());
////         	}
//            r1 = mergeZSkyline(s11, s21, dim);
//            r2 = mergeZSkyline(s12, s22, dim);
//            r3 = mergeZSkyline(s11, r2, dim-1);
//            rsl.addAll(r1);
//            rsl.addAll(r3);
//           // System.out.println("masuk dll");
////            for (RouteSummary routeSummary : rsl) {
////         		System.out.println(routeSummary.print());
////         	}
//            return rsl;
//        }
//}
//	
    
	protected static boolean checkIncomparable(ProfitabilityArea data, ProfitabilityArea comparator){
		if(data.getTripdistance() < comparator.getTripdistance()|| data.getSpendingTime() < comparator.getSpendingTime() || data.getProfit() > comparator.getProfit() || data.getDemandProbability() > comparator.getDemandProbability()){
	        return true; 
	    }
	    else return false;
	}
//	
	protected static boolean checkDomination(ProfitabilityArea data, ProfitabilityArea comparator){
		if(data.getTripdistance() == comparator.getTripdistance() && data.getSpendingTime() == comparator.getSpendingTime() && data.getProfit() == comparator.getProfit() && data.getDemandProbability() == comparator.getDemandProbability()){
			return false;
		}
		else if(data.getTripdistance() <= comparator.getTripdistance() && data.getSpendingTime() <= comparator.getSpendingTime() && data.getProfit() >= comparator.getProfit() && data.getDemandProbability() >= comparator.getDemandProbability()){
			return true;
		}
		else return false;
	}
//

    /**
     *
     * @param category
     * @return
     */
	protected static List<ProfitabilityArea> ZorderSkyline(Multimap category){
		//HashMap<String, List<ProfitabilityArea>> resHash = new HashMap<>();
		List<ProfitabilityArea> skyline,temp;
		//SortedMap<String, List<ProfitabilityArea>> category;

		skyline = new ArrayList<>();
		//category = new TreeMap<>();
//		Collections.sort(pm,new Comparator<ProfitabilityArea>(){
//				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
//					
//					return(o1.getZbin()).compareTo( o2.getZbin());
//				}
//			});
		//category = new ;
//		for (ProfitabilityArea profitabilityArea : pm) {
//			if(!category.containsKey(profitabilityArea.getZbin())){
//				temp = new ArrayList<>();
//				temp.add(profitabilityArea);
//				category.put(profitabilityArea.getZbin(), temp);
//			}else{
//				category.get(profitabilityArea.getZbin()).add(profitabilityArea);
//				//category.put(profitabilityArea.getZbin(), value)
//			}
//		}
		List<ProfitabilityArea> m;
		Set<String> s;
		s = category.keySet();
		Iterator i1 = s.iterator();
		String key;
		//List<Checkpoint> cp = new ArrayList<>();
		int ones = 0,other=0;
		while (i1.hasNext()) {
			key = (String) i1.next();
                        m  = (List<ProfitabilityArea>) category.get(key);
			//System.out.print(key+" "+m.size()+" "+m.get(0).print());
//			System.out.println("============isi============");
//                        for (ProfitabilityArea pa : m.getValue()) {
//                             System.out.println(pa.print());
//                            if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                             
//                                        System.out.println("masuk");}
//                        }
//          for (Checkpoint pa : m.getValue()) {
//         		System.out.println(pa.print());
//         	}
			if(skyline.isEmpty()){
				if(m.size()==1){
					skyline.addAll(m);}
				else{
					skyline.addAll(BNLSkyline(m));
				}
				ProfitabilityArea a = m.get(0);
				//Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
				//cp.add(c);
			}else{
				ProfitabilityArea pa = m.get(0);
				Boolean remove = false;
				Boolean check = false;
				for(ProfitabilityArea c:skyline){
//                                    if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                                        System.out.println(pa.print());
//                                        System.out.println(c.print());
//                                    }
					if(pa.getProfitpos() > c.getProfitpos()&& pa.getDemandprobabilitypos() > c.getDemandprobabilitypos()&& pa.getSpendingtimpos() > c.getSpendingtimpos()&& pa.getTripdistancepos() > c.getTripdistancepos()){
						remove = true;
                                                break;}
					else if(pa.getProfitpos().equals(c.getProfitpos())  || pa.getDemandprobabilitypos().equals(c.getDemandprobabilitypos())  || pa.getSpendingtimpos().equals(c.getSpendingtimpos()) || pa.getTripdistancepos().equals(c.getTripdistancepos())
                                                && !(pa.getProfitpos() < c.getProfitpos() || pa.getDemandprobabilitypos() < c.getDemandprobabilitypos() || pa.getSpendingtimpos() < c.getSpendingtimpos() || pa.getTripdistancepos() < c.getTripdistancepos())){
						check=true;
                                                break;
					}
				}
				if(!remove && !check){
					if(m.size()==1){
						skyline.addAll(m);
					}else{
						skyline.addAll(BNLSkyline(m));
					}
//                                        skyline.addAll(m);
//					skyline = BNLSkyline(skyline);
					//ProfitabilityArea a = m.getValue().get(0);
					//Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
					//cp.add(c);
                                        //System.out.println(" II");
				}
                                else if(check){
//					skyline.addAll(m);
//					skyline = BNLSkyline(skyline);
//                                        System.out.print(" size: "+m.size()+" "+BNLSkyline(m).size());
//                                        skyline.addAll(BNLSkyline(m));
                                      skyline.addAll(dominanceTest(skyline,BNLSkyline(m)));  
                                        //System.out.println(" III");
                                    
				}else if(remove){
                                    //System.out.println(" I");
//					if(m.getValue().size() == 1){
//						ones++;
//					}
//					else{other++;
//					//System.out.println(pa.getZbin()+" "+m.getValue().size());
//					}
                              }
//			System.out.println("============skyline temp============");
//	          for (ProfitabilityArea pa : skyline) {
//	       		System.out.println(pa.print());
//	       	}
		}}
//		System.out.println("one value "+ones);
//		System.out.println("more than one value "+other);
		return skyline;
	}
//	
//	protected static List<ProfitabilityArea> ZBNLSkyline(List<ProfitabilityArea> pm, Integer n,Integer l){
//		//HashMap<String, List<ProfitabilityArea>> resHash = new HashMap<>();
//		List<ProfitabilityArea> removedResult,temporaryResult,result,pruned = new ArrayList<ProfitabilityArea>();
//		HashMap<String,ProfitabilityArea> checkPruned;
//		List<String> checkReason;
//		Integer dimN,dimLevel,bitLenght;
//		dimN =n;
//		dimLevel=l;
//		bitLenght = pm.get(0).getZbin().length();
//		checkPruned = new HashMap<String,ProfitabilityArea>();
//		checkReason = new ArrayList<String>();
//		for (ProfitabilityArea profitabilityArea : pm) {
//			//boolean checkLastRegion = false;
//			String level = "";
//			for(int i=0;i<dimLevel;i++){
//				String tempvalue = profitabilityArea.getZbin().substring(i*dimN,(i*dimN)+dimN);
//				Integer ts = Integer.parseInt(tempvalue, 2);
//				
//				if(profitabilityArea.getZbin().substring(i*dimN,(i*dimN)+dimN).equals("1111")){
//					checkPruned.put(level,profitabilityArea);
////					}
//					break;
//				} else if(profitabilityArea.getZbin().substring(i*dimN,(i*dimN)+dimN).equals("0000")){
//					checkReason.add(level);
//				}
//				level+=ts.toString()+" ";
//				//level.c
////				}
//			}
//		}
//		Map.Entry<String,ProfitabilityArea> m;
//		Set s;
//		s = checkPruned.entrySet();
//		Iterator i1 = s.iterator();
//		
////		System.out.println("CHECK PRUNED");
////		while (i1.hasNext()) {
////			m = (Map.Entry<String,ProfitabilityArea>) i1.next();
////			System.out.println(m.getKey()+" "+m.getValue().print());
////		}
////		System.out.println("CHECK REASON");
////		for (String object : checkReason) {
////			System.out.println(object);
////		}
//		//System.out.println("Profitability map size: "+pm.size());
//		while (i1.hasNext()) {
//			m = (Map.Entry<String,ProfitabilityArea>) i1.next();
//			if(checkReason.contains(m.getKey())){
//				//System.out.println(m.getValue().print());
//				pm.remove(m.getValue());
//			}
//		}
//		result = new ArrayList<ProfitabilityArea>();
//		//pm.removeAll(pruned);
//		//System.out.println("============PM REMOVED============");
////        for (ProfitabilityArea pa : pm) {
////     		System.out.println(pa.print());
////     	}
//		for (ProfitabilityArea profitabilityArea : pm) {
//			if(result.isEmpty()) {
//				result.add(profitabilityArea);}
//			else{
//				temporaryResult = new ArrayList<ProfitabilityArea>();
//				removedResult = new ArrayList<ProfitabilityArea>();
//				Integer incomparable=0;
//				
//				for (ProfitabilityArea profitabilityAreaResult : result) {
//					//System.out.println("============COMPARISON============");
//			        //for (ProfitabilityArea pa : t) {
//			     		//System.out.println(profitabilityArea.print());
//			     		//System.out.println(profitabilityAreaResult.print());
//			     	//}
//			     	if(checkDomination(profitabilityArea, profitabilityAreaResult)){
//						//System.out.println("Dominant");
//						
//						if(!temporaryResult.contains(profitabilityArea)){
//							temporaryResult.add(profitabilityArea);
//						}
//						incomparable--;
//						removedResult.add(profitabilityAreaResult);
//						break;
//						}
//					else if(checkIncomparable(profitabilityArea, profitabilityAreaResult)){
//						//System.out.println("Incomparable");
//						incomparable++;
//						//temporaryResult.add(profitabilityArea);
//						//break;
//					} else{incomparable--;}
//				}
//				if(incomparable == result.size()) result.add(profitabilityArea);
//				result.addAll(temporaryResult);
//				result.removeAll(removedResult);
//
//			}
//		}
//		return result;
//	}
//	
	protected static List<ProfitabilityArea> skyline(List<ProfitabilityArea> pml, Integer dim){
		List<ProfitabilityArea> p1,p2,s1,s2,t;
	    /*System.out.println("pm list");
		for (RouteSummary routeSummary : pm) {
			System.out.println(routeSummary.print());
		}*/
		if(pml.size() == 1){
			return pml;
		}
		//median = dp
		List<ProfitabilityArea> pm=sort(pml, dim);
		ProfitabilityArea median = pm.get(pm.size()/2-1);
		//partition
		
		p1 = new ArrayList<ProfitabilityArea>();
		p2 = new ArrayList<ProfitabilityArea>();
		for (ProfitabilityArea profitabilityArea : pm) {
			if(profitabilityArea.equals(median))p1.add(profitabilityArea);
			else
			if(partition(profitabilityArea, median, dim)) p1.add(profitabilityArea);
			else p2.add(profitabilityArea);
		}
		
//		System.out.println("============P1============");
//        for (ProfitabilityArea pa : p1) {
//     		System.out.println(pa.print());
//     	}
//        System.out.println("============P2============");
//        for (ProfitabilityArea pa : p2) {
//     		System.out.println(pa.print());
//     	}
		s1 = new ArrayList<ProfitabilityArea>();
		s2 = new ArrayList<ProfitabilityArea>();
		//System.out.println("------------------------------------------------------------------------------------");
		if(!p1.isEmpty()){
			s1 = skyline(p1,dim);}
		if(!p2.isEmpty()){
			s2 = skyline(p2,dim);}
//		System.out.println("============S1============");
//        for (ProfitabilityArea pa : s1) {
//     		System.out.println(pa.print());
//     	}
//        System.out.println("============S2============");
//        for (ProfitabilityArea pa : s2) {
//     		System.out.println(pa.print());
//     	}
		s1.addAll(mergeSkyline(s1, s2,dim));
        //t = mergeSkyline(s1, s2,dim);
//		System.out.println("============RES============");
//        for (ProfitabilityArea pa : s1) {
//     		System.out.println(pa.print());
//     	}
	    //return t;
	    return s1;
		
		
	}

		protected static List<ProfitabilityArea> mergeSkyline(List<ProfitabilityArea> s1,List<ProfitabilityArea> s2, Integer dim){
		
	            List<ProfitabilityArea> rsl,r1,r2,r3,s11,s12,s21,s22;
	            rsl = new ArrayList<ProfitabilityArea>();
	            
	            if (s1.size() == 1) {
	                 for (ProfitabilityArea pm : s2) {
	                    if(checkDominationDC(pm, s1.get(0))){
	                        rsl.add(pm);
	                    }
	                }
	                return rsl;
	            } else if (s2.size() == 1) {
	            	for (ProfitabilityArea pm : s1) {
	                  if(!checkDominationDC(s2.get(0),pm)){
	                      return rsl;
	                  }
	                }
	                return s2;
	            } else if(dim == 2){
	            	List<ProfitabilityArea> pal1 = sort(s1, 2);
	            	List<ProfitabilityArea> pal2 = sort(s1, 1);
	            	///2 spending time
	            	// 1 trip distance
	            	//System.out.println("COMPARE");
	            	ProfitabilityArea temp = pal1.get(0);	            	
	            	ProfitabilityArea temp2 = pal2.get(0);
	            	for (ProfitabilityArea pm : s2) {
	            	//	System.out.println(temp.print());
	            	//	System.out.println(pm.print());
	            		
	                    if((checkDominationDC(pm, temp) && checkDominationDC(pm,temp2))){
	                        rsl.add(pm);
	                     //   System.out.println("not dominated");
	                    }
	                   // System.out.println("=====================");
	                }
	            	
	            	return rsl;
	            }
	            else{

	            	List<ProfitabilityArea> temp = sort(s1,dim-1);
	            	Integer med = temp.size()/2-1;
	            	ProfitabilityArea rs = s1.get(med);
	            	
	            	s11 = new ArrayList<ProfitabilityArea>();
	                s12 = new ArrayList<ProfitabilityArea>();
	                for (ProfitabilityArea profitabilityArea : s1) {
//                            if(profitabilityArea.getAreacode().equals("dr5rvnd") && profitabilityArea.getTime() == -2209093800000L && profitabilityArea.getDay() == 5 )
//                                {System.out.print(profitabilityArea.getAreacode());
//                                System.out.println();}
	                	if(profitabilityArea.equals(rs))s11.add(profitabilityArea);
	                	else if(partition(profitabilityArea, rs,dim-1)) s11.add(profitabilityArea);
						else s12.add(profitabilityArea);
					}
                
	                s21 = new ArrayList<ProfitabilityArea>();
	                s22 = new ArrayList<ProfitabilityArea>();

	                for (ProfitabilityArea profitabilityArea : s2) {
//                            if(profitabilityArea.getAreacode().equals("dr5rvnd") && profitabilityArea.getTime() == -2209093800000L && profitabilityArea.getDay() == 5 )
//                                    {System.out.print(profitabilityArea.getAreacode());
//                                    System.out.println();}
                            if(partition(profitabilityArea, rs,dim-1)) s21.add(profitabilityArea);
                            else s22.add(profitabilityArea);
					}
//	                System.out.println("========="+dim+"========");
//	              System.out.println("============s11============");
//	                for (ProfitabilityArea pa : s11) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============s21============");
//	                for (ProfitabilityArea pa : s21) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============s12============");
//	                for (ProfitabilityArea pa : s12) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============s22============");
//	                for (ProfitabilityArea pa : s22) {
//	             		System.out.println(pa.print());
//	             	}
	                r1 = mergeSkyline(s11, s21, dim);
	                
	                r2 = mergeSkyline(s12, s22, dim);
//	                System.out.println("============s11============");
//	                for (ProfitabilityArea pa : s11) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============r2============");
//	                for (ProfitabilityArea pa : r2) {
//	             		System.out.println(pa.print());
//	             	}
	                r3 = mergeSkyline(s11, r2, dim-1);
//	                System.out.println("========="+dim+"========");
//	                System.out.println("============r1============");
//	                for (ProfitabilityArea pa : r1) {
//	             		System.out.println(pa.print());
//	             	}
//	               
//	                System.out.println("============r3============");
//	                for (ProfitabilityArea pa : r3) {
//	             		System.out.println(pa.print());
//	             	}
	                rsl.addAll(r1);
	                rsl.addAll(r3);
//	                System.out.println("FINAL");
//	                for (ProfitabilityArea routeSummary : rsl) {
//	             		System.out.println(routeSummary.print());
//	             	}
	                return rsl;
	            }
		}
//		
		protected static List<ProfitabilityArea> BNLSkyline(List<ProfitabilityArea> pm){
			//HashMap<String, List<ProfitabilityArea>> resHash = new HashMap<>();
			List<ProfitabilityArea> removedResult,temporaryResult,result,pruned = new ArrayList<ProfitabilityArea>();
			
//			System.out.println("============PM============");
//	        for (ProfitabilityArea pa : pm) {
//	     		System.out.println(pa.print());
//	     	}
			result = new ArrayList<ProfitabilityArea>();
			pm.removeAll(pruned);
			for (ProfitabilityArea profitabilityArea : pm) {
				if(result.isEmpty()) {
					result.add(profitabilityArea);}
				else{
					temporaryResult = new ArrayList<ProfitabilityArea>();
					removedResult = new ArrayList<ProfitabilityArea>();
					Integer incomparable=0;
					
					for (ProfitabilityArea profitabilityAreaResult : result) {
						//System.out.println("============COMPARISON============");
				        //for (ProfitabilityArea pa : t) {
				     		//System.out.println(profitabilityArea.print());
				     		//System.out.println(profitabilityAreaResult.print());
				     	//}
				     	if(checkDomination(profitabilityArea, profitabilityAreaResult)){
							//System.out.println("Dominant");
							if(!temporaryResult.contains(profitabilityArea)){
								temporaryResult.add(profitabilityArea);
							}
							incomparable--;
							removedResult.add(profitabilityAreaResult);
							//break;
							}
						else if(checkIncomparable(profitabilityArea, profitabilityAreaResult)){
							//System.out.println("Incomparable");
							incomparable++;
							//temporaryResult.add(profitabilityArea);
							//break;
						} else{incomparable--;}
					}
					if(incomparable == result.size()) result.add(profitabilityArea);
					result.addAll(temporaryResult);
					result.removeAll(removedResult);
//					System.out.println("============RES============");
//			        for (ProfitabilityArea pa : result) {
//			     		System.out.println(pa.print());
//			     	}
				}
			}
			return result;
		}
	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		//Scanner scanner = new Scanner(new File("/Users/pankaj/abc.csv"));
        //scanner.useDelimiter(",");
            Scanner reader = new Scanner(System.in);
            MongoClient mongoClient = new MongoClient("164.125.121.111",27020);
            DB db = mongoClient.getDB("taxi_trip");
            DBCollection coll = db.getCollection("pm2");
            List<ProfitabilityArea> pml,rsl;

            int limit = reader.nextInt();
            //int r = reader.nextInt();
            while(limit!=0){
                for(int r=0;r<3;r++){
                    pml = new ArrayList<ProfitabilityArea>();
                    Long currenttime = System.currentTimeMillis();
                    ////System.out.println("division : "+distdiv+" "+timediv+" "+costdiv);
                    //SortedMap<String, List<ProfitabilityArea>> category = new TreeMap<>();
                    //Multimap<String,ProfitabilityArea> category = ArrayListMultimap.create();
                    Multimap<String, ProfitabilityArea> category = Multimaps.newMultimap(
                    Maps.<String, Collection<ProfitabilityArea>>newTreeMap(/* your comparator here*/),
                    new Supplier<Collection<ProfitabilityArea>>() {
                    @Override
                    public Collection<ProfitabilityArea> get() {
                        return Lists.newArrayList();
                    }
                });
                    //List<ProfitabilityArea> temp = new ArrayList<>();
                    //ListMultimap<String,ProfitabilityArea> category = MultimapBuilder;
                    //MultiMap<String,ProfitabilityArea> category = Multimaps.newMultimap(new TreeMap<>(Collections.reverseOrder()), Lists::newLinkedList);
                    DBCursor dbcursor = coll.find().limit(limit);
                    while(dbcursor.hasNext()){
                        //String[] split = splitLine(s);
                        DBObject o = dbcursor.next();

                        ProfitabilityArea pa = new ProfitabilityArea();
                        //pa.setZbin(o.get("zbin").toString());
                        pa.setDay(Integer.parseInt(o.get("day").toString()));
                        pa.setTime(Long.parseLong(o.get("time").toString()));
                        pa.setAreacode(o.get("areacode").toString());
                        pa.setProfit(Double.parseDouble(o.get("profit").toString()));
                        pa.setTripdistance(Double.parseDouble(o.get("tripdistance").toString()));
                        pa.setSpendingTime(Long.parseLong(o.get("spendingtime").toString()));
                        pa.setDemandProbability(Double.parseDouble(o.get("demandprobability").toString()));
                        pa.setZbin();

                        if(r==2){
                            //System.out.println(pa.print());
//                            if(!category.containsKey(pa.getZbin())){
//                                temp = new ArrayList<>();
//                                //temp.clear();
//                                temp.add(pa);
//                                category.put(pa.getZbin(), temp);
//                            }else{
//                                category.get(pa.getZbin()).add(pa);
//                                        //category.put(profitabilityArea.getZbin(), value)
//                            }
                            category.put(pa.getZbin(), pa);
                        }else{
                            pml.add(pa);
                        }
                    }
                    Long midtime = System.currentTimeMillis();
                    System.out.println((midtime-currenttime));

        //        double distdiv = tripdistance/64;
        //        long timediv = spendingtime/64;
        //        double profitdiv = profit/64;
        //        double demandprobdiv = demandprob/64;

        //		Collections.sort(pm,new Comparator<ProfitabilityArea>(){
        //				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
        //					
        //					return(o1.getZbin()).compareTo( o2.getZbin());
        //				}
        //			});
                        //category = new ;
                    List<ProfitabilityArea> skylinel = null;
                    //Integer r  = reader.nextInt();
                    //System.out.println("size:"+rsl.size());
                    if(r==2){
                           //ZSkyline zsky = new ZSkyline(rsl);
                           //skylinel = zsky.skyline();
                        skylinel = ZorderSkyline(category);
                    }
                    if(r==1){
    //                           DC dc = new DC(rsl);
    //                            skylinel = dc.skyline(rsl, 4);
                        skylinel = skyline(pml,4);
                    }
                    if(r==0){
    //                           BNL bnl = new BNL(rsl);
    //                            skylinel = bnl.skyline();
                        skylinel = BNLSkyline(pml);
                    }
        //        		if(r==4)
        //        			skylinel = ZSkyline(rsl, 4,0, 6);

                        //res.addAll(skylinel);     

        //                System.out.println("PROFITABLE AREAS");
        //            for (ProfitabilityArea a : skylinel) {
        //                    System.out.print(a.printcsv());
        //            }
                //System.out.println("PROFITABLE AREA SUMMARY SIZE: "+skylinel.size());
    //            System.out.println("------------------END--------------------");
                Long endtime = System.currentTimeMillis();
               System.out.println(endtime-midtime);
               //System.out.println("total: "+(endtime-currenttime));
            }
        System.out.println("------------------END--------------------");
        	limit = reader.nextInt();
        }
        mongoClient.close();
        
        
	}

}
