package query;

import com.google.common.base.Supplier;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;
import org.bson.BSONObject;

import com.mongodb.hadoop.MongoInputFormat;
import java.util.Collection;

import model.Checkpoint;
import model.ProfitabilityArea;
import static query.query.checkDomination;
import scala.Tuple2;
import scala.Tuple3;

public class distributedAllSkylineQuery {
	protected static Long splitBy4(long a){
		Long x = a & 0xffff; // we only look at the first 21 bits
		//x = (x | x << 24) & new BigInteger("ff000000ff",16).longValue();  // shift left 32 bits, OR with self, and 00011111000000000000000000000000000000001111111111111111
		x = (x | x << 12) & new BigInteger("f000f000f000f",16).longValue();  // shift left 32 bits, OR with self, and 00011111000000000000000011111111000000000000000011111111
		x = (x | x << 6) & new BigInteger("303030303030303",16).longValue(); // shift left 32 bits, OR with self, and 0001000000001111000000001111000000001111000000001111000000000000
		x = (x | x << 3) & new BigInteger("1111111111111111",16).longValue(); // shift left 32 bits, OR with self, and 0001000011000011000011000011000011000011000011000011000100000000
		return x;
	}
	 
	protected static Long mortonEncode_magicbits(Long x, Long y, Long z, Long a){
		Long answer = 0L;
	    answer |= splitBy4(x) | splitBy4(y) << 1 | splitBy4(z) << 2 | splitBy4(a) << 3;
	    return answer;
	}
//	
//	protected static String[] splitLine(String line){;
//	    String temp = line.replaceAll("\\[","").replaceAll("\\]", "").replaceAll("\\(", "").replaceAll("\\)", "");
//	    String[] res = temp.split(",");
//	    return res;
//	}
//	
	protected static List<ProfitabilityArea> sort(List<ProfitabilityArea> pm, Integer dim){
		if(dim == 4){
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o2.getProfit()).compareTo( o1.getProfit());
				}
			});
		} else if(dim==3){
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o2.getDemandProbability()).compareTo( o1.getDemandProbability());
				}
			});
		}else if(dim==2){
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o1.getSpendingTime()).compareTo( o2.getSpendingTime());
				}
			});
		}else{
			Collections.sort(pm, new Comparator<ProfitabilityArea>(){
				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
					
					return(o1.getTripdistance()).compareTo( o2.getTripdistance());
				}
			});
		}
		return pm;
	}
	
	protected static boolean partition(ProfitabilityArea pm, ProfitabilityArea med, Integer dim){
		if(dim == 4){
			if(pm.getProfit() > med.getProfit()) return true;
			else return false;
		} else if(dim==3){
			if(pm.getDemandProbability() > med.getDemandProbability()) return true;
			else return false;
		}else if(dim==2){
			if(pm.getSpendingTime() < med.getSpendingTime()) return true;
			else return false;
		}else{
			if(pm.getTripdistance() < med.getTripdistance()) return true;
			else return false;
		}
		//returnn false;
	}
//	
//
//	
////	protected static boolean checkDomination(ProfitabilityArea data, ProfitabilityArea comparator){
////		if(data.getTripdistance() < comparator.getTripdistance()|| data.getSpendingTime() < comparator.getSpendingTime() || data.getProfit() > comparator.getProfit() || data.getDemandProbability() > comparator.getDemandProbability()
////	    		|| (data.getTripdistance() <= comparator.getTripdistance() && data.getSpendingTime() <= comparator.getSpendingTime() && data.getProfit() >= comparator.getProfit() && data.getDemandProbability() >= comparator.getDemandProbability())){
////	        return true; 
////	    }
////	    else return false;
////	}
	protected static boolean checkDominationDC(ProfitabilityArea data, ProfitabilityArea comparator){
		if(data.getTripdistance() < comparator.getTripdistance()|| data.getSpendingTime() < comparator.getSpendingTime() || data.getProfit() > comparator.getProfit() || data.getDemandProbability() > comparator.getDemandProbability()
	    		|| (data.getTripdistance() <= comparator.getTripdistance() && data.getSpendingTime() <= comparator.getSpendingTime() && data.getProfit() >= comparator.getProfit() && data.getDemandProbability() >= comparator.getDemandProbability())){
	        return true; 
	    }
	    else return false;
	}

    
	protected static boolean checkIncomparable(ProfitabilityArea data, ProfitabilityArea comparator){
		if(data.getTripdistance() < comparator.getTripdistance()|| data.getSpendingTime() < comparator.getSpendingTime() || data.getProfit() > comparator.getProfit() || data.getDemandProbability() > comparator.getDemandProbability()){
	        return true; 
	    }
	    else return false;
	}
//	
	protected static boolean checkDomination(ProfitabilityArea data, ProfitabilityArea comparator){
		if(data.getTripdistance() == comparator.getTripdistance() && data.getSpendingTime() == comparator.getSpendingTime() && data.getProfit() == comparator.getProfit() && data.getDemandProbability() == comparator.getDemandProbability()){
			return false;
		}
		else if(data.getTripdistance() <= comparator.getTripdistance() && data.getSpendingTime() <= comparator.getSpendingTime() && data.getProfit() >= comparator.getProfit() && data.getDemandProbability() >= comparator.getDemandProbability()){
			return true;
		}
		else return false;
	}
//
        protected static List<ProfitabilityArea> dominanceTest(List<ProfitabilityArea> skyline, List<ProfitabilityArea> data){
            List<ProfitabilityArea> pal = new ArrayList<>();
            for(ProfitabilityArea pad:data){
                for(ProfitabilityArea pas: skyline){
                    //System.out.println(pad.print());
                    //System.out.println(pas.print());
                    //System.out.println("=====================================");
                    if(checkDomination(pas, pad) ){
                        pal.add(pad);
                        break;
                    }
                }
            }
            data.removeAll(pal);
            return data;
        }
                
        protected static List<ProfitabilityArea> ZorderSkyline(Multimap category){
		//HashMap<String, List<ProfitabilityArea>> resHash = new HashMap<>();
		List<ProfitabilityArea> skyline,temp;
		//SortedMap<String, List<ProfitabilityArea>> category;

		skyline = new ArrayList<>();
		//category = new TreeMap<>();
//		Collections.sort(pm,new Comparator<ProfitabilityArea>(){
//				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
//					
//					return(o1.getZbin()).compareTo( o2.getZbin());
//				}
//			});
		//category = new ;
//		for (ProfitabilityArea profitabilityArea : pm) {
//			if(!category.containsKey(profitabilityArea.getZbin())){
//				temp = new ArrayList<>();
//				temp.add(profitabilityArea);
//				category.put(profitabilityArea.getZbin(), temp);
//			}else{
//				category.get(profitabilityArea.getZbin()).add(profitabilityArea);
//				//category.put(profitabilityArea.getZbin(), value)
//			}
//		}
		List<ProfitabilityArea> m;
		Set<String> s;
		s = category.keySet();
		Iterator i1 = s.iterator();
		String key;
		//List<Checkpoint> cp = new ArrayList<>();
		int ones = 0,other=0;
		while (i1.hasNext()) {
			key = (String) i1.next();
                        m  = (List<ProfitabilityArea>) category.get(key);
//			System.out.println(m.getKey());
//			System.out.println("============isi============");
//                        for (ProfitabilityArea pa : m.getValue()) {
//                             System.out.println(pa.print());
//                            if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                             
//                                        System.out.println("masuk");}
//                        }
//          for (Checkpoint pa : m.getValue()) {
//         		System.out.println(pa.print());
//         	}
			if(skyline.isEmpty()){
				if(m.size()==1){
					skyline.addAll(m);}
				else{
					skyline.addAll(BNLSkyline(m));
				}
				ProfitabilityArea a = m.get(0);
				//Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
				//cp.add(c);
			}else{
				ProfitabilityArea pa = m.get(0);
				Boolean remove = false;
				Boolean check = false;
				for(ProfitabilityArea c:skyline){
//                                    if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                                        System.out.println(pa.print());
//                                        System.out.println(c.print());
//                                    }
					if(pa.getProfitpos() > c.getProfitpos()&& pa.getDemandprobabilitypos() > c.getDemandprobabilitypos()&& pa.getSpendingtimpos() > c.getSpendingtimpos()&& pa.getTripdistancepos() > c.getTripdistancepos()){
						remove = true;
					break;}
					else if(pa.getProfitpos().equals(c.getProfitpos())  || pa.getDemandprobabilitypos().equals(c.getDemandprobabilitypos())  || pa.getSpendingtimpos().equals(c.getSpendingtimpos()) || pa.getTripdistancepos().equals(c.getTripdistancepos())){
						check=true;
                                                break;
					}
				}
				if(!remove && !check){
					if(m.size()==1){
						skyline.addAll(m);
					}else{
						skyline.addAll(BNLSkyline(m));
					}
//                                        skyline.addAll(m);
//					skyline = BNLSkyline(skyline);
					//ProfitabilityArea a = m.getValue().get(0);
					//Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
					//cp.add(c);
				}
                                else if(check){
//					skyline.addAll(m);
//					skyline = BNLSkyline(skyline);
                                        skyline.addAll(dominanceTest(skyline,BNLSkyline(m)));  

                                        //skyline.addAll(BNLSkyline(m.getValue()));
				}
//					if(m.getValue().size() == 1){
//						ones++;
//					}
//					else{other++;
//					//System.out.println(pa.getZbin()+" "+m.getValue().size());
//					}
//			}
//			System.out.println("============skyline temp============");
//	          for (ProfitabilityArea pa : skyline) {
//	       		System.out.println(pa.print());
//	       	}
		}}
//		System.out.println("one value "+ones);
//		System.out.println("more than one value "+other);
		return skyline;
	}
        
        protected static List<ProfitabilityArea> ZorderSkyline(SortedMap<String, List<ProfitabilityArea>> category){
		//HashMap<String, List<ProfitabilityArea>> resHash = new HashMap<>();
		List<ProfitabilityArea> skyline,temp;
		//SortedMap<String, List<ProfitabilityArea>> category;

		skyline = new ArrayList<>();
		//category = new TreeMap<>();
//		Collections.sort(pm,new Comparator<ProfitabilityArea>(){
//				public int compare(ProfitabilityArea o1, ProfitabilityArea o2) {
//					
//					return(o1.getZbin()).compareTo( o2.getZbin());
//				}
//			});
		//category = new ;
//		for (ProfitabilityArea profitabilityArea : pm) {
//			if(!category.containsKey(profitabilityArea.getZbin())){
//				temp = new ArrayList<>();
//				temp.add(profitabilityArea);
//				category.put(profitabilityArea.getZbin(), temp);
//			}else{
//				category.get(profitabilityArea.getZbin()).add(profitabilityArea);
//				//category.put(profitabilityArea.getZbin(), value)
//			}
//		}
		Map.Entry<String,List<ProfitabilityArea>> m;
		Set s;
		s = category.entrySet();
		Iterator i1 = s.iterator();
		
		List<Checkpoint> cp = new ArrayList<>();
		int ones = 0,other=0;
		while (i1.hasNext()) {
			m = (Map.Entry<String,List<ProfitabilityArea>>) i1.next();
//			System.out.println(m.getKey());
//			System.out.println("============isi============");
//                        for (ProfitabilityArea pa : m.getValue()) {
//                             System.out.println(pa.print());
//                            if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                             
//                                        System.out.println("masuk");}
//                        }
//          for (Checkpoint pa : m.getValue()) {
//         		System.out.println(pa.print());
//         	}
			if(skyline.isEmpty()){
				if(m.getValue().size()==1){
					skyline.addAll(m.getValue());}
				else{
					skyline.addAll(BNLSkyline(m.getValue()));
				}
				ProfitabilityArea a = m.getValue().get(0);
				//Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
				//cp.add(c);
			}else{
				ProfitabilityArea pa = m.getValue().get(0);
				Boolean remove = false;
				Boolean check = false;
				for(ProfitabilityArea c:skyline){
//                                    if(pa.getAreacode().equals("dr72h89") && pa.getDay().equals(4) && pa.getTime().equals(-2209038600000L)){
//                                        System.out.println(pa.print());
//                                        System.out.println(c.print());
//                                    }
					if(pa.getProfitpos() > c.getProfitpos()&& pa.getDemandprobabilitypos() > c.getDemandprobabilitypos()&& pa.getSpendingtimpos() > c.getSpendingtimpos()&& pa.getTripdistancepos() > c.getTripdistancepos()){
						remove = true;
					break;}
					else if(pa.getProfitpos().equals(c.getProfitpos())  || pa.getDemandprobabilitypos().equals(c.getDemandprobabilitypos())  || pa.getSpendingtimpos().equals(c.getSpendingtimpos()) || pa.getTripdistancepos().equals(c.getTripdistancepos())){
						check=true;
                                                break;
					}
				}
				if(!remove && !check){
					if(m.getValue().size()==1){
						skyline.addAll(m.getValue());
					}else{
						skyline.addAll(BNLSkyline(m.getValue()));
					}
//                                        skyline.addAll(m.getValue());
//					skyline = BNLSkyline(skyline);
					//ProfitabilityArea a = m.getValue().get(0);
					//Checkpoint c = new Checkpoint(a.getProfitpos(), a.getTripdistancepos(), a.getSpendingtimpos(), a.getDemandprobabilitypos());
					//cp.add(c);
				}
                                else if(check){
					skyline.addAll(m.getValue());
					skyline = BNLSkyline(skyline);
                                    //skyline.addAll(BNLSkyline(m.getValue()));
				}
//                                else if(check){
//					skyline.addAll(m.getValue());
//					skyline = BNLSkyline(skyline);
//                                    //skyline.addAll(BNLSkyline(m.getValue()));
//				}
//                                else if(remove){
//					if(m.getValue().size() == 1){
//						ones++;
//					}
//					else{other++;
//					//System.out.println(pa.getZbin()+" "+m.getValue().size());
//					}
//			}
//			System.out.println("============skyline temp============");
//	          for (ProfitabilityArea pa : skyline) {
//	       		System.out.println(pa.print());
//	       	}
		}}
//		System.out.println("one value "+ones);
//		System.out.println("more than one value "+other);
		return skyline;
	}
	protected static List<ProfitabilityArea> skyline(List<ProfitabilityArea> pml, Integer dim){
		List<ProfitabilityArea> p1,p2,s1,s2,t;
	    /*System.out.println("pm list");
		for (RouteSummary routeSummary : pm) {
			System.out.println(routeSummary.print());
		}*/
		if(pml.size() == 1){
			return pml;
		}
		//median = dp
		List<ProfitabilityArea> pm=sort(pml, dim);
		ProfitabilityArea median = pm.get(pm.size()/2-1);
		//partition
		
		p1 = new ArrayList<ProfitabilityArea>();
		p2 = new ArrayList<ProfitabilityArea>();
		for (ProfitabilityArea profitabilityArea : pm) {
			if(profitabilityArea.equals(median))p1.add(profitabilityArea);
			else
			if(partition(profitabilityArea, median, dim)) p1.add(profitabilityArea);
			else p2.add(profitabilityArea);
		}
		
//		System.out.println("============P1============");
//        for (ProfitabilityArea pa : p1) {
//     		System.out.println(pa.print());
//     	}
//        System.out.println("============P2============");
//        for (ProfitabilityArea pa : p2) {
//     		System.out.println(pa.print());
//     	}
		s1 = new ArrayList<ProfitabilityArea>();
		s2 = new ArrayList<ProfitabilityArea>();
		//System.out.println("------------------------------------------------------------------------------------");
		if(!p1.isEmpty()){
			s1 = skyline(p1,dim);}
		if(!p2.isEmpty()){
			s2 = skyline(p2,dim);}
//		System.out.println("============S1============");
//        for (ProfitabilityArea pa : s1) {
//     		System.out.println(pa.print());
//     	}
//        System.out.println("============S2============");
//        for (ProfitabilityArea pa : s2) {
//     		System.out.println(pa.print());
//     	}
		s1.addAll(mergeSkyline(s1, s2,dim));
        //t = mergeSkyline(s1, s2,dim);
//		System.out.println("============RES============");
//        for (ProfitabilityArea pa : s1) {
//     		System.out.println(pa.print());
//     	}
	    //return t;
	    return s1;
		
		
	}

		protected static List<ProfitabilityArea> mergeSkyline(List<ProfitabilityArea> s1,List<ProfitabilityArea> s2, Integer dim){
		
	            List<ProfitabilityArea> rsl,r1,r2,r3,s11,s12,s21,s22;
	            rsl = new ArrayList<ProfitabilityArea>();
	            
	            if (s1.size() == 1) {
	                 for (ProfitabilityArea pm : s2) {
	                    if(checkDominationDC(pm, s1.get(0))){
	                        rsl.add(pm);
	                    }
	                }
	                return rsl;
	            } else if (s2.size() == 1) {
	            	for (ProfitabilityArea pm : s1) {
	                  if(!checkDominationDC(s2.get(0),pm)){
	                      return rsl;
	                  }
	                }
	                return s2;
	            } else if(dim == 2){
	            	List<ProfitabilityArea> pal1 = sort(s1, 2);
	            	List<ProfitabilityArea> pal2 = sort(s1, 1);
	            	///2 spending time
	            	// 1 trip distance
	            	//System.out.println("COMPARE");
	            	ProfitabilityArea temp = pal1.get(0);	            	
	            	ProfitabilityArea temp2 = pal2.get(0);
	            	for (ProfitabilityArea pm : s2) {
	            	//	System.out.println(temp.print());
	            	//	System.out.println(pm.print());
	            		
	                    if((checkDominationDC(pm, temp) && checkDominationDC(pm,temp2))){
	                        rsl.add(pm);
	                     //   System.out.println("not dominated");
	                    }
	                   // System.out.println("=====================");
	                }
	            	
	            	return rsl;
	            }
	            else{

	            	List<ProfitabilityArea> temp = sort(s1,dim-1);
	            	Integer med = temp.size()/2-1;
	            	ProfitabilityArea rs = s1.get(med);
	            	
	            	s11 = new ArrayList<ProfitabilityArea>();
	                s12 = new ArrayList<ProfitabilityArea>();
	                for (ProfitabilityArea profitabilityArea : s1) {
//                            if(profitabilityArea.getAreacode().equals("dr5rvnd") && profitabilityArea.getTime() == -2209093800000L && profitabilityArea.getDay() == 5 )
//                                {System.out.print(profitabilityArea.getAreacode());
//                                System.out.println();}
	                	if(profitabilityArea.equals(rs))s11.add(profitabilityArea);
	                	else if(partition(profitabilityArea, rs,dim-1)) s11.add(profitabilityArea);
						else s12.add(profitabilityArea);
					}
                
	                s21 = new ArrayList<ProfitabilityArea>();
	                s22 = new ArrayList<ProfitabilityArea>();

	                for (ProfitabilityArea profitabilityArea : s2) {
//                            if(profitabilityArea.getAreacode().equals("dr5rvnd") && profitabilityArea.getTime() == -2209093800000L && profitabilityArea.getDay() == 5 )
//                                    {System.out.print(profitabilityArea.getAreacode());
//                                    System.out.println();}
                            if(partition(profitabilityArea, rs,dim-1)) s21.add(profitabilityArea);
                            else s22.add(profitabilityArea);
					}
//	                System.out.println("========="+dim+"========");
//	              System.out.println("============s11============");
//	                for (ProfitabilityArea pa : s11) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============s21============");
//	                for (ProfitabilityArea pa : s21) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============s12============");
//	                for (ProfitabilityArea pa : s12) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============s22============");
//	                for (ProfitabilityArea pa : s22) {
//	             		System.out.println(pa.print());
//	             	}
	                r1 = mergeSkyline(s11, s21, dim);
	                
	                r2 = mergeSkyline(s12, s22, dim);
//	                System.out.println("============s11============");
//	                for (ProfitabilityArea pa : s11) {
//	             		System.out.println(pa.print());
//	             	}
//	                System.out.println("============r2============");
//	                for (ProfitabilityArea pa : r2) {
//	             		System.out.println(pa.print());
//	             	}
	                r3 = mergeSkyline(s11, r2, dim-1);
//	                System.out.println("========="+dim+"========");
//	                System.out.println("============r1============");
//	                for (ProfitabilityArea pa : r1) {
//	             		System.out.println(pa.print());
//	             	}
//	               
//	                System.out.println("============r3============");
//	                for (ProfitabilityArea pa : r3) {
//	             		System.out.println(pa.print());
//	             	}
	                rsl.addAll(r1);
	                rsl.addAll(r3);
//	                System.out.println("FINAL");
//	                for (ProfitabilityArea routeSummary : rsl) {
//	             		System.out.println(routeSummary.print());
//	             	}
	                return rsl;
	            }
		}
//		
		protected static List<ProfitabilityArea> BNLSkyline(List<ProfitabilityArea> pm){
			//HashMap<String, List<ProfitabilityArea>> resHash = new HashMap<>();
			List<ProfitabilityArea> removedResult,temporaryResult,result,pruned = new ArrayList<ProfitabilityArea>();
			
//			System.out.println("============PM============");
//	        for (ProfitabilityArea pa : pm) {
//	     		System.out.println(pa.print());
//	     	}
			result = new ArrayList<ProfitabilityArea>();
			pm.removeAll(pruned);
			for (ProfitabilityArea profitabilityArea : pm) {
				if(result.isEmpty()) {
					result.add(profitabilityArea);}
				else{
					temporaryResult = new ArrayList<ProfitabilityArea>();
					removedResult = new ArrayList<ProfitabilityArea>();
					Integer incomparable=0;
					
					for (ProfitabilityArea profitabilityAreaResult : result) {
						//System.out.println("============COMPARISON============");
				        //for (ProfitabilityArea pa : t) {
				     		//System.out.println(profitabilityArea.print());
				     		//System.out.println(profitabilityAreaResult.print());
				     	//}
				     	if(checkDomination(profitabilityArea, profitabilityAreaResult)){
							//System.out.println("Dominant");
							if(!temporaryResult.contains(profitabilityArea)){
								temporaryResult.add(profitabilityArea);
							}
							incomparable--;
							removedResult.add(profitabilityAreaResult);
							//break;
							}
						else if(checkIncomparable(profitabilityArea, profitabilityAreaResult)){
							//System.out.println("Incomparable");
							incomparable++;
							//temporaryResult.add(profitabilityArea);
							//break;
						} else{incomparable--;}
					}
					if(incomparable == result.size()) result.add(profitabilityArea);
					result.addAll(temporaryResult);
					result.removeAll(removedResult);
//					System.out.println("============RES============");
//			        for (ProfitabilityArea pa : result) {
//			     		System.out.println(pa.print());
//			     	}
				}
			}
			return result;
		}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String size = args[0];
		String method = args[1];
		SparkConf conf = new SparkConf().setAppName("Query Processing "+size+" "+method);
		Configuration mongodbConfig = new Configuration();
		mongodbConfig.set("mongo.job.input.format", "com.mongodb.hadoop.MongoInputFormat");
		mongodbConfig.set("mongo.input.uri", "mongodb://164.125.121.111:27020/taxi_trip."+size);
		//mongodbConfig.set("mongo.input.query","{'limit':"+size+"}");
		mongodbConfig.set("mongo.input.split_size", "100");
		//mongodbConfig.set("mongo.input.limit", size);
		JavaSparkContext sc = new JavaSparkContext(conf);
	    
                
	    final Broadcast<String> methodbc = sc.broadcast(method);
	    
	    JavaPairRDD<Object, BSONObject> data = sc.newAPIHadoopFile("asd", MongoInputFormat.class, Object.class, BSONObject.class, mongodbConfig);
	    
	    JavaRDD<ProfitabilityArea> profitablearea = data.map(new Function<Tuple2<Object,BSONObject>, ProfitabilityArea>() {

			public ProfitabilityArea call(Tuple2<Object, BSONObject> v1) throws Exception {
				BSONObject o = v1._2();
				ProfitabilityArea pa = new ProfitabilityArea();
	        	//pa.setZbin(o.get("zbin").toString());
	        	pa.setDay(Integer.parseInt(o.get("day").toString()));
	        	pa.setTime(Long.parseLong(o.get("time").toString()));
	        	pa.setAreacode(o.get("areacode").toString());
	        	pa.setProfit(Double.parseDouble(o.get("profit").toString()));
	        	pa.setTripdistance(Double.parseDouble(o.get("tripdistance").toString()));
	        	pa.setSpendingTime(Long.parseLong(o.get("spendingtime").toString()));
	        	pa.setDemandProbability(Double.parseDouble(o.get("demandprobability").toString()));
                        //if("0".equals(methodbc.getValue())) {
                            pa.setZbin();//}
	        	//pa = computeZValue(pa);
	        	//System.out.println(pa.print());

				return pa;
			}
		});
	    
	    JavaPairRDD<Integer, Iterable<ProfitabilityArea>> profitabilitymap = profitablearea.groupBy(new Function<ProfitabilityArea, Integer>() {

			public Integer call(ProfitabilityArea v1) throws Exception {
				int a = (int) (Math.random()*100%3);
				//System.out.println(a);
				return a;
			}
		},5);
	    
	    JavaRDD<ProfitabilityArea> localprofitableareas = profitabilitymap.map(new Function<Tuple2<Integer, Iterable<ProfitabilityArea>>, Iterable<ProfitabilityArea>>() {
            public Iterable<ProfitabilityArea> call(Tuple2<Integer, Iterable<ProfitabilityArea>> v1) throws Exception {
            	//System.out.println(x);
                List<ProfitabilityArea> skylinel,rsl,temp;
                skylinel = new ArrayList<ProfitabilityArea>();
                rsl = new ArrayList<ProfitabilityArea>();
                temp = new ArrayList<>();
                //SortedMap<String, List<ProfitabilityArea>> category = new TreeMap<String, List<ProfitabilityArea>>();
                //Multimap<String,ProfitabilityArea> category = ArrayListMultimap.create();
                //MultimapBuilder.treeKeys(/* you comparator here */).linkedListValues().build();
                Multimap<String, ProfitabilityArea> category = Multimaps.newMultimap(
                    Maps.<String, Collection<ProfitabilityArea>>newTreeMap(/* your comparator here*/),
                    new Supplier<Collection<ProfitabilityArea>>() {
                    @Override
                    public Collection<ProfitabilityArea> get() {
                        return Lists.newArrayList();
                    }
                });
//Long currenttime = System.currentTimeMillis();
                int k = 0;
                    
                if("0".equals(methodbc.getValue())){
                    //System.out.println("CHECKEMPTY: "+v1.itera);
                    for(ProfitabilityArea rs:v1._2()){
//                        if(!category.containsKey(rs.getZbin())){
//                            temp = new ArrayList<>();
//                            temp.add(rs);
//                            category.put(rs.getZbin(), temp);
//                        }else{
//                            category.get(rs.getZbin()).add(rs);
//                                    //category.put(profitabilityArea.getZbin(), value)
//                        }
                    category.put(rs.getZbin(), rs);
                    k++;
                    }
                }else{
                    for(ProfitabilityArea rs:v1._2()){
                          rsl.add(rs); 
                          k++;
                    }
                }
                System.out.print(k);
                //Long midtime = System.currentTimeMillis();
                //System.out.println("insert to list/hash "+(midtime-currenttime));
                //currenttime = System.currentTimeMillis();
                switch (methodbc.getValue()) {
                    case "0":
                        //System.out.println("CATEGORY SIZE: "+category.size());
//                        ZSkyline zsky = new ZSkyline(category);
//                        skylinel = zsky.skyline();
                        skylinel = ZorderSkyline(category);
                        break;
                    case "1":
//                        BNL bnl = new BNL(rsl);
//                        skylinel= bnl.skyline();
                        skylinel = BNLSkyline(rsl);
                        break;
                    case "2":
//                        DC dc = new DC(rsl);
//                        skylinel= dc.skyline(rsl, 4);
                        skylinel = skyline(rsl, 4);
                        break;
                    default:
                        break;
                }
                //Long endtime = System.currentTimeMillis();
                //System.out.println("skyline process "+(endtime-midtime));
//                System.out.println("local partisi");
//                for(ProfitabilityArea pa:skylinel){
//                    System.out.println(pa.print());
//                }
                System.out.println("local skyline: "+skylinel.size());

                return skylinel;
                    }
            }).flatMap(new FlatMapFunction<Iterable<ProfitabilityArea>, ProfitabilityArea>() {
                            @Override
                            public Iterable<ProfitabilityArea> call(Iterable<ProfitabilityArea> t) throws Exception {
                                return t;
                            }
                });
        JavaRDD<Iterable<ProfitabilityArea>> mergeLocalProfitableArea = localprofitableareas.groupBy(new Function<ProfitabilityArea, Integer>() {

			public Integer call(ProfitabilityArea v1) throws Exception {
				//int a = (int) (Math.random()*100%10);
				//System.out.println(a);
				return 1;
			}
		},1).values();
        
        JavaRDD<Iterable<ProfitabilityArea>> globalprofitableareas = mergeLocalProfitableArea.map(new Function<Iterable<ProfitabilityArea>, Iterable<ProfitabilityArea>>() {

            public Iterable<ProfitabilityArea> call(Iterable<ProfitabilityArea> v1) throws Exception {
                List<ProfitabilityArea> skylinel,rsl,temp;
                skylinel = new ArrayList<ProfitabilityArea>();
                rsl = new ArrayList<ProfitabilityArea>();
                temp = new ArrayList<>();
                //SortedMap<String, List<ProfitabilityArea>> category = new TreeMap<String, List<ProfitabilityArea>>();
                Multimap<String, ProfitabilityArea> category = Multimaps.newMultimap(
                    Maps.<String, Collection<ProfitabilityArea>>newTreeMap(/* your comparator here*/),
                    new Supplier<Collection<ProfitabilityArea>>() {
                    @Override
                    public Collection<ProfitabilityArea> get() {
                        return Lists.newArrayList();
                    }
                });
                if("0".equals(methodbc.getValue())){
                    //System.out.println("CHECKEMPTY: "+v1.itera);
                    for(ProfitabilityArea rs:v1){
//                        if(!category.containsKey(rs.getZbin())){
//                            temp = new ArrayList<>();
//                            temp.add(rs);
//                            category.put(rs.getZbin(), temp);
//                        }else{
//                            category.get(rs.getZbin()).add(rs);
//                                    //category.put(profitabilityArea.getZbin(), value)
//                        }
                    category.put(rs.getZbin(), rs);
                    }
                }else{
                    for(ProfitabilityArea rs:v1){
                          rsl.add(rs); 
                    }
                }
                
                switch (methodbc.getValue()) {
                    case "0":
                        //System.out.println("CATEGORY SIZE: "+category.size());
//                        ZSkyline zsky = new ZSkyline(category);
//                        skylinel = zsky.skyline();
                        skylinel = ZorderSkyline(category);
                        break;
                    case "1":
//                        BNL bnl = new BNL(rsl);
//                        skylinel= bnl.skyline();
                        skylinel = BNLSkyline(rsl);
                        break;
                    case "2":
//                        DC dc = new DC(rsl);
//                        skylinel= dc.skyline(rsl, 4);
                        skylinel = skyline(rsl, 4);
                        break;
                    default:
                        break;
                }
//                System.out.println("global partisi");
//                for(ProfitabilityArea pa:skylinel){
//                    System.out.println(pa.print());
//                }
                System.out.println("global skyline: "+skylinel.size());

                    return skylinel;
				}
			});   

        JavaRDD<ProfitabilityArea> pa = globalprofitableareas.flatMap(new FlatMapFunction<Iterable<ProfitabilityArea>, ProfitabilityArea>() {
			public Iterable<ProfitabilityArea> call(Iterable<ProfitabilityArea> t) throws Exception {
				 
				return t;
			}
		});
        List<ProfitabilityArea> pal = pa.collect();
//        System.out.println("===========================OUTPUT===========================");
//        for (ProfitabilityArea profitabilityArea : pal) {
//            System.out.println(profitabilityArea.print());
//        }
//        System.out.println("=============================================================");
        System.out.println("SIZE:"+pal.size());
        //sc.stop();
	}
        

}
