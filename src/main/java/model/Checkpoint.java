package model;


public class Checkpoint {
	private Long profit;
	private Long tripdistance;
	private Long spendingTime;
	private Long demandProbability;
	
	public Checkpoint(){
		
	}
	public Checkpoint(Long p,Long td,Long t,Long dm){
		profit=p;
		tripdistance=td;
		spendingTime=t;
		demandProbability=dm;
	}
	public Long getProfit() {
		return profit;
	}
	public void setProfit(Long profit) {
		this.profit = profit;
	}
	public Long getTripdistance() {
		return tripdistance;
	}
	public void setTripdistance(Long tripdistance) {
		this.tripdistance = tripdistance;
	}
	public Long getSpendingTime() {
		return spendingTime;
	}
	public void setSpendingTime(Long spendingTime) {
		this.spendingTime = spendingTime;
	}
	public Long getDemandProbability() {
		return demandProbability;
	}
	public void setDemandProbability(Long demandProbability) {
		this.demandProbability = demandProbability;
	}
	
}
