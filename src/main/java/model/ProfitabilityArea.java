package model;



import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class ProfitabilityArea implements Serializable{
	private String areacode;
	private Integer day;
	private Long time;
	private Integer groupid;
	private Double profit;
	private Double tripdistance;
	private Long spendingTime;
	private Double demandProbability;
	private Boolean zskyline;
    private String zbin;
    private Long zvalue;
    private Long profitpos,tripdistancepos,spendingtimpos,demandprobabilitypos;    
	public ProfitabilityArea(){
		profit = 0.0;
		tripdistance = 0.0;
		spendingTime = 0L;
		demandProbability = 0.0;
		areacode="";
		day = 0;
		time = 0L;
                zskyline = false;
                zbin="";
	}
	
        public ProfitabilityArea(ProfitabilityArea pm){
                profit = pm.profit;
		tripdistance = pm.tripdistance;
		spendingTime = pm.spendingTime;
		demandProbability = pm.demandProbability;
		areacode= pm.areacode;
		day = pm.day;
		time = pm.time;
                zskyline = pm.zskyline;
                zbin = pm.zbin;
        }
	public ProfitabilityArea(Double p, Double td, Long st, Double dp){
		areacode="";
		day = 0;
		time = 0L;
		profit = p;
		tripdistance = td;
		spendingTime = st;
		demandProbability = dp;
                zskyline = false;
                zbin="";
	}
	
	public ProfitabilityArea(Integer d,Long t,String area){
		areacode=area;
		day = d;
		time = t;
		profit = 0.0;
		tripdistance = 0.0;
		spendingTime = 0L;
		demandProbability = 0.0;
                zskyline = false;
                zbin="";
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Double getProfit() {
		return profit;
	}

	public void setProfit(Double profit) {
		this.profit = profit;
	}
	
	public void setProfit(Double fare,Double cost) {
		this.profit = fare-cost;
	}

	public Double getTripdistance() {
		return tripdistance;
	}

	public void setTripdistance(Double tripdistance) {
		this.tripdistance = tripdistance;
	}

	public Long getSpendingTime() {
		return spendingTime;
	}

	public void setSpendingTime(Long spendingTime) {
		this.spendingTime = spendingTime;
	}
	
	public void setSpendingTime(Date cl,long traveltime,Date pickupTime) {
		Date pt = new Date(0, 0, 0, pickupTime.getHours(), pickupTime.getMinutes(), 0);
		Date ct = new Date(0, 0, 0, cl.getHours(), cl.getMinutes(), 0);
		Long t = ct.getTime()+traveltime;
		Long waitingtime = pt.getTime()-t;
		this.spendingTime = traveltime+waitingtime;
	}
	
	public Double getDemandProbability() {
		return demandProbability;
	}

	public void setDemandProbability(Double demandProbability) {
		this.demandProbability = demandProbability;
	}
	
	
	public Integer getGroupid() {
		return groupid;
	}

	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}

	public String print(){
		return "ZBin:"+zbin+" Day:"+day+" time:"+time+" areacode:"+areacode+" profit:"+profit+" trip distance:"+tripdistance+" spending time:"+spendingTime+" demand probability:"+demandProbability+" "+profitpos+" "+tripdistancepos+" "+spendingtimpos+" "+demandprobabilitypos;
	}
	
	public String printcsv(){
		return zbin+","+day+","+time+","+areacode+","+profit+","+tripdistance+","+spendingTime+","+demandProbability+"\n";
	}
	
	public String printjson(){
		return "{zbin:'"+zbin+"',day:"+day+",time:"+time+",areacode:'"+areacode+"',profit:"+profit+",tripdistance:"+tripdistance+",spendingtime:"+spendingTime+",demandprobability"+demandProbability+"}";
	}

    /**
     * @return the zskyline
     */
    public Boolean getZskyline() {
        return zskyline;
    }

    /**
     * @param zskyline the zskyline to set
     */
    public void setZskyline(Boolean zskyline) {
        this.zskyline = zskyline;
    }

    /**
     * @return the zbin
     */
    public String getZbin() {
        return zbin;
    }

    /**
     * @param zbin the zbin to set
     */
    public void setZbin(String zbin) {
        this.zbin = zbin;
    }
    
    public void setZbin() {
        Long w,x,y,z;
        w = Math.max(0,15 -(long) Math.ceil(getDemandProbability()*1000L));
        x = (long) Math.min(15, Math.ceil(getTripdistance()));
        y = (long) Math.min(15,Math.ceil(getSpendingTime()/100000));
        z = Math.max(0,15 -(long) Math.ceil(getProfit()));
        
        setDemandprobabilitypos(w);
        setTripdistancepos(x);
        setSpendingtimpos(y);
        setProfitpos(z);
        Long test = mortonEncode_magicbits(w,x,y,z);
        this.zbin = String.format("%16s", Long.toBinaryString(test)).replace(' ', '0');
        this.zvalue = Long.parseLong(zbin);
    }
    public Long getProfitpos() {
            return profitpos;
    }

    public void setProfitpos(Long profitpos) {
            this.profitpos = profitpos;
    }

    public Long getTripdistancepos() {
            return tripdistancepos;
    }

    public void setTripdistancepos(Long tripdistancepos) {
            this.tripdistancepos = tripdistancepos;
    }

    public Long getSpendingtimpos() {
            return spendingtimpos;
    }

    public void setSpendingtimpos(Long spendingtimpos) {
            this.spendingtimpos = spendingtimpos;
    }

    public Long getDemandprobabilitypos() {
            return demandprobabilitypos;
    }

    public void setDemandprobabilitypos(Long demandprobabilitypos) {
            this.demandprobabilitypos = demandprobabilitypos;
    }

    public Long getZvalue() {
            return zvalue;
    }

    public void setZvalue(Long zvalue) {
            this.zvalue = zvalue;
    }

    protected static Long splitBy4(long a){
            Long x = a & 0xf; // we only look at the first 21 bits
            //x = (x | x << 24) & new BigInteger("ff000000ff",16).longValue();  // shift left 32 bits, OR with self, and 00011111000000000000000000000000000000001111111111111111
            //x = (x | x << 12) & new BigInteger("f000f",16).longValue();  // shift left 32 bits, OR with self, and 00011111000000000000000011111111000000000000000011111111
            x = (x | x << 6) & new BigInteger("303",16).longValue(); // shift left 32 bits, OR with self, and 0001000000001111000000001111000000001111000000001111000000000000
            x = (x | x << 3) & new BigInteger("1111",16).longValue(); // shift left 32 bits, OR with self, and 0001000011000011000011000011000011000011000011000011000100000000
            return x;
    }

    protected static Long mortonEncode_magicbits(Long x, Long y, Long z, Long a){
            Long answer = 0L;
        answer |= splitBy4(x) | splitBy4(y) << 1 | splitBy4(z) << 2 | splitBy4(a) << 3;
        return answer;
    }

	
	
}
