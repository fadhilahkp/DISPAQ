package model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import scala.Tuple4;
import scala.Tuple2;


public class AreaSummary extends Area implements Serializable{
	private Integer day;
	private Long time;
	private HashMap<Date, Double> pickuptimeList;
	private Double demandprob;
	
	public AreaSummary(){
		pickuptimeList = new HashMap<Date, Double>();
		day = 0;
		time = 0L;
	}
	
	public AreaSummary(String a){
		pickuptimeList = new HashMap<Date, Double>();
		this.setAreacode(a);
		day = 0;
		time = 0L;
	}
	
	public AreaSummary(Area a){
		this.setAreacode(a.getAreacode());
		this.setCount(a.getCount());
		this.setFare(a.getFare());
		this.setTip(a.getTip());
		pickuptimeList = new HashMap<Date, Double>();
		if(a.getPickuptime() != null){
		pickuptimeList.put(a.getPickuptime(), 1.0);}
		day = 0;
		time = 0L;
	}
        
        public AreaSummary(Area a, Integer d, Long t){
		this.setAreacode(a.getAreacode());
		this.setCount(a.getCount());
		this.setFare(a.getFare());
		this.setTip(a.getTip());
		pickuptimeList = new HashMap<Date, Double>();
		if(a.getPickuptime() != null){
		pickuptimeList.put(a.getPickuptime(), 1.0);}
		day = d;
		time = t;
	}
        
        public AreaSummary(AreaSummary a){
                this.setAreacode(a.getAreacode());
		this.setCount(a.getCount());
		this.setFare(a.getFare());
		this.setTip(a.getTip());
		pickuptimeList = a.getPickuptimeHash();
		demandprob = a.getDemandprob();
                day = a.getDay();
		time = a.getTime();
        }

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public HashMap<Date, Double> getPickuptimeHash() {
		return pickuptimeList;
	}

	public void setPickuptimeList(HashMap<Date, Double> pickuptimeList) {
		this.pickuptimeList = pickuptimeList;
	}
	
	public void addPickupTime(Date d){
		if(pickuptimeList.containsKey(d)){
			this.pickuptimeList.put(d, pickuptimeList.get(d)+1);
		} else
		{
			this.pickuptimeList.put(d, 1.0);
		}
                //this.setCount(getCount()+1.0);
	}
	
	public void addPickupTime(Date d, Double i){
		if(pickuptimeList.containsKey(d)){
			this.pickuptimeList.put(d, pickuptimeList.get(d)+i);
		} else
		{
			this.pickuptimeList.put(d, i);
		}
                //this.setCount(getCount()+i);

	}
	
	public void addPickupTime(HashMap<Date, Double> ptlist){
		Map.Entry<Date,Double> m;
		Set s;
		s = ptlist.entrySet();
		Iterator i = s.iterator();
		while(i.hasNext()){
			m = (Map.Entry<Date,Double>) i.next();
			if(pickuptimeList.containsKey(m.getKey())){
				this.pickuptimeList.put(m.getKey(), pickuptimeList.get(m.getKey())+m.getValue());
			} else
			{
				this.pickuptimeList.put(m.getKey(), m.getValue());
			}
                        //this.setCount(getCount()+1.0);
		}
	}
	
	public void computeAverage(Integer i){
		Map.Entry<Date,Double> m;
		Set s;
		s = pickuptimeList.entrySet();
		Iterator i1 = s.iterator();
		while(i1.hasNext()){
			m = (Map.Entry<Date,Double>) i1.next();
                        
			this.pickuptimeList.put(m.getKey(), (m.getValue()/i));
		}
		//Collections.sort(pickuptimeList);
	}
	
	public List<Tuple2<Date, Double>> getPickuptimeList(){
		Map.Entry<Date,Double> m;
		Set s;
		List<Tuple2<Date,Double>> t = new ArrayList<Tuple2<Date,Double>>();
		s = pickuptimeList.entrySet();
		Iterator i1 = s.iterator();
		while(i1.hasNext()){
			m = (Map.Entry<Date,Double>) i1.next();
			t.add(new Tuple2<Date, Double>(m.getKey(), m.getValue()));
		}
		return t;
	}
	
//	public HashMap<Long, Double> getPickuptimeList(){
//		Map.Entry<Date,Double> m;
//		Set s;
//		 HashMap<Long, Double> t = new HashMap<>();
//		s = pickuptimeList.entrySet();
//		Iterator i1 = s.iterator();
//		while(i1.hasNext()){
//			m = (Map.Entry<Date,Double>) i1.next();
//			t.put(m.getKey().getTime(), m.getValue())
//			t.add(new Tuple2<Date, Double>(m.getKey(), m.getValue()));
//		}
//		return t;
//	}
	
	
	public Date getPickupTime(Date arrivalTime){
		Map.Entry<Date,Double> m;
		Set s;
		s = pickuptimeList.entrySet();
		Iterator i1 = s.iterator();
		Double c = 0.0;
		Date time = new Date(0, 0, 0, arrivalTime.getHours(), arrivalTime.getMinutes());
		Date selectedTime = null;
		while(i1.hasNext()){
			m = (Map.Entry<Date,Double>) i1.next();
			if(m.getKey().after(time) && m.getValue() > c){
				c = m.getValue();
				selectedTime = m.getKey();
			}
		}
		if(selectedTime == null){
			Integer min = time.getMinutes();
			long temp;
			if(min%10 < 5) temp = time.getTime() + ((5-min%10)*60000);
			else temp = time.getTime()+((15-min%10)*60000);
			selectedTime = new Date(temp);
		}
		return selectedTime;
	}
	public Tuple4<String,Double,Double,Iterable<Tuple2<Date, Double>>> convertToTuple(){
		
		return new Tuple4<String, Double, Double, Iterable<Tuple2<Date, Double>>>(getAreacode(), getFare(), getCount(), getPickuptimeList());
	}
	
	public String print(){
		 String s = "Day:"+day+" time:"+time+" area code: "+ this.getAreacode() +" fare: "+getFare()+" count: "+getCount()+" pickup timelist: [";
		 for (Tuple2<Date, Double> t : getPickuptimeList()) {
			s = s+"{"+t._1.getTime()+":"+t._2+"},";
		}
		 s += "]";
		 return s;
		
	}

	public Double getDemandprob() {
		return demandprob;
	}

	public void setDemandprob(Double demandprob) {
		this.demandprob = demandprob;
	}
	
	
}
