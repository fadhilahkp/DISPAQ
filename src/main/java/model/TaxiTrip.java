package model;

import java.io.Serializable;
import java.util.Date;

public class TaxiTrip implements Serializable{
    /*
    Analyzed Data
    
    1  pickup date 			: Date
    2  dropoff date			: Date
    3  day					: Integer
    4  pickup time category	: Date
    5  pickup area			: String
    6  dropoff area			: String
    7  route					: String
    8  trip distance			: double
    9  fare amount			: double
    10  tip amount			: double
    11 tolls amount			: double
    12 travel time			: double
    */
	
	private Date pickuptime;
	private Integer day;
	private Long timecat;
	private Area pickuparea;
	private Route route;
	
	public TaxiTrip(){
		pickuptime = new Date();
		pickuparea = new Area();
		route = new Route();
		day = 0;
		timecat = 0L;
	}
	
	public TaxiTrip(Date pt, Area pa, Route r, Integer d, Long tc){
		pickuptime = pt;
		pickuparea = pa;
		route = r;
		day = d;
		timecat = tc;
	}
	public Date getPickuptime() {
		return pickuptime;
	}

	public void setPickuptime(Date pickuptime) {
		this.pickuptime = pickuptime;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Long getTimecat() {
		return timecat;
	}

	public void setTimecat(Long timecat) {
		this.timecat = timecat;
	}

	public Area getPickuparea() {
		return pickuparea;
	}

	public void setPickuparea(Area pickuparea) {
		this.pickuparea = pickuparea;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}
	
	
}
