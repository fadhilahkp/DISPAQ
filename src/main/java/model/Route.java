package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Route implements Serializable{
	
	protected Area pickuparea,dropoffarea;
	
	protected Double tripdistance,cost;
	protected Long traveltime;
	protected Double count;
	protected String routecode;
	
	public Area getPickuparea() {
		return pickuparea;
	}

	public void setPickuparea(Area pickuparea) {
		this.pickuparea = pickuparea;
	}

	public Area getDropoffarea() {
		return dropoffarea;
	}

	public void setDropoffarea(Area dropoffarea) {
		this.dropoffarea = dropoffarea;
	}

	public Double getTripdistance() {
		return tripdistance;
	}

	public void setTripdistance(Double tripdistance) {
		this.tripdistance = tripdistance;
	}

	public Long getTraveltime() {
		return traveltime;
	}

	public void setTraveltime(Long traveltime) {
		this.traveltime = traveltime;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double tolls) {
		this.cost = (tripdistance* 0.0625 *0.1)+tolls;
	}
	
	public void setCost(Double tolls,Integer i) {
		this.cost = tolls;
	}
	public Double getCount() {
		return count;
	}

	public void setCount(Double count) {
		this.count = count;
	}

	public String getRoutecode() {
		return routecode;
	}

	public void setRoutecode(String pa, String da) {
		this.routecode = pa+"-"+da;
	}

	public Route(){
		pickuparea = new Area();
		dropoffarea = new Area();
		cost = 0.0;
		traveltime = 0L;
		tripdistance = 0.0;
	}
	
	public Route(Area pa, Area da){
		pickuparea = pa;
		dropoffarea = da;
		routecode = pa.getAreacode()+"-"+da.getAreacode();
		cost = 0.0;
		traveltime = 0L;
		tripdistance = 0.0;
	}
}
