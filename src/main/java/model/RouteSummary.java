package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RouteSummary extends Route implements Serializable{
	private Integer day;
	private Long time;
	private Boolean skyline;
	private Integer zvalue;
	private String zbin;
	
	public RouteSummary(){
		day = 0;
		time = 0L;
		skyline = false;
		zvalue = 0;
		zbin = "00000";

	}
	
	public RouteSummary(RouteSummary rs){
		day = rs.getDay();
		time = rs.getTime();
		skyline = rs.getSkyline();
		zvalue = rs.getZvalue();
		zbin = rs.getZbin();
		setCost(rs.cost);
		setCount(rs.count);
		setDropoffarea(rs.dropoffarea);
		setPickuparea(rs.pickuparea);
		setRoutecode(rs.pickuparea.getAreacode(), rs.dropoffarea.getAreacode());
		setTraveltime(rs.traveltime);
		setTripdistance(rs.tripdistance);
	}
	
	public RouteSummary(Area pa, Area da){
		day = 0;
		time = 0L;
		skyline = false;
		zvalue = 0;
		zbin = "00000";
		this.setPickuparea(pa);
		this.setDropoffarea(da);
		this.setRoutecode(pa.getAreacode(),da.getAreacode());
		this.setCost(0.0);
		this.setTraveltime(0L);
		this.setTripdistance(0.0);
	}
	
	public String getZbin() {
		return zbin;
	}

	public void setZbin(String zbin) {
		this.zbin = zbin;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	

	public Integer getZvalue() {
		return zvalue;
	}

	public void setZvalue(Integer zvalue) {
		this.zvalue = zvalue;
	}
	
	public String print(){
		return "zbin: "+zbin+" zvalue: "+zvalue+" route: "+getRoutecode()+" Dropoff: "+getDropoffarea().getAreacode()+" Cost: "+getCost()+" Trip Distance: "+getTripdistance()+" Travel Time: "+getTraveltime();
	}

    /**
     * @return the skyline
     */
    public Boolean getSkyline() {
        return skyline;
    }

    /**
     * @param skyline the skyline to set
     */
    public void setSkyline(Boolean skyline) {
        this.skyline = skyline;
    }

}
