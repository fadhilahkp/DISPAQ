/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import scala.Tuple2;

/**
 *
 * @author fadhi
 */
public class UpgradedSummary implements Serializable{
    private RouteSummary rs;
    private Long minTimePeriod;
    private Long maxTimePeriod;
    private AreaSummary minArea;
    private AreaSummary maxArea;

    public UpgradedSummary(){
        rs = new RouteSummary();
        minArea = new AreaSummary();
        maxArea = new AreaSummary();
        minTimePeriod = 0L;
        maxTimePeriod = 0L;
    }
    
    public UpgradedSummary(RouteSummary rs, Long mintp, AreaSummary minas, Long maxtp,AreaSummary maxas){
        rs = new RouteSummary(rs);
        minArea = new AreaSummary(minas);
        maxArea = new AreaSummary(maxas);
        minTimePeriod = mintp;
        maxTimePeriod = maxtp;
    }
    /**
     * @return the rs
     */
    public RouteSummary getRs() {
        return rs;
    }

    /**
     * @return the minArea
     */
    
    
    public ProfitabilityArea createProfitabilityArea(Long ct){
        ProfitabilityArea pa = new ProfitabilityArea();
        pa.setAreacode(rs.getDropoffarea().getAreacode());
        pa.setTime(ct);
        pa.setDay(rs.getDay());
        pa.setTripdistance(rs.getTripdistance());
        //pa.setSpendingTime(rs.getTraveltime());
        Date arrivalTime = new Date(ct + rs.getTraveltime());
        
        if(ct < maxTimePeriod){
            pa.setProfit(minArea.getFare()-rs.getCost());
            pa.setDemandProbability(minArea.getDemandprob());
            pa.setSpendingTime(new Date(ct), rs.getTraveltime(), minArea.getPickupTime(arrivalTime));
        }else{
            pa.setProfit(maxArea.getFare()-rs.getCost());
            pa.setDemandProbability(maxArea.getDemandprob());
            pa.setSpendingTime(new Date(ct), rs.getTraveltime(), maxArea.getPickupTime(arrivalTime));

        }
        return pa;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(RouteSummary rs) {
        this.rs = rs;
    }

    /**
     * @return the minTimePeriod
     */
    public Long getMinTimePeriod() {
        return minTimePeriod;
    }

    /**
     * @param minTimePeriod the minTimePeriod to set
     */
    public void setMinTimePeriod(Long minTimePeriod) {
        this.minTimePeriod = minTimePeriod;
    }

    /**
     * @return the maxTimePeriod
     */
    public Long getMaxTimePeriod() {
        return maxTimePeriod;
    }

    /**
     * @param maxTimePeriod the maxTimePeriod to set
     */
    public void setMaxTimePeriod(Long maxTimePeriod) {
        this.maxTimePeriod = maxTimePeriod;
    }

    /**
     * @return the minArea
     */
    public AreaSummary getMinArea() {
        return minArea;
    }

    /**
     * @param minArea the minArea to set
     */
    public void setMinArea(AreaSummary minArea) {
        this.minArea = minArea;
    }

    /**
     * @return the maxArea
     */
    public AreaSummary getMaxArea() {
        return maxArea;
    }

    /**
     * @param maxArea the maxArea to set
     */
    public void setMaxArea(AreaSummary maxArea) {
        this.maxArea = maxArea;
    }
    
            
}
