package model;

import java.io.Serializable;
import java.util.Date;

public class Area implements Serializable{
	private String areacode;
	private double fare;
	private double tip;
	private Date pickuptime;
	private double count;
	
	public String getAreacode() {
		return areacode;
	}
	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}
	public double getFare() {
		return fare;
	}
	public void setFare(double fare) {
		this.fare = fare;
	}
	public Date getPickuptime() {
		return pickuptime;
	}
	public void setPickuptime(Date pickuptime) {
		this.pickuptime = pickuptime;
	}
	public Double getCount() {
		return count;
	}
	public void setCount(Double count) {
		this.count = count;
	}
	
	
	public double getTip() {
		return tip;
	}
	public void setTip(double tip) {
		this.tip = tip;
	}
	public Area(){
		areacode = "";
		fare = 0.0;
		count = 0;
	}
	
	public Area(String ac){
		areacode = ac;
		fare = 0.0;
		count = 0;
	}
}
