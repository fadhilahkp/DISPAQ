package construction;

import scala.Tuple2;
import scala.Tuple3;

import org.apache.spark.Partitioner;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.bson.Document;

import com.github.davidmoten.geo.GeoHash;
import com.google.common.collect.Iterables;
import com.mongodb.hadoop.MongoOutputFormat;
import com.mongodb.spark.MongoSpark;

import model.Area;
import model.AreaSummary;
import model.Route;
import model.RouteSummary;
import model.TaxiTrip;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.broadcast.Broadcast;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;
import org.bson.types.BasicBSONList;
/*
 * spark-submit
 * --class thesis.construction.construction 
 * --master local[2] 
 * --jars C:\Users\fadhi\.m2\repository\com\github\davidmoten\geo\0.7.1\geo-0.7.1.jar,C:\Users\fadhi\.m2\repository\com\github\davidmoten\grumpy-core\0.2.2\grumpy-core-0.2.2.jar 
 * D:\dhila\workspace\thesis\target\thesis-0.0.1.jar D:\dhila\test.csv D:\dhila\output.csv
 * */

public class naiveconstruction {
    
	protected static String[] splitLine(String line){;
	    String temp = line.replaceAll("\\[","").replaceAll("\\]", "").replaceAll("\\(", "").replaceAll("\\)", "");
	    String[] res = temp.split(",");
	    return res;
	}
	
	protected static String defineArea(String lat, String lon, Integer len){
		Double areaLong = Double.parseDouble(lon);
        Double areaLat = Double.parseDouble(lat);
        if (areaLat > -90.0 && areaLat < 90) {
            return GeoHash.encodeHash(areaLat, areaLong, len);
        }
        else return null;
	}
	
	protected static String defineRoute(String line){
        String pickupHash = null, dropoffHash = null;
        String[] sp = splitLine(line);
 
        pickupHash = defineArea(sp[2], sp[1], 6);
        dropoffHash = defineArea(sp[4], sp[3], 6);
        return pickupHash+"-"+dropoffHash;
     }
	
     @SuppressWarnings({ "serial", "resource" })
	public static void main(String[] args) {
    	 JavaRDD<TaxiTrip> analyzeData;
    	 JavaPairRDD<Tuple2<Integer, Long>, Tuple2<TaxiTrip, Integer>> timeGroupData;
	    if (args.length < 1) {
	        System.err.println("Usage: Naive Constructor");
	        System.exit(1);
	      }

//	      SparkConf conf = new SparkConf().setAppName("IndexConstruction")
//	    		  .set("spark.sql.warehouse.dir", "D:\\dhila\\workspace")
//	    		  .set("spark.mongodb.output.uri", "mongodb://127.0.0.1/test."+args[1]);
//	      JavaSparkContext sc = new JavaSparkContext(conf);
//
//	      //final Accumulator<Double> accum = sc.accumulator(0L);
//	      JavaRDD<String> lines = sc.textFile(args[0]).filter(new Function<String, Boolean>() {
//			
//			public Boolean call(String v1) throws Exception {
//				if(v1.contains("VendorID") == true)
//					return false;
//				else return true;
//			}
//		});
	      
              SparkConf conf = new SparkConf().setAppName("Naive Index Construction");//.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
	    		 // .set("spark.mongodb.output.uri", "mongodb://164.125.121.111:27020/taxi_trip."+args[1]);
	      JavaSparkContext sc = new JavaSparkContext(conf);
              Configuration outputConfig = new Configuration();
                outputConfig.set("mongo.output.uri","mongodb://164.125.121.111:27020/taxi_trip."+args[1]);   
	      //final Accumulator<Double> accum = sc.accumulator(0L);
              Integer partition = Integer.parseInt(args[2]);
//              Integer interval = Integer.parseInt(args[3]);
//              Integer codeLength = Integer.parseInt(args[4]);
//              final Broadcast<Integer> intervalBc = sc.broadcast(interval);
//              final Broadcast<Integer> codeLengthBc = sc.broadcast(codeLength);
	      //JavaRDD<String> lines = sc.textFile("/user/hduser/"+args[0]).filter(new Function<String, Boolean>() 
              JavaRDD<String> lines = sc.textFile(args[0]).filter(new Function<String, Boolean>() {
			
                        @Override
			public Boolean call(String v1) throws Exception {
				if(v1.contains("VendorID") == true || v1.contains("vendor_id") == true || v1.isEmpty())
					return false;
				else return true;
			}
		}).repartition(partition);
	      /*
	       Data Raw Format
	        [1] "VendorID"              "tpep_pickup_datetime"  "tpep_dropoff_datetime" "passenger_count"       "trip_distance"        
			[6] "pickup_longitude"      "pickup_latitude"       "RatecodeID"            "store_and_fwd_flag"    "dropoff_longitude"    
			[11] "dropoff_latitude"      "payment_type"          "fare_amount"           "extra"                 "mta_tax"              
			[16] "tip_amount"            "tolls_amount"          "improvement_surcharge" "total_amount"
	       
	       */
	      //PRE-PROCESSING
        analyzeData = lines.map(
            new Function<String, TaxiTrip>() {

                @SuppressWarnings("deprecation")
                public TaxiTrip call(String arg0) throws Exception {
                    try{
                    TaxiTrip d = new TaxiTrip();
                    //System.out.println(arg0+"++");
                    Date pickupDate,dropoffDate;
                    Route route;
                    Area pickupArea,dropoffArea;
                    Integer day;
                    Double tripDistance,fareAmount,tipAmount,tollsAmount;
                    Long timecategory, tripTime;

                    String[] splitData = splitLine(arg0);

                    DateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //DateFormat dt = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    pickupDate = dt.parse(splitData[1]);
                    dropoffDate = dt.parse(splitData[2]);
                    day = pickupDate.getDay();
                    tripTime = dropoffDate.getTime() - pickupDate.getTime();
                    Date temp = new Date(0, 0, 0, pickupDate.getHours(), pickupDate.getMinutes());
                    if (pickupDate.getMinutes()%10 > 5) {
                            timecategory = temp.getTime()+((10-(temp.getMinutes()%10))*60000);
                            //System.out.println("> 5 "+pickupDate+" "+temp +" "+temp.getMinutes() +" "+new Date(timecategory));
                    }
                    else{
                            timecategory = temp.getTime()-(temp.getMinutes()%10*60000);
                            //System.out.println("<= 5 "+pickupDate+" "+temp +" "+temp.getMinutes() +" "+new Date(timecategory));
                    }

                    pickupArea = new Area(defineArea(splitData[6], splitData[5], 6));
                    pickupArea.setPickuptime(temp);
                    pickupArea.setCount(1.0);
                    dropoffArea = new Area(defineArea(splitData[10], splitData[9], 6));
                    route = new Route(pickupArea,dropoffArea);
                    route.setCount(1.0);
                    tripDistance = Double.parseDouble(splitData[4]);
                    route.setTripdistance(tripDistance);
                    route.setTraveltime(tripTime);
                    fareAmount =Double.parseDouble(splitData[12]);
                    pickupArea.setFare(fareAmount);
                    tipAmount = Double.parseDouble(splitData[15]);
                    pickupArea.setTip(tipAmount);
                    tollsAmount = Double.parseDouble(splitData[16]);
                    route.setCost(tollsAmount);
                    d = new TaxiTrip(temp, pickupArea, route, day, timecategory);
                    //d = new Tuple12<Date, Date, Integer, Date, String, String, String, Double, Double, Double, Double, Long>(
                    //		pickupDate, dropoffDate, day, new Date(timecategory), pickupArea, dropoffArea, route, tripDistance, fareAmount, tipAmount, tollsAmount, tripTime);
                    return d;
                    }catch(Exception e){
                        System.out.println(arg0);
                    }
                    return new TaxiTrip();
                }
                  }).filter(
                    new Function<TaxiTrip, Boolean>() {

                        public Boolean call(
                            TaxiTrip v1)
                            throws Exception {
                            try{
                            if(v1.getPickuparea().getFare() == 0.0 || v1.getRoute().getTraveltime() == 0.0 || v1.getPickuparea().getAreacode().contains("s000000") == true || v1.getRoute().getDropoffarea().getAreacode() == "s000000" || v1.getRoute().getTripdistance() == 0.0){
                                    return false;}
                            else{
                                    return true;}
                            }catch (Exception e) {
                                    return false;
                            }
                    }
                });
	      
	      /*
	       Analyzed Data
	       
	       1  pickup date 			: Date
	       2  dropoff date			: Date
	       3  day					: Integer
	       4  pickup time category	: Date
	       5  pickup area			: String
	       6  dropoff area			: String
	       7  route					: String
	       8  trip distance			: double
	       9  fare amount			: double
	       10  tip amount			: double
	       11 tolls amount			: double
	       12 travel time			: double
	       */
	      
	      timeGroupData = analyzeData.groupBy(
                new Function<TaxiTrip, Tuple2<Integer,Long>>() {
                    public Tuple2<Integer,Long> call(
                            TaxiTrip v1)
                            throws Exception {
                    // TODO Auto-generated method stub
                            return new Tuple2<Integer,Long>(v1.getDay(),v1.getTimecat());
                    }
		}).flatMapToPair(
                    new PairFlatMapFunction<Tuple2<Tuple2<Integer,Long>,Iterable<TaxiTrip>>, Tuple2<Integer,Long>, Tuple2<TaxiTrip,Integer>>() {

                        public Iterable<Tuple2<Tuple2<Integer, Long>, Tuple2<TaxiTrip, Integer>>> call(Tuple2<Tuple2<Integer,Long>, Iterable<TaxiTrip>> t)
                                        throws Exception {
                            Integer count = Iterables.size(t._2);
                            Tuple2<TaxiTrip, Integer> d;
                            List<Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip, Integer>>> l = new ArrayList<Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip, Integer>>>();
                            for (TaxiTrip tt : t._2) {
                                    if(tt.getRoute().getCost() == 0.0) System.out.println("cost = 0");
                                    if(tt.getRoute().getTripdistance() == 0.0) System.out.println("trip distance = 0");
                                    if(tt.getRoute().getTraveltime() == 0L) System.out.println("travel time = 0");
                                    d = new Tuple2<TaxiTrip, Integer>(tt,count);
                                    l.add(new Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip,Integer>>(t._1,d));
                            }
                            // TODO Auto-generated method stub
                            return l;
                        }
		});
	      
	      //ROUTE SUMMARY
	      
		  /*
		   * key: day, pickup datetime, pickup area
		   * value : trip distance, travel time, spending cost, 1
		  */
	      JavaPairRDD<Tuple3<Integer, Long, String>, RouteSummary> routeSummary = timeGroupData.mapToPair(
                    new PairFunction<Tuple2<Tuple2<Integer,Long>,Tuple2<TaxiTrip,Integer>>, Tuple3<Integer, Long, String>, Tuple2<Route, Integer>>() {
                        public Tuple2<Tuple3<Integer, Long, String>, Tuple2<Route, Integer>> call(
                                        Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip, Integer>> t) throws Exception {
                                // TODO Auto-generated method stub
                            Tuple3<Integer, Long, String> key = new Tuple3<Integer, Long, String>(t._1._1,t._1._2, t._2._1.getRoute().getRoutecode());
                            Tuple2<Route, Integer> value = new Tuple2<Route, Integer>(t._2._1.getRoute(), 1);
                            return new Tuple2<Tuple3<Integer,Long,String>, Tuple2<Route,Integer>>(key, value);
                        }

                }).reduceByKey(
                    new Function2<Tuple2<Route,Integer>, Tuple2<Route,Integer>, Tuple2<Route,Integer>>() {

                        public Tuple2<Route, Integer> call(Tuple2<Route, Integer> v1, Tuple2<Route, Integer> v2) throws Exception {
                            Double tdSum,cSum;
                            Long ttSum;
                            Integer count;
                            Route r = new Route(v1._1.getPickuparea(), v1._1.getDropoffarea());
                            ttSum = v1._1.getTraveltime();
                            tdSum = v1._1.getTripdistance()+v2._1.getTripdistance();
                            cSum = v1._1.getCost() + v2._1.getCost();
                            count = v1._2+v2._2;
                            r.setCost(cSum);
                            r.setTraveltime(ttSum);
                            r.setTripdistance(tdSum);
                            //r.setCount(count);
                            return new Tuple2<Route, Integer>(r, count);
                        }
                }).mapToPair(
                    new PairFunction<Tuple2<Tuple3<Integer,Long,String>,Tuple2<Route,Integer>>, Tuple3<Integer,Long,String>, RouteSummary>() {

                        public Tuple2<Tuple3<Integer, Long, String>, RouteSummary> call(
                                        Tuple2<Tuple3<Integer, Long, String>, Tuple2<Route, Integer>> t)
                                        throws Exception {
                            Tuple3<Integer, Long, String> key = new Tuple3<Integer, Long, String>(t._1._1(), t._1._2(), t._2._1.getPickuparea().getAreacode());

                            RouteSummary r = new RouteSummary(t._2._1.getPickuparea(), t._2._1.getDropoffarea());
                            r.setCost(t._2._1().getCost()/t._2._2());
                            r.setTraveltime(t._2._1().getTraveltime()/t._2._2());
                            r.setTripdistance(t._2._1().getTripdistance()/t._2._2());
                            return new Tuple2<Tuple3<Integer,Long,String>, RouteSummary>(key, r);
                        }

                });
	      
	      /*JavaPairRDD<Tuple3<Integer, Date, String>, Tuple2<Route, Integer>> routeData = analyzeData.mapToPair(
	    		  new PairFunction<TaxiTrip, Tuple3<Integer, Date, String>, Tuple2<Route, Integer>>() {
	    			  public Tuple2<Tuple3<Integer, Date, String>, Route> call(
	    					Tuple12<Date, Date, Integer, Date, String, String, String, Double, Double, Double, Double, Long> arg0)
	    					throws Exception {

	    				Tuple3<Integer, Date, String> key = new Tuple3<Integer, Date, String>(arg0._3(), arg0._4(), arg0._7());
	    				// fuel : 0.0625L / miles
	    				// fuel cost = $2.225 / gallon -> $0.6 / L
	    				Double cost = (arg0._8() * 0.0625 *0.6)+arg0._11();
	    				Tuple4<Double, Long, Double,Integer> value = new Tuple4<Double, Long, Double,Integer>(arg0._8(), arg0._12(), cost,1);
	    				return new Tuple2<Tuple3<Integer,Date,String>, Tuple4<Double,Long,Double,Integer>>(key, value);
	    			}
		}).reduceByKey(
	    		  new Function2<Tuple4<Double,Long,Double,Integer>, Tuple4<Double,Long,Double,Integer>, Tuple4<Double,Long,Double,Integer>>() {
	    			  /*
	    			   * key: day, pickup datetime, pickup area
	    			   * value : trip distance sum, travel time sum, spending cost sum, data count
	    			  
					public Tuple4<Double, Long, Double, Integer> call(Tuple4<Double, Long, Double, Integer> v1,
							Tuple4<Double, Long, Double, Integer> v2) throws Exception {
						Double tripDistSum = v1._1()+v2._1();
						Long travelTimeSum = v1._2()+v2._2();
						Double spendingCostSum = v1._3()+v2._3();
						Integer count = v1._4()+v2._4();
						return new Tuple4<Double, Long, Double, Integer>(tripDistSum, travelTimeSum, spendingCostSum, count);
					}
				}).mapToPair(
	    		  new PairFunction<Tuple2<Tuple3<Integer,Date,String>,Tuple4<Double,Long,Double,Integer>>, Tuple3<Integer, Date, String>, Tuple3<Double, Long, Double>>() {
	    			  public Tuple2<Tuple3<Integer, Date, String>, Tuple3<Double, Long, Double>> call(
	    					Tuple2<Tuple3<Integer, Date, String>, Tuple4<Double, Long, Double, Integer>> t)
	    					throws Exception {
	    				Double tripDistAvg = t._2._1()/t._2._4();
	    				Long travelTimeAvg = t._2._2()/t._2._4();
	    				Double spendingCostAvg = t._2._3()/t._2._4();
	    				Tuple3<Double, Long, Double> a = new Tuple3<Double, Long, Double>(tripDistAvg, travelTimeAvg, spendingCostAvg);
	    				return new Tuple2<Tuple3<Integer,Date,String>, Tuple3<Double,Long,Double>>(t._1, a);
	    			}
				});*/
	      
	      //AREA SUMMARY
	      
		  /*
		   * key: day, pickup datetime, area
		   * value : Area Summary(income, cruising time, demand probability)
		   * */
	      
            JavaPairRDD<Tuple3<Integer, Long, String>, AreaSummary> areaSummary = timeGroupData.mapToPair(
                new PairFunction<Tuple2<Tuple2<Integer,Long>,Tuple2<TaxiTrip,Integer>>, Tuple3<Integer, Long, String>, Tuple2<AreaSummary,Integer>>() {

                        public Tuple2<Tuple3<Integer, Long, String>, Tuple2<AreaSummary, Integer>> call(
                                        Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip, Integer>> t) throws Exception {
                                // TODO Auto-generated method stub
                                Tuple3<Integer,Long,String> key = new Tuple3<Integer, Long, String>(t._1._1,t._1._2, t._2._1.getPickuparea().getAreacode());
                                AreaSummary aa = new AreaSummary(t._2._1.getPickuparea());
                                //aa.addPickupTime(t._2._1.getPickuptime());
                                //System.out.println(aa.print());
                                Tuple2<AreaSummary,Integer> value = new Tuple2<AreaSummary, Integer>(aa, t._2._2);
                                return new Tuple2<Tuple3<Integer,Long,String>, Tuple2<AreaSummary,Integer>>(key,value);
                        }

            }).reduceByKey(
                new Function2<Tuple2<AreaSummary,Integer>, Tuple2<AreaSummary,Integer>, Tuple2<AreaSummary,Integer>>() {

                    public Tuple2<AreaSummary, Integer> call(Tuple2<AreaSummary, Integer> v1, Tuple2<AreaSummary, Integer> v2)
                                    throws Exception {
//                        System.out.println("v1");    
//                        System.out.println(v1._1.print());
//                        System.out.println("v2");    
//                        System.out.println(v2._1.print());
                        AreaSummary a = new AreaSummary(v1._1.getAreacode());
                        a.setCount(v1._1.getCount()+v2._1.getCount());
                        a.setFare(v1._1.getFare()+v1._1.getTip() + v2._1.getFare()+v2._1.getTip());
                        //a.setPickuptimeList(v1._1.getPickuptimeHash());
                        a.addPickupTime(v1._1.getPickuptimeHash());
                        a.addPickupTime(v2._1.getPickuptimeHash());
                        return new Tuple2<AreaSummary, Integer>(a, v1._2);
                    }
            }).mapToPair(
                new PairFunction<Tuple2<Tuple3<Integer,Long,String>,Tuple2<AreaSummary,Integer>>, Tuple3<Integer, Long, String>, AreaSummary>() {
                        public Tuple2<Tuple3<Integer, Long, String>, AreaSummary> call(
                                        Tuple2<Tuple3<Integer, Long, String>, Tuple2<AreaSummary, Integer>> t)
                                        throws Exception {
                                AreaSummary aa = new AreaSummary();
                                aa.setAreacode(t._2._1.getAreacode());
                                aa.setCount(t._2._1.getCount()/t._2._2);
                                aa.setFare(t._2._1.getFare()/t._2._1.getCount().intValue());
                                aa.setPickuptimeList(t._2._1.getPickuptimeHash());
                                //System.out.println(aa.print()+" "+t._2._1.getCount());
                                aa.computeAverage(t._2._1.getCount().intValue());
                                return new Tuple2<Tuple3<Integer,Long,String>, AreaSummary>(t._1, aa);
                        }
                });

	      

	      /*final JavaPairRDD<Tuple3<Integer, Date, String>, Tuple3<Double, List<Date>, Double>> areaData = analyzeData.mapToPair(
	    		  new PairFunction<Tuple12<Date,Date,Integer,Date,String,String,String,Double,Double,Double,Double,Long>, Tuple3<Integer, Date, String>, Tuple3<Double, List<Date>, Double>>() {
	    			  public Tuple2<Tuple3<Integer, Date, String>, Tuple3<Double, List<Date>, Double>> call(
	    					Tuple12<Date, Date, Integer, Date, String, String, String, Double, Double, Double, Double, Long> arg0)
	    					throws Exception {
	    				List<Date>  a= new ArrayList<Date>();
	    				a.add( arg0._1());
		    			Tuple3<Integer, Date, String> key = new Tuple3<Integer, Date, String>(arg0._3(), arg0._4(), arg0._5());
		    			Tuple3<Double, List<Date>, Double> value = new Tuple3<Double, List<Date>, Double>(arg0._9()+arg0._10(),a, 1.0);
		    			accum.add(1.0);
	    				return new Tuple2<Tuple3<Integer,Date,String>, Tuple3<Double,List<Date>,Double>>(key, value);
	    			}
				});
	      
	      JavaPairRDD<Tuple3<Integer, Date, String>, Tuple3<Double, List<Date>, Double>> areaSum = areaData.reduceByKey(
	    		  new Function2<Tuple3<Double,List<Date>,Double>, Tuple3<Double,List<Date>,Double>, Tuple3<Double,List<Date>,Double>>() {
					
					public Tuple3<Double, List<Date>, Double> call(Tuple3<Double, List<Date>, Double> v1, Tuple3<Double, List<Date>, Double> v2)
							throws Exception {
						Double incomeSum = v1._1()+v2._1();
						v1._2().addAll(v2._2());

						Double demandProb = v1._3()+v2._3();
						return new Tuple3<Double, List<Date>, Double>(incomeSum,v1._2(),demandProb);
					}
				});
	      JavaPairRDD<Tuple3<Integer, Date, String>, Tuple3<Double, Date, Double>> areaSummary = areaSum.mapToPair(
	    		  new PairFunction<Tuple2<Tuple3<Integer,Date,String>,Tuple3<Double,List<Date>,Double>>, Tuple3<Integer, Date, String>, Tuple3<Double, Date, Double>>() {
	    			  public Tuple2<Tuple3<Integer, Date, String>, Tuple3<Double, Date, Double>> call(
	    					Tuple2<Tuple3<Integer, Date, String>, Tuple3<Double, List<Date>, Double>> t)
	    					throws Exception {
	    				  Double incomeAvg = t._2._1()/t._2._3();
		    				Collections.sort(t._2._2());
		    				int l = t._2._2().size();
		    				Date median = t._2._2().get(l/2);
		    				Double demp = t._2._3();
		    				Tuple3<Double, Date, Double> a = new Tuple3<Double, Date, Double>(incomeAvg, median, demp);
		    				return new Tuple2<Tuple3<Integer,Date,String>, Tuple3<Double,Date,Double>>(t._1, a);
	    			}
		});*/
	      //areaSummary.saveAsObjectFile("AREA-SUMMARY");
	      //areaSummary.collect();
	      //routeSummary.collect();
	     
	     //Z-Order Value
	      
//	      JavaPairRDD<Tuple3<Integer, Long, String>, RouteSummary> sortedData = routeSummary.groupByKey(new Partitioner() {
//			
//			@Override
//			public int numPartitions() {
//				// TODO Auto-generated method stub
//				return 7;
//			}
//			
//			@Override
//			public int getPartition(Object arg0) {
//				Tuple3<Integer, Long, String> a = (Tuple3<Integer, Long, String>) arg0;
//				return a._1();
//			}
//		}).flatMapToPair(new PairFlatMapFunction<Tuple2<Tuple3<Integer,Long,String>,Iterable<RouteSummary>>, Tuple3<Integer, Long, String>, RouteSummary>() {
//
//			public Iterable<Tuple2<Tuple3<Integer, Long, String>, RouteSummary>> call(
//					Tuple2<Tuple3<Integer, Long, String>, Iterable<RouteSummary>> t) throws Exception {
//				List<Tuple2<Tuple3<Integer, Long, String>, RouteSummary>> temp = new ArrayList<Tuple2<Tuple3<Integer,Long,String>,RouteSummary>>();
//				for(RouteSummary rs: t._2){
//					temp.add(new Tuple2<Tuple3<Integer,Long,String>, RouteSummary>(t._1, rs));
//				}
//				return temp;
//			}
//		});
	      
	      
	      
	      //merge RouteSummary and AreaSummary
	      JavaPairRDD<Tuple3<Integer, Long, String>, Tuple2<Iterable<AreaSummary>, Iterable<RouteSummary>>> gzindex = areaSummary.cogroup(routeSummary);
	      //JavaPairRDD<Tuple3<Integer, Long, String>, Tuple2<Iterable<Tuple3<Double,Double,Integer>>, Iterable<Tuple3<Double,Long,Double>>>>
	      //JavaPairRDD<Tuple3<Integer, Long, String>, Tuple2<Iterable<Tuple3<Double,Double,Integer>>, Iterable<Tuple3<Double,Long,Double>>>>
              JavaPairRDD<Object,BSONObject> output;
            output = gzindex.mapToPair(new PairFunction<Tuple2<Tuple3<Integer, Long, String>, Tuple2<Iterable<AreaSummary>, Iterable<RouteSummary>>>, Object,BSONObject>() {
                @Override
                public Tuple2<Object,BSONObject> call(Tuple2<Tuple3<Integer, Long, String>, Tuple2<Iterable<AreaSummary>, Iterable<RouteSummary>>> t) throws Exception {
                    BSONObject bsono,aso,rso,ptl,temp;
                    bsono= new BasicBSONObject();
                    bsono.put("day", t._1._1());
                    bsono.put("time", t._1._2());
                    bsono.put("areacode", t._1._3());
                    
                    aso= new BasicBSONObject();
                    for (AreaSummary as : t._2._1) {
                        aso.put("fareaverage", as.getFare());
                        aso.put("demandprob", as.getCount());
                        ptl = new BasicBSONList();
                        for(int i=0;i<as.getPickuptimeList().size();i++){
//                        for(Tuple2<Date,Double> pd: as.getPickuptimeList()){
                            Tuple2<Date,Double> pd = as.getPickuptimeList().get(i);
                            temp = new BasicBSONObject();
                            temp.put("key", pd._1.getTime());
                            temp.put("value", pd._2());
                            ptl.put(i+"", temp);
                        }
                        aso.put("pickuptime",ptl); 
                    }
                    bsono.put("areasummary", aso);
                    
                    rso = new BasicBSONList();
                    int i = 0;
                    for(RouteSummary rs:t._2._2){
                        temp = new BasicBSONObject();
                        //temp.put("zvalue", rs.getZvalue());
                        //temp.put("routecode", rs.getRoutecode());
                        temp.put("dropoffarea", rs.getDropoffarea().getAreacode());
                        temp.put("tripdistance", rs.getTripdistance());
                        temp.put("traveltime", rs.getTraveltime());
                        temp.put("cost", rs.getCost());
                        //temp.put("skyline", rs.getSkyline());
                        rso.put(i+"", temp);
                        i = i+1;
                    }
                    bsono.put("routesummary", rso);
                    return new Tuple2<>(null,bsono);
                }
            });
              
             output.saveAsNewAPIHadoopFile("file:///bogus", Object.class, BSONObject.class, MongoOutputFormat.class, outputConfig);

	      //sc.stop();
	}


}
