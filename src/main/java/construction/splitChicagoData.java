/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package construction;

import com.google.common.collect.Iterables;
import com.mongodb.hadoop.MongoOutputFormat;
import static construction.naiveconstruction.splitLine;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.Partitioner;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;
import scala.Tuple3;

/**
 *
 * @author fadhi
 */
public class splitChicagoData {
    protected static String[] splitLine(String line){;
	    //String temp = line.replaceAll("\\[","").replaceAll("\\]", "").replaceAll("\\(", "").replaceAll("\\)", "");
	    String[] res = line.split(",");
	    return res;
	}
    public static void main(String[] args) {
    	 
	    if (args.length < 1) {
	        System.err.println("Usage: Naive Constructor");
	        System.exit(1);
	      }

//	      SparkConf conf = new SparkConf().setAppName("IndexConstruction")
//	    		  .set("spark.sql.warehouse.dir", "D:\\dhila\\workspace")
//	    		  .set("spark.mongodb.output.uri", "mongodb://127.0.0.1/test."+args[1]);
//	      JavaSparkContext sc = new JavaSparkContext(conf);
//
//	      //final Accumulator<Double> accum = sc.accumulator(0L);
//	      JavaRDD<String> lines = sc.textFile(args[0]).filter(new Function<String, Boolean>() {
//			
//			public Boolean call(String v1) throws Exception {
//				if(v1.contains("VendorID") == true)
//					return false;
//				else return true;
//			}
//		});
	      System.out.println(args[0]);
              SparkConf conf = new SparkConf().setAppName("Naive Index Construction");//.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
	    		 // .set("spark.mongodb.output.uri", "mongodb://164.125.121.111:27020/taxi_trip."+args[1]);
	      JavaSparkContext sc = new JavaSparkContext(conf);
              //Configuration outputConfig = new Configuration();
               //outputConfig.set("mongo.output.uri","mongodb://164.125.121.111:27020/taxi_trip."+args[1]);   
	      //final Accumulator<Double> accum = sc.accumulator(0L);
              //Integer partition = Integer.parseInt(args[2]);
//              Integer interval = Integer.parseInt(args[3]);
//              Integer codeLength = Integer.parseInt(args[4]);
//              final Broadcast<Integer> intervalBc = sc.broadcast(interval);
//              final Broadcast<Integer> codeLengthBc = sc.broadcast(codeLength);
	      //JavaRDD<String> lines = sc.textFile("/user/hduser/"+args[0]).filter(new Function<String, Boolean>() 
              JavaRDD<String> lines = sc.textFile(args[0]).filter(new Function<String, Boolean>() {
			
                        @Override
			public Boolean call(String v1) throws Exception {
				if(v1.contains("Trip Start Timestamp") == true || v1.contains("Trip ID") == true || v1.isEmpty())
					return false;
				else return true;
			}
		});//.repartition(partition);
	      /*
	       Data Raw Format
	        [1] "VendorID"              "tpep_pickup_datetime"  "tpep_dropoff_datetime" "passenger_count"       "trip_distance"        
			[6] "pickup_longitude"      "pickup_latitude"       "RatecodeID"            "store_and_fwd_flag"    "dropoff_longitude"    
			[11] "dropoff_latitude"      "payment_type"          "fare_amount"           "extra"                 "mta_tax"              
			[16] "tip_amount"            "tolls_amount"          "improvement_surcharge" "total_amount"
	       
	       */
	      //PRE-PROCESSING
        JavaPairRDD<Tuple2<Integer,Integer>, String> result= lines.mapToPair(new PairFunction<String, Tuple2<Integer,Integer>,String>() {
                @Override
                public Tuple2<Tuple2<Integer, Integer>,String> call(String t) throws Exception {
                    Tuple2<Integer,Integer> key = new Tuple2<>(0,0);
                    try{
                    String[] splitData = splitLine(t);
                    //07/10/2014 12:45:00 AM
                    //System.out.println(t);
                    DateFormat dt = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
                    //DateFormat dt = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    Date pickupDate = dt.parse(splitData[2]);
                     key = new Tuple2<>(pickupDate.getMonth(),pickupDate.getYear());
                   }
                    catch(Exception e){
                        System.out.println(t);
                        //return new Tuple2<>(0,0);}
                    }
                    return new Tuple2<>(key,t);
                }
            }).filter(new Function<Tuple2<Tuple2<Integer, Integer>, String>, Boolean>() {
                @Override
                public Boolean call(Tuple2<Tuple2<Integer, Integer>, String> v1) throws Exception {
                    if(v1._1._2.equals(0))return false;
                    else return true;
                }
            }).partitionBy(new Partitioner() {
                @Override
                public int numPartitions() {
                    return 48;
                }

                @Override
                public int getPartition(Object arg0) {
                    //System.out.println(arg0);
                    Tuple2<Integer,Integer> key = (Tuple2<Integer,Integer>) arg0;
                    Integer c=-1;
                    switch (key._2) {
                        case 113:
                            c=0;
                            break;
                        case 114:
                            c=1;
                            break;
                        case 115:
                            c=2;
                            break;
                        case 116:
                            c=3;
                            break;
                        default:
                            break;
                    }
                    if((12*c)+key._1() < 0){
                        
                        System.out.println(key+" "+(12*c)+key._1());
                    }
                    
                            
                    return (12*c)+key._1();
                }
            });
            
            JavaRDD<String> output = result.values();
            
              output.saveAsTextFile("chicago-split");
             //output.saveAsNewAPIHadoopFile("file:///bogus", Object.class, BSONObject.class, MongoOutputFormat.class, outputConfig);

	      //sc.stop();
	}
}
