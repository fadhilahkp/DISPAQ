/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package construction;

import com.github.davidmoten.geo.GeoHash;
import com.google.common.collect.Iterables;
import com.mongodb.hadoop.MongoOutputFormat;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import model.Area;
import model.AreaSummary;
import model.Route;
import model.RouteSummary;
import model.TaxiTrip;
import model.UpgradedSummary;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;
import org.bson.types.BasicBSONList;
import scala.Tuple2;
import scala.Tuple3;

/**
 *
 * @author fadhilah
 * 
 * spark-submit 
    --master yarn-cluster 
    --executor-memory 4GB 
    --num-executors 4 
    --jars 
            /home/hduser/dispaq/lib/geo-0.7.1.jar,
            /home/hduser/dispaq/lib/grumpy-core-0.2.2.jar,
            /home/hduser/dispaq/lib/mongo-spark-connector_2.10-2.0.0-rc0.jar,
            /home/hduser/dispaq/lib/mongo-java-driver-3.3.0.jar,
            /home/hduser/dispaq/lib/mongo-hadoop-core-1.5.0.jar 
    --class "construction.upgradeconstruction" 
    [application jar] 		ex: /home/hduser/dispaq/dispaq-1.0.jar 
    [data source]		ex: dataset1month 
    [destination]		ex: month1 (collection in MongoDB)
    [number of partition]	ex: 50
    [interval time period]      ex: 10 (10 minutes)
    [length of geohash code]    ex: 6

 */
public class upgradedconstruction {
    protected static long timePeriodIndentification(Date ct, Integer interval){
            Long timecategory;
            Date temp = new Date(0, 0, 0, ct.getHours(), ct.getMinutes());
            if(temp.getMinutes()%interval > 0 ) {
                    timecategory = temp.getTime()+((interval-(temp.getMinutes()%interval))*60000);
            }
            else{
                    timecategory = temp.getTime();
            }
            return timecategory;		
    }
    
    protected static String[] splitLine(String line){;
	    String temp = line.replaceAll("\\[","").replaceAll("\\]", "").replaceAll("\\(", "").replaceAll("\\)", "");
	    String[] res = temp.split(",");
            
	    return res;
    }
	
    protected static String defineArea(String lat, String lon, Integer len){
            Double areaLong = Double.parseDouble(lon);
            Double areaLat = Double.parseDouble(lat);
            
            if (areaLat > -90.0 && areaLat < 90) {
                return GeoHash.encodeHash(areaLat, areaLong, len);
            }
            else return null;
    }
	
    protected static String defineRoute(String line, Integer len){
            String pickupHash = null, dropoffHash = null;
            String[] sp = splitLine(line);

            pickupHash = defineArea(sp[2], sp[1], len);
            dropoffHash = defineArea(sp[4], sp[3], len);
            
            return pickupHash+"-"+dropoffHash;
     }
	
     @SuppressWarnings({ "serial", "resource" })
    public static void main(String[] args) {
            JavaRDD<TaxiTrip> analyzeData;
            JavaPairRDD<Tuple2<Integer, Long>, Tuple2<TaxiTrip, Integer>> timeGroupData;
	    JavaRDD<String> lines;
            JavaPairRDD<Tuple3<Integer, Long, String>, RouteSummary> routeSummary;
            JavaPairRDD<Tuple3<Integer, Long, String>, AreaSummary> areaSummary;
            JavaPairRDD<Tuple3<Integer, Long, String>,Iterable<UpgradedSummary>>  pqindex;
            JavaPairRDD<Object,BSONObject> output;         
            
            if (args.length < 1) {
	        System.err.println("Usage: Naive Constructor");
	        System.exit(1);
	      }
	      
            SparkConf conf = new SparkConf().setAppName("Upgraded Taxi Information Construction");//.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
                       // .set("spark.mongodb.output.uri", "mongodb://164.125.121.111:27020/taxi_trip."+args[1]);
            JavaSparkContext sc = new JavaSparkContext(conf);
            Configuration outputConfig = new Configuration();
            outputConfig.set("mongo.output.uri","mongodb://164.125.121.111:27020/taxi_trip."+args[1]);   

            Integer partition = Integer.parseInt(args[2]);
            Integer interval = Integer.parseInt(args[3]);
            Integer codeLength = Integer.parseInt(args[4]);

            final Broadcast<Integer> intervalBc = sc.broadcast(interval);
            final Broadcast<Integer> codeLengthBc = sc.broadcast(codeLength);


            lines = sc.textFile(args[0],partition).filter(new Function<String, Boolean>() {

                     @Override
                     public Boolean call(String v1) throws Exception {
                             if(v1.contains("VendorID") == true || v1.contains("vendor_id") == true || v1.isEmpty())
                                     return false;
                             else return true;
                     }
             });
            
	      /*
	       Data Raw Format
	        [1] "VendorID"              "tpep_pickup_datetime"  "tpep_dropoff_datetime" "passenger_count"       "trip_distance"        
			[6] "pickup_longitude"      "pickup_latitude"       "RatecodeID"            "store_and_fwd_flag"    "dropoff_longitude"    
			[11] "dropoff_latitude"      "payment_type"          "fare_amount"           "extra"                 "mta_tax"              
			[16] "tip_amount"            "tolls_amount"          "improvement_surcharge" "total_amount"
	       
	       */
	      //PRE-PROCESSING
            analyzeData = lines.map( 
                new Function<String, TaxiTrip>() {
                @SuppressWarnings("deprecation")
                    public TaxiTrip call(String arg0) throws Exception {
                        try{
                            TaxiTrip d = new TaxiTrip();
                            
                            Date pickupDate,dropoffDate;
                            Route route;
                            Area pickupArea,dropoffArea;
                            Integer day;
                            Double tripDistance,fareAmount,tipAmount,tollsAmount;
                            Long timecategory, tripTime;

                            String[] splitData = splitLine(arg0);

                            DateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            pickupDate = dt.parse(splitData[1]);
                            dropoffDate = dt.parse(splitData[2]);
                            day = pickupDate.getDay();
                            tripTime = dropoffDate.getTime() - pickupDate.getTime();
                            Date temp = new Date(0, 0, 0, pickupDate.getHours(), pickupDate.getMinutes());

                            timecategory = timePeriodIndentification(temp, intervalBc.getValue());
                            fareAmount =Double.parseDouble(splitData[12]);
                            tipAmount = Double.parseDouble(splitData[15]);

                            pickupArea = new Area(defineArea(splitData[6], splitData[5], codeLengthBc.getValue()));
                            pickupArea.setPickuptime(temp);
                            pickupArea.setCount(1.0);
                            pickupArea.setFare(fareAmount);
                            pickupArea.setTip(tipAmount);

                            dropoffArea = new Area(defineArea(splitData[10], splitData[9], codeLengthBc.getValue()));

                            route = new Route(pickupArea,dropoffArea);
                            route.setCount(1.0);
                            tripDistance = Double.parseDouble(splitData[4]);
                            route.setTripdistance(tripDistance);
                            route.setTraveltime(tripTime);
                            tollsAmount = Double.parseDouble(splitData[16]);
                            route.setCost(tollsAmount);
                            
                            d = new TaxiTrip(temp, pickupArea, route, day, timecategory);

                            return d;
                        }catch(Exception e){
                            System.out.println(arg0);
                        }
                        return new TaxiTrip();
                    }
                  }).filter(
                    new Function<TaxiTrip, Boolean>() {
                        public Boolean call(TaxiTrip v1)
                            throws Exception {
                            try{
                                if(v1.getPickuparea().getFare() == 0.0 || v1.getRoute().getTraveltime() == 0.0 || v1.getPickuparea().getAreacode().contains("s000000") == true || v1.getRoute().getDropoffarea().getAreacode() == "s000000" || v1.getRoute().getTripdistance() == 0.0){
                                    return false;}
                                else{
                                    return true;}
                            }catch (Exception e) {
                                    return false;
                            }
                    }
                });
	      
	      /*
	       Analyzed Data
	       
	       1  pickup date 			: Date
	       2  dropoff date			: Date
	       3  day					: Integer
	       4  pickup time category	: Date
	       5  pickup area			: String
	       6  dropoff area			: String
	       7  route					: String
	       8  trip distance			: double
	       9  fare amount			: double
	       10  tip amount			: double
	       11 tolls amount			: double
	       12 travel time			: double
	       */
	      
            timeGroupData = analyzeData.groupBy(
                    new Function<TaxiTrip, Tuple2<Integer,Long>>() {
                        public Tuple2<Integer,Long> call(
                                TaxiTrip v1)
                                throws Exception {
                                return new Tuple2<Integer,Long>(v1.getDay(),v1.getTimecat());
                        }
		}).flatMapToPair(
                    new PairFlatMapFunction<Tuple2<Tuple2<Integer,Long>,Iterable<TaxiTrip>>, Tuple2<Integer,Long>, Tuple2<TaxiTrip,Integer>>() {
                        public Iterable<Tuple2<Tuple2<Integer, Long>, Tuple2<TaxiTrip, Integer>>> call(Tuple2<Tuple2<Integer,Long>, Iterable<TaxiTrip>> t)
                                        throws Exception {
                            Integer count = Iterables.size(t._2);
                            Tuple2<TaxiTrip, Integer> d;
                            List<Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip, Integer>>> l = new ArrayList<Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip, Integer>>>();
                            for (TaxiTrip tt : t._2) {
                                    d = new Tuple2<TaxiTrip, Integer>(tt,count);
                                    l.add(new Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip,Integer>>(t._1,d));
                            }
                            return l;
                        }
		});
	      
	      //ROUTE SUMMARY
	      
		  /*
		   * key: day, pickup datetime, pickup area
		   * value : trip distance, travel time, spending cost, 1
		  */
	    routeSummary = timeGroupData.mapToPair(
                    new PairFunction<Tuple2<Tuple2<Integer,Long>,Tuple2<TaxiTrip,Integer>>, Tuple3<Integer, Long, String>, Tuple2<Route, Integer>>() {
                        public Tuple2<Tuple3<Integer, Long, String>, Tuple2<Route, Integer>> call(Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip, Integer>> t) throws Exception {
                            Tuple3<Integer, Long, String> key = new Tuple3<Integer, Long, String>(t._1._1,t._1._2, t._2._1.getRoute().getRoutecode());
                            Tuple2<Route, Integer> value = new Tuple2<Route, Integer>(t._2._1.getRoute(), 1);
                            return new Tuple2<Tuple3<Integer,Long,String>, Tuple2<Route,Integer>>(key, value);
                        }

                }).reduceByKey(
                    new Function2<Tuple2<Route,Integer>, Tuple2<Route,Integer>, Tuple2<Route,Integer>>() {
                        public Tuple2<Route, Integer> call(Tuple2<Route, Integer> v1, Tuple2<Route, Integer> v2) throws Exception {
                            Double tdSum,cSum;
                            Long ttSum;
                            Integer count;
                            Route r = new Route(v1._1.getPickuparea(), v1._1.getDropoffarea());
                            
                            ttSum = v1._1.getTraveltime();
                            tdSum = v1._1.getTripdistance()+v2._1.getTripdistance();
                            cSum = v1._1.getCost() + v2._1.getCost();
                            count = v1._2+v2._2;
                            
                            r.setCost(cSum);
                            r.setTraveltime(ttSum);
                            r.setTripdistance(tdSum);
                            //r.setCount(count);
                            return new Tuple2<Route, Integer>(r, count);
                        }
                }).map(
                    new Function<Tuple2<Tuple3<Integer, Long, String>, Tuple2<Route, Integer>>, List<Tuple2<Tuple3<Integer,Long,String>,RouteSummary>>>() {
                 @Override
                        public List<Tuple2<Tuple3<Integer,Long,String>,RouteSummary>> call(Tuple2<Tuple3<Integer, Long, String>, Tuple2<Route, Integer>> v1) throws Exception {
                            RouteSummary r = new RouteSummary(v1._2._1.getPickuparea(), v1._2._1.getDropoffarea());
                            Long minarrivaltime,maxarrivaltime,minatp,maxatp;
                            Tuple3<Integer, Long, String> minkey,maxkey;
                            List<Tuple2<Tuple3<Integer,Long,String>,RouteSummary>> l = new ArrayList<Tuple2<Tuple3<Integer,Long,String>,RouteSummary>>();
                            
                            r.setDay(v1._1._1());
                            r.setTime(v1._1._2());
                            r.setCost(v1._2._1().getCost()/v1._2._2());
                            r.setTraveltime(v1._2._1().getTraveltime()/v1._2._2());
                            r.setTripdistance(v1._2._1().getTripdistance()/v1._2._2());

                            minarrivaltime = v1._1._2() - (10L*60000)+ 1 + r.getTraveltime();
                            
                            minatp = timePeriodIndentification(new Date(minarrivaltime), intervalBc.getValue());
                            maxatp = timePeriodIndentification(new Date(v1._1._2()+r.getTraveltime()), intervalBc.getValue());

                            minkey = new Tuple3<Integer, Long, String>(v1._1._1(), minatp, v1._2._1.getDropoffarea().getAreacode());
                            maxkey = new Tuple3<>(v1._1._1(), maxatp, v1._2._1.getDropoffarea().getAreacode());
                            
                            l.add(new Tuple2<>(minkey,r));
                            l.add(new Tuple2<>(maxkey,r));
                            
                            return l;
                 }
             }).flatMapToPair(
                    new PairFlatMapFunction<List<Tuple2<Tuple3<Integer, Long, String>, RouteSummary>>, Tuple3<Integer, Long, String>, RouteSummary>() {
                        @Override
                        public Iterable<Tuple2<Tuple3<Integer, Long, String>, RouteSummary>> call(List<Tuple2<Tuple3<Integer, Long, String>, RouteSummary>> t) throws Exception {
                            return t;
                     }
             });
	      
            areaSummary = timeGroupData.mapToPair(
                    new PairFunction<Tuple2<Tuple2<Integer,Long>,Tuple2<TaxiTrip,Integer>>, Tuple3<Integer, Long, String>, Tuple2<AreaSummary,Integer>>() {
                        public Tuple2<Tuple3<Integer, Long, String>, Tuple2<AreaSummary, Integer>> call(Tuple2<Tuple2<Integer,Long>, Tuple2<TaxiTrip, Integer>> t) throws Exception {
                            Tuple3<Integer,Long,String> key = new Tuple3<Integer, Long, String>(t._1._1,t._1._2, t._2._1.getPickuparea().getAreacode());

                            AreaSummary aa = new AreaSummary(t._2._1.getPickuparea());
                            Tuple2<AreaSummary,Integer> value = new Tuple2<AreaSummary, Integer>(aa, t._2._2);

                            return new Tuple2<Tuple3<Integer,Long,String>, Tuple2<AreaSummary,Integer>>(key,value);
                        }

            }).reduceByKey(
                    new Function2<Tuple2<AreaSummary,Integer>, Tuple2<AreaSummary,Integer>, Tuple2<AreaSummary,Integer>>() {
                        public Tuple2<AreaSummary, Integer> call(Tuple2<AreaSummary, Integer> v1, Tuple2<AreaSummary, Integer> v2) throws Exception {
                            AreaSummary a = new AreaSummary(v1._1.getAreacode());
                            a.setCount(v1._1.getCount()+v2._1.getCount());
                            a.setFare(v1._1.getFare()+v1._1.getTip() + v2._1.getFare()+v2._1.getTip());
                            a.addPickupTime(v1._1.getPickuptimeHash());
                            a.addPickupTime(v2._1.getPickuptimeHash());
                            return new Tuple2<AreaSummary, Integer>(a, v1._2);
                    }
            }).mapToPair(
                    new PairFunction<Tuple2<Tuple3<Integer,Long,String>,Tuple2<AreaSummary,Integer>>, Tuple3<Integer, Long, String>, AreaSummary>() {
                        public Tuple2<Tuple3<Integer, Long, String>, AreaSummary> call(Tuple2<Tuple3<Integer, Long, String>, Tuple2<AreaSummary, Integer>> t) throws Exception {
                            AreaSummary aa = new AreaSummary();

                            aa.setAreacode(t._2._1.getAreacode());
                            aa.setCount(t._2._1.getCount()/t._2._2);
                            aa.setFare(t._2._1.getFare()/t._2._1.getCount().intValue());
                            aa.setPickuptimeList(t._2._1.getPickuptimeHash());
                            aa.computeAverage(t._2._1.getCount().intValue());

                            return new Tuple2<Tuple3<Integer,Long,String>, AreaSummary>(t._1, aa);
                        }
                });

             pqindex = areaSummary.join(routeSummary).mapToPair(new PairFunction<Tuple2<Tuple3<Integer, Long, String>, Tuple2<AreaSummary, RouteSummary>>, Tuple3<Integer, Long, String>, UpgradedSummary>() {
                 @Override
                 public Tuple2<Tuple3<Integer, Long, String>, UpgradedSummary> call(Tuple2<Tuple3<Integer, Long, String>, Tuple2<AreaSummary, RouteSummary>> t) throws Exception {
                     UpgradedSummary us = new UpgradedSummary();
                     RouteSummary rs = new RouteSummary(t._2._2);
                     AreaSummary as = new AreaSummary(t._2._1());
                     us.setRs(t._2._2());
                      Long mintime;
                        Date maxat = new Date(rs.getTime()+rs.getTraveltime());
                        if(t._1._2().equals(timePeriodIndentification(maxat, intervalBc.getValue()))){
                            int mod = maxat.getMinutes()%10;
                            mintime = rs.getTime()-mod*60000L;
                        }else{
                            mintime = (rs.getTime()-10*60000L)+1;
                        }
                        us.setMinTimePeriod(mintime);
                            us.setMinArea(as);
                     
                     Tuple3<Integer, Long, String> key = new Tuple3<>(us.getRs().getDay(),us.getRs().getTime(),us.getRs().getPickuparea().getAreacode());
                        return new Tuple2<>(key,us);
                 }
             }).filter(new Function<Tuple2<Tuple3<Integer, Long, String>, UpgradedSummary>, Boolean>() {
                 @Override
                 public Boolean call(Tuple2<Tuple3<Integer, Long, String>, UpgradedSummary> v1) throws Exception {
                     if(v1._2.getMinArea().getFare() == 0.0)  
                         return false;
                     else 
                         return true;
                 }
             }).groupByKey();
                  
            output = pqindex.mapToPair(new PairFunction<Tuple2<Tuple3<Integer, Long, String>, Iterable<UpgradedSummary>>, Tuple3<Integer, Long, String>, Iterable<UpgradedSummary>>() {
                 @Override
                 public Tuple2<Tuple3<Integer, Long, String>, Iterable<UpgradedSummary>> call(Tuple2<Tuple3<Integer, Long, String>, Iterable<UpgradedSummary>> t) throws Exception {
                    HashMap<String,UpgradedSummary> tempmap = new HashMap<String, UpgradedSummary>();

                    for(UpgradedSummary us:t._2()){
                        if(tempmap.containsKey(us.getRs().getDropoffarea().getAreacode())){
                            UpgradedSummary ustemp = tempmap.get(us.getRs().getDropoffarea().getAreacode());
                            Long time = ustemp.getMinTimePeriod();
                            if(time == 0L){
                                ustemp.setMinTimePeriod(us.getMinTimePeriod());
                                ustemp.setMinArea(us.getMinArea());
                            }
                            else{
                            ustemp.setMaxTimePeriod(us.getMinTimePeriod());
                                ustemp.setMaxArea(us.getMinArea());
                            }
                        }else{
                            UpgradedSummary ustemp = new UpgradedSummary();
                            if(us.getMinTimePeriod().equals((us.getRs().getTime()-10*60000L)+1)){
                                tempmap.put(us.getRs().getDropoffarea().getAreacode(), us);}
                            else{
                                ustemp.setRs(us.getRs());
                                ustemp.setMaxTimePeriod(us.getMinTimePeriod());
                                ustemp.setMaxArea(us.getMinArea());
                                tempmap.put(us.getRs().getDropoffarea().getAreacode(), ustemp);}
                            
                        }
                    }
                    Iterable<UpgradedSummary> usl = new ArrayList<>(tempmap.values()); 
                    return new Tuple2<>(t._1,usl);
                    
                 }
             }).mapToPair(new PairFunction<Tuple2<Tuple3<Integer, Long, String>,Iterable<UpgradedSummary>>, Object,BSONObject>() {
                 @Override
                 public Tuple2<Object,BSONObject> call(Tuple2<Tuple3<Integer, Long, String>,Iterable<UpgradedSummary>> t) throws Exception {
                      BSONObject temp,ptl,rso,rstemp,temparea,bsono = new BasicBSONObject();
                      
                        bsono.put("day", t._1._1());
                        bsono.put("time", t._1._2());
                        bsono.put("areacode", t._1._3());
                        rso = new BasicBSONList();
                        int j =0;
                        for(UpgradedSummary us:t._2()){
                            rstemp = new BasicBSONObject();
                            rstemp.put("dropoffarea", us.getRs().getDropoffarea().getAreacode());
                            rstemp.put("tripdistance", us.getRs().getTripdistance());
                            rstemp.put("traveltime",us.getRs().getTraveltime());
                            rstemp.put("cost", us.getRs().getCost());
                            
                            
                            temparea = new BasicBSONObject();
                            temparea.put("time",us.getMinTimePeriod());
                            temparea.put("areatime", us.getMinArea().getTime());
                            temparea.put("fareaverage", us.getMinArea().getFare());
                            temparea.put("demandprob", us.getMinArea().getCount());
                            
                            ptl = new BasicBSONList();
                            for(int i=0;i<us.getMinArea().getPickuptimeList().size();i++){
    //                        for(Tuple2<Date,Double> pd: as.getPickuptimeList()){
                                Tuple2<Date,Double> pd = us.getMinArea().getPickuptimeList().get(i);
                                temp = new BasicBSONObject();
                                temp.put("key", pd._1.getTime());
                                temp.put("value", pd._2());
                                ptl.put(i+"", temp);
                            }
                            temparea.put("pickuptime",ptl);
                            rstemp.put("min",temparea);
                            
                            temparea = new BasicBSONObject();
                            temparea.put("time",us.getMaxTimePeriod() );
                            temparea.put("areatime", us.getMaxArea().getTime());
                            temparea.put("fareaverage", us.getMaxArea().getFare());
                            temparea.put("demandprob", us.getMaxArea().getCount());
                            
                            ptl = new BasicBSONList();
                            for(int i=0;i<us.getMaxArea().getPickuptimeList().size();i++){
    //                        for(Tuple2<Date,Double> pd: as.getPickuptimeList()){
                                Tuple2<Date,Double> pd = us.getMaxArea().getPickuptimeList().get(i);
                                temp = new BasicBSONObject();
                                temp.put("key", pd._1.getTime());
                                temp.put("value", pd._2());
                                ptl.put(i+"", temp);
                            }
                            temparea.put("pickuptime",ptl);
                            rstemp.put("max",temparea);
                            
                            rso.put(j+"", rstemp);
                            j++;
                        }
                       
                        bsono.put("route summary", rso);
                        
                    return new Tuple2<>(null,bsono);
                        
                 }
             });
              
             output.saveAsNewAPIHadoopFile("file:///bogus", Object.class, BSONObject.class, MongoOutputFormat.class, outputConfig);

	     
	}
}
