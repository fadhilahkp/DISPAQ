package construction;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;

import com.github.davidmoten.geo.GeoHash;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import model.Area;
import model.AreaSummary;
import model.ProfitabilityArea;
import model.RouteSummary;


public class profitabilitymapConverter {
    protected static long timePeriodIndentification(Date ct){
            Long timecategory;
            Date temp = new Date(0, 0, 0, ct.getHours(), ct.getMinutes());
            if(temp.getMinutes()%10 > 5) {
                    timecategory = temp.getTime()+((10-(temp.getMinutes()%10))*60000);
                    ////System.out.println("> 5 "+pickupDate+" "+temp +" "+temp.getMinutes() +" "+new Date(timecategory));
            }
            else{
                    timecategory = temp.getTime()-(temp.getMinutes()%10*60000);
                    ////System.out.println("<= 5 "+pickupDate+" "+temp +" "+temp.getMinutes() +" "+new Date(timecategory));
            }
            ////System.out.println("time period:" + timecategory);
            return timecategory;

    }
	
    protected static String[] splitLine(String line){;
        String temp = line.replaceAll("\\[","").replaceAll("\\]", "").replaceAll("\\(", "").replaceAll("\\)", "");
        String[] res = temp.split(",");
        return res;
    }

    protected static String defineArea(String lat, String lon, Integer len){
	Double areaLong = Double.parseDouble(lon);
        Double areaLat = Double.parseDouble(lat);
        if (areaLat > -90.0 && areaLat < 90) {
            return GeoHash.encodeHash(areaLat, areaLong, len);
        }
        else return null;
    }

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String query = null,cl,currentarea,areal,timel,ctl,wb,xb,yb,zb,code = "";
        Date ct;
        Long timecategory = null,spendingtime=0L,timediv,timelength;
        Integer d = null,w,x,y,z,arealength;
        RouteSummary rs;
        AreaSummary as;
        List<String> destlist;
        List<Integer> daylist;
        List<Long> timelist;
        HashMap<String,RouteSummary> rslist;
        HashMap<String,AreaSummary> aslist;
        DBCursor cursor; 
        Map.Entry<String,AreaSummary> am;
        Map.Entry<String,RouteSummary> rm;
        List<ProfitabilityArea> rsl ;
        List<ProfitabilityArea> res ;
        JSONObject jsonobj;
        BasicDBList pmlist;
        Double tripdistance = 0.0,profit=0.0,demandprob=0.0,profitdiv,demandprobdiv,distdiv;
        //Map.Entry<String,RouteSummary> m;
        Set s;
        Iterator it;

        MongoClient mongoClient = new MongoClient("164.125.121.111",27020);
        DB db = mongoClient.getDB("taxi_trip");
        DBCollection coll = db.getCollection("month6");
        DBCollection pmcoll = db.getCollection("pm2");                
		//JSONParser parser = new JSONParser();
        System.out.print ("Current location: ");
        cl = reader.next();
        //while(cl != "."){
        System.out.print ("Area length: ");
		//areal = reader.next();
		//timel = reader.next();
		//ctl =reader.next();
        while(!cl.equals("-")){
            pmlist = new BasicDBList();

            //String[] clt = splitLine(cl);
            System.out.print ("Area length: ");
            arealength = Integer.parseInt("7");
            Long currenttime = System.currentTimeMillis();

            currentarea = "^"+cl;
            System.out.print ("Millisecond Time: ");
            Long ctll = reader.nextLong();
            for(long k = ctll; k < ctll*10;k+=600000L){
                ct = new Date(k);
                timecategory = timePeriodIndentification(ct);
            rsl = new ArrayList<ProfitabilityArea>();
            res = new ArrayList<ProfitabilityArea>();
            //timecategory = -2209048800000L;
            //System.out.print ("Current day:");
            d = ct.getDay();
            System.out.println("Current time: "+dateFormat.format(ct));
            //System.out.println("\nCurrent time in second: "+ct.getTime());
            //read file
            //FileWriter fileWriter = new FileWriter("G:\\S2\\pm2\\"+d+"-"+timecategory+"-"+cl+".csv") ;


            //dapet seluruh data sesuai ct dan cl
            BasicDBObject areaRegex = new BasicDBObject("$regex",currentarea);

            //DBObject timeRange = new BasicDBObject();
//					timelength = 240*60000L;
//					timeRange.put("$gte",timecategory);
//					timeRange.put("$lte",timecategory+timelength);

            BasicDBObject dayObject = new BasicDBObject("day",d);
            BasicDBObject timeObject = new BasicDBObject("time", timecategory);
            BasicDBObject areaObject = new BasicDBObject("areacode", areaRegex);
            BasicDBList dbl = new BasicDBList();
            dbl.add(dayObject);
            dbl.add(timeObject);
            dbl.add(areaObject);
            DBObject search = new BasicDBObject();
            search.put("$and", dbl);
            System.out.println(search);

            cursor = coll.find( search);
            rslist = new HashMap<String, RouteSummary>();
            destlist = new ArrayList<String>();
            timelist = new ArrayList<Long>();

            while(cursor.hasNext()){
                //Object obj = parser.parse(query);
                DBObject cn = cursor.next();
                jsonobj= new JSONObject(cn.toString());
                //System.out.println(jsonobj.toString());
                /*
                data format
                query = 
                [
                        {day: 1
                         time:
                         AreaCode:
                         AreaSummary: {
                                fareAverage:
                                pickupTimeMod:[{}]
                                demandProb:
                         }
                         RouteSummary:
                                [
                                        {
                                                groupid:
                                                zvalue:
                                                route:
                                                destarea:
                                                distanceTrip:
                                                travelTime:
                                                cost:
                                        },
                                        {...}
                                ]
                         },
                         {...}
                ]
         * */

                //select route

                JSONArray array = jsonobj.getJSONArray("routesummary");
                //List<Object> rsl=array.;


                //System.out.println("ROUTE SUMMARY");
                for(int i=0;i<array.length();i++){
                    //System.out.println(array.get(i).toString());
                    JSONObject o = (JSONObject) array.get(i);
                    //System.out.println(o.toString());
                    String dropoffarea = o.getString("dropoffarea").substring(0, arealength);
                    //Long temp = new Date(0, 0, 0, ct.getHours(), ct.getMinutes()).getTime() +o.getLong("traveltime");
                    //if(!rslist.containsKey(dropoffarea)){
                    rs = new RouteSummary(new Area(currentarea),new Area(dropoffarea));

                    rs.setCost(o.getDouble("cost"),0);
                    rs.setTraveltime(o.getLong("traveltime"));
                    rs.setTripdistance(o.getDouble("tripdistance"));
                    rs.setCount(1.0);

                    //Long timeperiod =timePeriodIndentification(new Date(temp));
                    //timelist.add(timeperiod);
                    rslist.put(dropoffarea,rs);
//			                }else{
//			                	rs = rslist.get(dropoffarea);
//			                	
//			                	rs.setCost(o.getDouble("cost")+rs.getCost(),0);
//				                rs.setTraveltime(o.getLong("traveltime")+rs.getTraveltime());
//				                rs.setTripdistance(o.getDouble("tripdistance")+rs.getTripdistance());
//				                rs.setCount(1.0+rs.getCount());
//				                
//				                rslist.replace(dropoffarea, rs);
//			                }
                }
            }
                  //Set s;
            s = rslist.entrySet();
            Iterator i1 = s.iterator();
            while(i1.hasNext()){
                    rm = (Entry<String, RouteSummary>) i1.next();
                    RouteSummary rstemp = rm.getValue();

                    rstemp.setCost(rstemp.getCost()/rstemp.getCount(),0);
                    rstemp.setTraveltime(rstemp.getTraveltime()/rstemp.getCount().intValue());
                    rstemp.setTripdistance(rstemp.getTripdistance()/rstemp.getCount());
                    //rstemp.setCount(1.0+rstemp.getCount());

                    Long temp = new Date(0, 0, 0, ct.getHours(), ct.getMinutes()).getTime() +rstemp.getTraveltime();
                    Long timeperiod =timePeriodIndentification(new Date(temp));
                    destlist.add(rstemp.getDropoffarea().getAreacode());
                    timelist.add(timeperiod);
                    //System.out.println(rstemp.print());
                    rslist.replace(rstemp.getDropoffarea().getAreacode(), rstemp);
                        }

            System.out.println("ROUTE SUMMARY SIZE: "+rslist.size());


                        //SELECT AREA
            if(rslist.size() > 0){
                List<DBObject> andl = new ArrayList<DBObject>();
                for (int i = 0; i < destlist.size(); i++) {
                    List<DBObject> tdbl = new ArrayList<DBObject>();
                    areaRegex = new BasicDBObject("$regex","^"+destlist.get(i));

    //			                    timeRange = new BasicDBObject();
    //			            		timeRange.put("$gte",timelist.get(i));
    //			            		timeRange.put("$lte",timelist.get(i)+timelength);

                    tdbl.add(new BasicDBObject("day",d));
                    tdbl.add(new BasicDBObject("time", timelist.get(i)));
                    tdbl.add(new BasicDBObject("areacode", areaRegex));
                    andl.add(new BasicDBObject("$and", tdbl));
                }
                DBObject or = new BasicDBObject("$or", andl);
                cursor = coll.find(or);
                aslist = new HashMap<String, AreaSummary>();
                //System.out.println(or);
                //cursor.
                System.out.println("AREA SUMMARY");
                while(cursor.hasNext()){
                    DBObject cn = cursor.next();
                    //System.out.println(cn.toString());
                    jsonobj= new JSONObject(cn.toString());
                    //System.out.println(jsonobj.toString());
                    String areacode = jsonobj.getString("areacode").substring(0, arealength);
                    //if(!aslist.containsKey(areacode)){
                    as = new AreaSummary(new Area(areacode));
                    as.setDay(jsonobj.getInt("day"));
                    as.setTime(jsonobj.getLong("time"));

                    as.setCount(jsonobj.getJSONObject("areasummary").getDouble("demandprob"));
                    as.setFare(jsonobj.getJSONObject("areasummary").getDouble("fareaverage"));
                    JSONArray array = jsonobj.getJSONObject("areasummary").getJSONArray("pickuptime");
                    //rsl = array.toList();
                    for(int i=0;i<array.length();i++){
                        JSONObject o = new JSONObject(array.get(i).toString());
                        as.addPickupTime(new Date(o.getLong("key")),o.getDouble("value"));
                    }
                                //as.setPickuptime(new Date(jsonobj.getJSONObject("areasummary").getString("pickupTimeMod")));
                    aslist.put(areacode,as);
    //			                    } else{
    //			                    	as = aslist.get(areacode);
    //			                    	as.setCount(as.getCount()+jsonobj.getJSONObject("areasummary").getDouble("demandprob"));
    //			                    	as.setFare(as.getFare()+jsonobj.getJSONObject("areasummary").getDouble("fareaverage"));
    //			                    	
    //			                    	 JSONArray array = jsonobj.getJSONObject("areasummary").getJSONArray("pickuptime");
    //			 	                    
    //			                    	 for(int i=0;i<array.length();i++){
    //			 	                            JSONObject o = new JSONObject(array.get(i).toString());
    //			 	                            as.addPickupTime(new Date(o.getLong("key")),o.getDouble("value"));
    //			 	                    }
    //			 	                    
    //			                    	//aslist.replace(areacode,as);
    //			 	                    
    //			                    }
                        //System.out.println(as.print());
                }
                System.out.println("AREA SUMMARY SIZE: "+aslist.size());
                //calculate PM
                List<ProfitabilityArea> profitabilityMap = new ArrayList<ProfitabilityArea>();
                ProfitabilityArea ap;

                s = aslist.entrySet();
                it = s.iterator();
                if(!it.hasNext()) System.out.println("Profitable Area is not found");

                while(it.hasNext()){
                    am = (Map.Entry<String,AreaSummary>) it.next();
                    RouteSummary a = rslist.get(am.getKey());
                    ap = new ProfitabilityArea(am.getValue().getDay(),am.getValue().getTime(),am.getValue().getAreacode());
                    ap.setDemandProbability(am.getValue().getCount());
                    ap.setTripdistance(a.getTripdistance());
                    ap.setProfit(am.getValue().getFare(),a.getCost());
                   // ap.setZskyline(a.getSkyline());

                    //ap.setGroupid(a.getGroupId());
                    //choose the biggest probability after arrival time
                    Long temp = new Date(0, 0, 0, ct.getHours(), ct.getMinutes()).getTime() +a.getTraveltime();
                    Date at = am.getValue().getPickupTime(new Date(temp));
                    ap.setSpendingTime(new Date(0, 0, 0, ct.getHours(), ct.getMinutes()), a.getTraveltime(), at);
                    if(ap.getSpendingTime() < 0){
                        System.out.println(new Date(0, 0, 0, ct.getHours(),ct.getMinutes())+" "+a.getTraveltime()+" "+at);
                        System.out.println("");
                    }

    //			                    if(ap.getTripdistance() > tripdistance) tripdistance = ap.getTripdistance();
    //			                    if(ap.getSpendingTime() > spendingtime) spendingtime = ap.getSpendingTime();
    //			                    if(ap.getDemandProbability() > demandprob) demandprob = ap.getDemandProbability();
    //			                    if(ap.getProfit() > profit) profit = ap.getProfit();
                   // System.out.println(new Date(0, 0, 0, ct.getHours(), ct.getMinutes())+" "+ a.getTraveltime()+" "+ at+" "+new Date(temp));
                    //System.out.println(ap.print());
                    if(ap.getProfit() > 0.0){
                        profitabilityMap.add(ap);
                    }
                }
                                                ////System.out.println("------------------END--------------------");

                                                //skyline

    //			                                distdiv=tripdistance/64;
    //			                            timediv = spendingtime/64;
    //			                            profitdiv = profit/64;
    //			                            demandprobdiv = demandprob/64;
                                            ////System.out.println("division : "+distdiv+" "+timediv+" "+costdiv);
                String csv = "";
                for(ProfitabilityArea pa:profitabilityMap){
                ////System.out.println("value : "+rs.getTripdistance()+" "+rs.getTraveltime()+" "+rs.getCost());
                ////System.out.println("asli : "+rs.getTripdistance()/distdiv+" "+rs.getTraveltime()/timediv+" "+rs.getCost()/costdiv);
                //if(!pa.getZskyline()){
                    rsl.add(pa);//} 

                //csv+=pa.printcsv();
                //fileWriter.append(pa.printcsv());
               // else {
                    //res.add(pa);
                //}
                    BasicDBObject doc = new BasicDBObject("day", pa.getDay())
                                    .append("time", pa.getTime())
                                    .append("areacode", pa.getAreacode())
                                    //.append("zbin", pa.getZbin())
                                    .append("profit", pa.getProfit())
                                    .append("tripdistance", pa.getTripdistance())
                                    .append("spendingtime", pa.getSpendingTime())
                                    .append("demandprobability", pa.getDemandProbability());
                    //pmlist.add(doc);
                    try{
                        pmcoll.insert(doc);}catch (Exception e) {
                                                                    //System.out.println("duplicate");
                        }

                }
            }
    //			                            fileWriter.flush();
    //			                            fileWriter.close();

                        //areal = reader.next();
                        //timel = reader.next();
                        //ctl =reader.next();
                        }
            System.out.print ("Current location: ");
            cl = reader.next();
            System.out.print ("Area length: ");
		
        }
        mongoClient.close();
		
    }
}